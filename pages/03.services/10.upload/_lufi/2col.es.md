---
title: Subida
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://upload.disroot.org/">Subir archivos</a>
<a class="button button1" href="http://e2olmnzdp5d72z3xs2ugftvwgxywgbgipofa443zizolbgxoj5m46vyd.onion">Tor</a>

---

![](lufi.png)
## Lufi - Servicio de alojamiento temporal cifrado de archivos

El servicio **Subida** de **Disroot** es un software de hospedaje de archivos, desarrollado por **Lufi**. Almacena temporalmente archivos así pueden compartirse con otras personas a través de un link.
Para proteger la privacidad, todos los archivos son cifrados en el navegador mismo, esto significa que los archivos nunca salen de nuestra computadora sin cifrar.
Los administradores no pueden ver el contenido de los archivos - ni el administrador de tu red o tu ISP - solo el nombre del archivo, su tamaño y su tipo [MIME](https://es.wikipedia.org/wiki/MIME) (qué clase de archivo es: video, texto, etc).

Código fuente: [https://framagit.org/fiat-tux/hat-softwares/lufi](https://framagit.org/fiat-tux/hat-softwares/lufi)
