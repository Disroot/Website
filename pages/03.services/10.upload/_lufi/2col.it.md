---
title: Upload
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://upload.disroot.org/">Carica file</a>
<a class="button button1" href="http://e2olmnzdp5d72z3xs2ugftvwgxywgbgipofa443zizolbgxoj5m46vyd.onion">Tor</a>

---

![](lufi.png)
## Lufi - Servizio di caricamento criptato di file temporanei

Upload è un software di file hosting, fornito da Lufi. Memorizza temporaneamente i file in modo da poterli condividere con altri utilizzando un link.
Per proteggere la privacy, tutti i file sono criptati nel browser stesso - Significa che i file non lasciano mai il computer se non in modo criptato.
L'amministratore dell'istanza Lufi (in questo caso gli amministratori Disroot) non saranno in grado di vedere il contenuto del tuo file, né il tuo amministratore di rete o il tuo ISP.
L'amministratore può vedere solo il nome del file, la sua dimensione e il suo mimetype (che tipo di file è: video, testo, ecc.).
