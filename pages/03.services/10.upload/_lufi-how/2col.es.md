---
title: 'Cómo se usa Lufi'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## ¿Cómo funciona?

Arrastrando y sueltando archivos en el área correspondiente, o a la manera tradicional de navegar y seleccionarlos, y estos serán cargados, cifrados y enviados al servidor. Obtendremos dos enlaces por archivo: uno de descarga (que le damos a las personas con las que queremos compartir el archivo), y uno de eliminación, que permite borrar el archivo cuando queramos.
Se puede ver la lista de los archivos haciendo click en el enlace *"Mis archivos"* en la parte superior derecha de esa página.

No es necesario registrarse para subir archivos.

---

![](en/lufi-drop.png?lightbox=1024)
