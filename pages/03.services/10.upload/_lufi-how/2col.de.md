---
title: 'Lufi How'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Wie funktioniert das?

Du kannst Dateien per "Drag and Drop" in den entsprechenden Bereich im Browser ziehen oder Du suchst auf traditionellem Weg nach Deinen Dateien und wählst sie aus. Die Dateien werden dann gepackt, verschlüsselt und zum Server übertragen. Du erhältst zwei Links: einen Download-Link, den Du den von Dir gewählten Empfängern gibst, und einen Lösch-Link, um die Datei zu löschen, wenn Du das möchtest. Du kannst Dir außerdem eine Liste Deiner Dateien ansehen, indem Du auf den Link *"My files"* oben rechts auf jener Seite klickst.

Du musst Dich nicht registrieren, um Dateien hochzuladen.

---

![](en/lufi-drop.png?lightbox=1024)
