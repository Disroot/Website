---
title: 'Salles de chat Xmpp'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Chat-rooms

Avec XMPP, vous pouvez créer et rejoindre des salles privées ou publiques sur le réseau. Les adresses des salles sont simples à mémoriser et ressemblent à des listes de diffusion de type email : *votre_salle*@chat.disroot.org. De plus, vous pouvez définir différents paramètres de confidentialité par salle comme par exemple: si l'historique de la salle doit être stocké sur le serveur, si la salle est publiquement consultable et ouverte à tous, vous pouvez définir des administrateurs et des modérateurs pour la salle et le chiffrement de la salle.  

Vérifiez la [liste globale des salles](https://search.jabber.network/rooms/1) de tous les serveurs qui soumettent volontairement leurs salles publiques au répertoire global.

---

## <br> ##
![](chatrooms.png)
