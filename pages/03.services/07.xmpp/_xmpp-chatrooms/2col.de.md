---
title: 'Xmpp Chatrooms'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Chatrooms

Mit XMPP kannst Du private und öffentliche Räume erstellen oder bestehenden Räumen beitreten. Die Adressen für Räume sind einfach zu merken und gleichen Email-Mailinglisten wie: *Dein_Raum*@chat.disroot.org. Zusätzlich kannst Du für jeden Raum verschiedene Privatsphäre-Einstellungen vornehmen, wie zum Beispiel:
+ Soll der Raum-Verlauf gespeichert werden?
+ Soll der Raum öffentlich gesucht und betreten werden können?
+ Administratoren und Moderatoren bestimmen
+ Soll die Kommunikation verschlüsselt erfolgen?

Sieh Dir die [Globale Raum-Liste](https://search.jabber.network/rooms/1) aller Server an, die freiwillig ihre öffentlichen Räume an Globales Verzeichnis melden.


---

## <br> ##
![](chatrooms.png)
