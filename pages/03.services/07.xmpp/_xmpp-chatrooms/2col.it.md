---
title: 'Xmpp stanze di chat'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---

## Stanze di chat

Con XMPP puoi creare e unirti a chat private o pubbliche nella rete. L'indirizzo della chat è semplice da ricordare e ricorda le mailing list: *la_tua_chat_room*@chat.disroot.org. Inoltre, puoi configurare varie impostazioni di privacy per ogni chat. Ad esempio puoi impostare se la cronologia delle stanze deve essere memorizzata sul server, se la stanza è pubblicamente ricercabile e aperta a chiunque e puoi impostare amministratori e moderatori.  

Guarda la [lista globale delle stanze](https://search.jabber.network/rooms/1) di tutti i server che hanno volontariamente aggiunto le loro stanze pubbliche in questa lista.


---

## <br> ##
![](chatrooms.png)
