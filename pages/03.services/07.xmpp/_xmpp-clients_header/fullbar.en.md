---
title: 'XMPP Clients'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

# Use your favorite chat-client
There are a lot of desktop/web/mobile clients to choose from. Pick the one you like the best.<br>
[https://xmpp.org/software/](https://xmpp.org/software/)
