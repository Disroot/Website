---
title: 'XMPP Clients'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

# Nutze Deinen bevorzugten Chat-Client
Es gibt eine Menge Desktop-/Web-/Mobil-Clients, zwischen denen Du wählen kannst. Such Dir den aus, der Dir am besten gefällt.<br>
[https://xmpp.org/software/](https://xmpp.org/software/)
