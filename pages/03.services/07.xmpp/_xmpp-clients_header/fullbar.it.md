---
title: 'XMPP Clients'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

# Usa il tuo client di chat preferito
Ci sono diversi client per desktop, web e mobile tra cui scegliere. Utilizza quello che preferisci.<br>
[https://xmpp.org/software/](https://xmpp.org/software/)
