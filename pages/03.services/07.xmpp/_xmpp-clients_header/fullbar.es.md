---
title: 'Clientes XMPP'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

# Usar nuestro cliente de chat preferido
Hay un montón de clientes para el escritorio/la web/el móvil para elegir. Podemos elegir el que más nos guste.<br>
[https://xmpp.org/software/](https://xmpp.org/software/)
