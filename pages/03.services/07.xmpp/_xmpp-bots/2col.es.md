---
title: 'Bots XMPP'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Bots

Los bots son cuentas de chat que pueden hacer cosas por las personas. Desde chequear el clima, saludar a quienes recién llegan a una sala, recordar cosas, incluso realizar búsquedas en la web o proporcionar fuentes RSS y una tonelada de cosas útiles y otras más inútiles. Ofrecemos un número de bots multipropósito basados en **hubot** así como también un servidor de bots dedicado llamado `bot.disroot.org`. Si tienen un bot y están buscándole un hogar, comuníquense con nosotros.<br>

Nuestro servidor de bots no guarda ningún historial en él.

---

![](hubot_logo.png)
