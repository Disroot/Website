---
title: 'Cifrado XMPP'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## ¡Cifra todo!

Utilizando los métodos de cifrado de Extremo-a-Extremo OMEMO (GPG u OTR) en el lado del cliente (nuestro lado), los mensajes llegarán al destinatario sin ser interceptados por cualquiera (ni siquiera los administradores).  

---

![](omemo_logo.png)
