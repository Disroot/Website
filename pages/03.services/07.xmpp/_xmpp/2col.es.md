---
title: XMPP
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<br><br><br><br>
<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Inscribirse en Disroot</a>
<a class="button button1" href="https://webchat.disroot.org/">Iniciar sesión en Webchat</a>

---
![](xmpp_logo.png?resize=128,128)

# [XMPP](https://es.wikipedia.org/wiki/Extensible_Messaging_and_Presence_Protocol) - Mensajería instantánea descentralizada y federada

Para comunicarnos utilizando un protocolo de chat estandarizado, abierto y federado, con la capacidad de cifrar las comunicaciones con el protocolo OMEMO (basado en un método de cifrado utilizado también por servicios como Signal y Matrix). Con XMPP las personas no están atadas a un proveedor de servicio (p.ej. el servidor XMPP de Disroot) sino que pueden comunicarse libremente con contactos de otros servidores XMPP, exactamente como lo hacemos entre distintos servidores de correo.

Página del proyecto: [https://xmpp.org](https://xmpp.org)
Servidor de implementación: [https://prosody.im](https://prosody.im)

<a href='https://compliance.conversations.im/server/disroot.org'><img src='https://compliance.conversations.im/badge/disroot.org'></a>
