---
title: 'Xmpp Bridges'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](conversations2.png)

---

## Gateways, Bridges und Transports

XMPP bietet verschiedene Möglichkeiten, sich mit unterschiedlichen Chat-Protokollen zu verbinden.

### Mit einem IRC-Raum verbinden
Über unser Biboumi IRC-Gateway kannst Du Dich mit jedem IRC-Raum verbinden, der auf einem beliebigen IRC-Server gehostet wird:
<ul class=disc>
<li>Um einem IRC-Raum beizutreten: <b><i>#room%irc.domain.tld@irc.disroot.org</i></b></li>
<li> Um einen IRC-Kontakt hinzuzufügen: <b><i>contactname%irc.domain.tld@irc.disroot.org</i></b></li>
</ul>

Dabei steht **#room** für den IRC-Raum, dem Du beitreten möchtest, und **irc.domain.tld** für die Adresse des IRC-Servers, dem Du beitreten möchtest. Stelle sicher, dass Du **@irc.disroot.org** unverändert lässt, da dies die Adresse des IRC-Gateways ist.

### Mit einem Matrix-Raum verbinden
Du kannst Dich über die Matrix.org-Bridge mit jedem Matrix-Raum verbinden, der auf einem beliebigen Matrix-Server gehostet wird:
<ul class=disc>
<li>Um einem Matrix-Raum beizutreten: <b><i>#room#matrix.domain.ldt@matrix.org</i></b></li>
</ul>

Dabei steht **#room** für den Matrix-Raum, dem Du beitreten möchtest, und **matrix.domain.tld** für die Adresse des Matrix-Servers, dem Du  beitreten möchtest. Achte darauf, **@matrix.org** beizubehalten, da das die Matrix-Bridge-Adresse ist.

### Zukünftig...
Für die Zukunft planen wir, eine eigene Matrix-Bridge zu betreiben. Wir hoffen, auch eine Telegram-Bridge und eine Reihe anderer Bridges und Transports anbieten zu können.
