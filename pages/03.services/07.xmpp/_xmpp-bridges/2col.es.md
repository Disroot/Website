---
title: 'Puentes XMPP'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](conversations2.png)

---

## Pasarelas, Puentes y Transportes

XMPP permite varias maneras de conectar a diferentes protocolos de chat.

### Conectar a una sala IRC
Podemos conectarnos a cualquier sala IRC en cualquier servidor a través de la pasarela IRC Biboumi.
<ul class=disc>
<li>Para unirse a una sala IRC: <b><i>#sala%irc.dominio.tld@irc.disroot.org</i></b></li>
<li>Para agregar un contacto IRC: <b><i>nombredecontacto%irc.dominio.tld@irc.disroot.org</i></b></li>
</ul>

Donde **#sala** es la sala IRC a la cual queremos unirnos e **irc.dominio.tld** es la dirección del servidor IRC en la que está hospedada. Hay que asegurse de dejar **@irc.disroot.org** como está, porque es la dirección de la pasarela IRC.

### Conectar a una sala Matrix
Para conectarse a cualquier sala Matrix hospedada en cualquier servidor Matrix a través del puente Matrix.org:
<ul class=disc>
<li>Para unirse a una sala Matrix: <b><i>#sala#matrix.dominio.ldt@matrix.org</i></b></li>
</ul>

Donde **#sala** es la sala Matrix a la que queremos unirnos y **matrix.dominio.tld** es la dirección del servidor Matrix que la hospeda. Hay que asegurse de dejar **@matrix.org** como está, porque es la dirección del puente Matrix.


### Lo que vendrá...
A futuro planeamos correr nuestro propio puente Matrix. También esperamos poder brindar un puente Telegram y un número de otros puentes y transportes.
