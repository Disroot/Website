---
title: 'Almacenamiento extra para el buzón'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Añade espacio a tu buzón
Si 1GB GRATUITO no es suficiente, puedes ampliar el almacenamiento de tu correo.

Estos son los precios **por año, tasas de pagos incluidas**:

||||
|---:|---|---:|
| 5GB |......| 11€ |
| 10GB |......| 20€ |
| 15GB |......| 29€ |
| 30GB |......| 56€ |
| 45GB |......| 83€ |
| 60GB |......| 110€ |


<br>
Las transacciones dentro de la UE están sujetas a un I.V.A (Impuesto al Valor Agregado) adicional de 21%.

---

<br><br>

<a class="button button1" href="/forms/extra-storage-space">Solicitar almacenamiento extra para el buzón</a>
