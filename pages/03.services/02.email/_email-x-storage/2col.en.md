---
title: 'Extra mailbox storage'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Add storage to your mailbox
If the free 1GB is not enough, you can extend your email storage.

Here are the prices **per year** (payment fees included):

||||
|---:|---|---:|
| 5GB |......| 11€ |
| 10GB |......| 20€ |
| 15GB |......| 29€ |
| 30GB |......| 56€ |
| 45GB |......| 83€ |
| 60GB |......| 110€ |


<br>
Transactions made within the EU are subjected to extra VAT of 21%.

---

<br><br>

<a class="button button1" href="/forms/extra-storage-space">Request Extra Mailbox Storage</a>
