---
title: Email
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Sign up for Disroot</a>
<a class="button button1" href="https://webmail.disroot.org/">Log in</a>
<a class="button button1" href="http://mdlwkwn2nhzzuymmyr5mjyoqppoyp46k4remaliu2zrmkayl5yfeh7ad.onion">Tor</a>

---

## E-mail

**Disroot** provides secure email accounts for your desktop or mobile email client, or via our webmail. The communication between your device and the mail server is encrypted with SSL, providing as much privacy as possible. Furthermore, all emails sent out from our server are TLS encrypted if the recipient's email server supports it. This means that emails are no longer sent as a "postcard", but are actually put in an "envelope".

**Nevertheless, we encourage you to be cautious when communicating using email, and to make use of GPG end-to-end encryption to ensure your communication is as secure and private as possible.**
