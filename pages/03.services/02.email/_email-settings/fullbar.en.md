---
title: 'Email Settings'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
##### IMAP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Port <span style="color:#8EB726">993</span> | Authentication: <span style="color:#8EB726">Normal Password</span>
##### SMTP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">STARTTLS</span> Port <span style="color:#8EB726">587</span> | Authentication: <span style="color:#8EB726">Normal Password</span>
##### SMTPS: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">TLS</span> Port <span style="color:#8EB726">465</span> | Authentication: <span style="color:#8EB726">Normal Password</span>
##### POP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Port <span style="color:#8EB726">995</span> | Authentication: <span style="color:#8EB726">Normal Password</span>

---

**Mailbox size limit:** 1 GB
**Attachment size limit:** 64 MB

---

**Delimiters:** The plus sign ("+") can be used as a delimiter in your email address to create sub-addresses, e.g. **username+whatever@disroot.org**. This can be used, for instance, to filter and track spam.
Example: david@disroot.org could have an email address like 'david+bank@disroot.org' that he could give to his bank. It can be used to send and receive emails. These addresses do not need to be manually configured; they are always routed back to the original mailbox.
