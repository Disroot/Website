---
title: 'Parametri di configurazione posta elettronica'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

##### IMAP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Porta <span style="color:#8EB726">993</span> | Autentificazione: <span style="color:#8EB726">Normal Password</span>
##### SMTP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">STARTTLS</span> Porta <span style="color:#8EB726">587</span> | Autentificazione: <span style="color:#8EB726">Normal Password</span>
##### SMTPS: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">TLS</span> Porta <span style="color:#8EB726">465</span> | Autentificazione: <span style="color:#8EB726">Normal Password</span>
##### POP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Porta <span style="color:#8EB726">995</span> | Autentificazione: <span style="color:#8EB726">Normal Password</span>

---

**Limite Mailbox:** 1 GB
**Limite allegati:** 64 MB

---

**Delimitatori:** Puoi utilizzare un delimitatore (segno più, "+") nel tuo indirizzo di posta elettronica per creare sottoindirizzi come **nomeutente+qualunquecosa@disroot.org**. I delimitatori si possono utilizzare ad esempio per filtrare o tracciare lo spam. Esempio: david@disroot.org può usare come indirizzo da dare alla sua banca: 'david+bank@disroot.org'. Può essere utilizzato per inviare e ricevere e-mail.
