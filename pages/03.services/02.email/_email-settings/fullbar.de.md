---
title: 'E-Mail-Einstellungen'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br>
##### IMAP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Port <span style="color:#8EB726">993</span> | Authentifizierung: <span style="color:#8EB726">Passwort, normal</span>
##### SMTP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">STARTTLS</span> Port <span style="color:#8EB726">587</span> | Authentifizierung: <span style="color:#8EB726">Passwort, normal</span>
##### SMTPS: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">TLS</span> Port <span style="color:#8EB726">465</span> | Authentifizierung: <span style="color:#8EB726">Passwort, normal</span>
##### POP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Port <span style="color:#8EB726">995</span> | Authentifizierung: <span style="color:#8EB726">Passwort, normal</span>

---

**maximale Postfachgröße:** 1 GB
**maximale Anhanggröße:** 64 MB

---

**Trennzeichen:** Du kannst ein Pluszeichen ("+") in Deiner E&#8209;Mail-Adresse verwenden, um Unteradressen wie **Benutzername+wasauchimmer@disroot.org** zu erstellen, z.B. zum Filtern und Nachvollziehen von Spam. Beispiel: *david@disroot.org* kann Mail-Adressen wie *david+bank@disroot.org* einrichten, die er seiner Bank geben könnte. Sie kann zum Senden und Empfangen von E&#8209;Mails genutzt werden.
