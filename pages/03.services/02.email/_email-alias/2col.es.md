---
title: 'Email Alias'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
section_id: alias
---

## Vinculación de Dominios
<br>
<a class="button button2" href="https://disroot.org/perks">Solicitar Beneficios</a>

---

<br>
Esta funcionalidad es un beneficio por tu contribución económica al proyecto. Te permite utilizar tu propio dominio para poder enviar y recibir correos. Es un beneficio de por vida.
---

