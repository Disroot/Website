---
title: 'Email Alias'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
section_id: alias
---

## Custom domain linking
<br>
<a class="button button2" href="https://disroot.org/perks">Request Perks</a>

---

<br>
This feature is a lifetime bonus for those who make a financial contribution to the project. It allows you to send and receive emails using your own domain name.
