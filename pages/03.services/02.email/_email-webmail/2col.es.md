---
title: 'Email Webmail'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

## Webmail

![webmail](en/webmail.png)

---

<br>
Acceso al correo electrónico desde cualquier dispositivo, utilizando nuestro cliente web en [https://webmail.disroot.org](https://webmail.disroot.org).


Nuestro webmail está desarrollado por **Roundcube**. Es simple, rápido y proporciona la mayoría de las características que podríamos esperar del correo web:

<ul class=disc>
<li>Un tema agradable y adaptable: un hermoso tema de Disroot con soporte para múltiples dispositivos.</li>
<li>Traducciones: la interfaz está traducida a más de 80 idiomas.</li>
<li>Gestión de mensajes "arrastrar-y-soltar": mueve tus correos con el ratón.</li>
<li>Soporte completo para mensajes MIME y HTML.</li>
<li>Vista previa de adjuntos: ve las imágenes directamente en tus mensajes.</li>
<li>Listado de mensajes en hilos: muestra las conversaciones en forma de hilos jerárquicos.</.</li>
<li>Filtros: configura reglas para mover o copiar correos automáticamente a carpetas específicas, reenviarlos o rechazarlos de acuerdo con el asunto, el remitente, etc.</li>
<li>Carpetas: gestiona tus carpetas (agregar, quitar u ocultar).</li>
</ul>


Página del proyecto: [https://roundcube.net](https://roundcube.net)
