---
title: 'Email Webmail'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

## Webmail

![webmail](en/webmail.png)

---

<br>
You can access your email from any device by using our hosted webmail at [https://webmail.disroot.org](https://webmail.disroot.org)

Our webmail is powered by **Roundcube**. Roundcube is a modern approach to webmail. It's simple, fast and provides most of the features one could expect from webmail:

<ul class=disc>
<li>A nice responsive skin: a beautiful Disroot flavor skin with multi-device support.</li>
<li>Translations: user interface is available in more than 80 languages.</li>
<li>Drag-and-drop message management: easily move your messages with your mouse.</li>
<li>Full support for MIME and HTML messages.</li>
<li>Attachment previews: see images directly within messages.</li>
<li>Threaded message listing: display conversations in a threaded fashion.</li>
<li>Filters: set up rules to automatically move or copy email to specific folders, as well as forwarding or rejecting emails (according to the subject, sender, etc.)</li>
<li>Folder management: add, remove, or hide your folders.</li>
</ul>


Project homepage: [https://roundcube.net](https://roundcube.net)
