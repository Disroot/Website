---
title: 'Email Encryption'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: left
wider_column: right
---

## Что такое шифрование

---

Шифрование - это когда вы изменяете данные с помощью специального процесса кодирования, так что данные становятся неузнаваемыми (они зашифрованы). Затем вы можете применить специальный процесс декодирования, и вы получите исходные данные обратно. Сохраняя ключ декодирования в секрете, никто другой не сможет восстановить исходные данные из наших зашифрованных данных.

[Видео о том, как работает ассиметричное шифрование (EN)](https://www.youtube.com/watch?v=E5FEqGYLL0o)

<br>
![mailvelope](en/mailvelope.svg?resize=150)
You can use Mailvelope, a browser add-on in Chrome, Edge and Firefox, that securely encrypts your emails with PGP using Disroot webmail. Check this [howto](https://mailvelope.com/en/help).