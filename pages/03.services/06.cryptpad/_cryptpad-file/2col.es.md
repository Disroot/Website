---
title: 'Almacenamiento de Archivos'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](drive.png)

## Almacenamiento cifrado de archivos
Subir y compartir cualquier archivo. Todos los archivos almacenados están cifrados de extremo-a-extremo.



---
![](code.png)

## Editor de código colaborativo
Para editar código junto a colegas de equipo mientras está cifrado de extremo-a-extremo con conocimiento cero por parte del servidor.
