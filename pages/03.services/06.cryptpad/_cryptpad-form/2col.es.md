---
title: 'Formularios'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](form.png)

## Formularios cifrados de extremo-a-extremo
**Formularios** ofrece la posibilidad de crear y compartir formularios completamente cifrados.


---
![](presentation.png)

## Diapositivas Markdown cifradas sobre la marcha
Se pueden crear presentaciones cifradas de extremo-a-extremo con un editor sencillo junto a amistades o personas de nuestra organización o trabajo. La presentación terminada puede ser "reproducida" directamente desde **CryptPad**.
