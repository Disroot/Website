---
title: 'Form'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](form.png)

## Moduli
**Form** ti offre la possibilità di creare e condividere moduli crittografati completamente end-to-end .


---
![](presentation.png)

## Diapositives Markdown
Crea presentazioni crittografate end-to-end con un semplice editor e insieme ai tuoi amici o membri del team. La tua presentazione finita può essere "riprodotta" direttamente dal CryptPad. 
