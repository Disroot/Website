---
title: 'Kanban'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](todo.png)

## Encrypted simple Todo lists and kanban boards
Easily create, share and manage your encrypted TODO lists and your project kanban boards!


---
![](whiteboard.png)

## Whiteboards
Encrypted white boards allow you to draw in realtime together with others. All end-to-end encrypted!
