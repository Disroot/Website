---
title: 'Kanban'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](todo.png)

## Semplici liste di cose da fare e bacheche kanban crittografate 
Crea, condividi e gestisci facilmente le tue liste TODO crittografate e le bacheche kanban del tuo progetto! 

---
![](whiteboard.png)

## Lavagne
Le lavagne bianche crittografate ti consentono di disegnare in tempo reale insieme ad altri. Tutto crittografato end-to-end! 
