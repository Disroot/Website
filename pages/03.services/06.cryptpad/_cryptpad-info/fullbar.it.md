---
title: 'Settings'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
Ogni utente riceve **500MB MB di spazio di archiviazione** per i file. 

---

**Sebbene CryptPad non richieda un account, puoi crearne uno se desideri conservare tutti i tuoi pad, file e sondaggi in un unico posto. Dovrai creare un account separato per CryptPad. Il tuo account disroot non funzionerà!** 

---
