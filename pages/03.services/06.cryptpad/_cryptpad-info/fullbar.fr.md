---
title: 'Paramètres'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
Chaque utilisateur dispose de **500MB d'espace de stockage** pour les fichiers.

---

**Bien que CryptPad ne nécessite pas de compte, vous pouvez en créer un si vous souhaitez conserver tous vos blocs, fichiers et sondages au même endroit. Vous devrez créer un compte séparé pour le CryptPad. Votre compte Disroot ne fonctionnera pas !

---
