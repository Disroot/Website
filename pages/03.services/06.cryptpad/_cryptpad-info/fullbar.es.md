---
title: 'CryptPad info'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
Cada usuarie obtiene **500MB de espacio de almacenamiento** para archivos.

---

**Aunque CryptPad no requiere una cuenta, se puede crear una si queremos conservar documentos, archivos y encuestas en un solo sitio. Es necesario crear una cuenta separada para CryptPad. La cuenta de Disroot no funcionará.**

---
