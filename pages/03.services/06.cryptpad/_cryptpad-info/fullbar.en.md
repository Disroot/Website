---
title: 'Settings'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
Every user gets **500MB storage space** for files.

---

**Although CryptPad does not require an account, you can create one if you want to keep all your pads, files, and polls in one place. You will need to create a separate account for CryptPad. Your disroot account won't work!**

---
