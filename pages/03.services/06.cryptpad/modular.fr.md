---
title: CryptPad
bgcolor: '#8EB726'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _cryptpad
            - _cryptpad-info
            - _cryptpad-x-storage
            - _cryptpad-highlights
            - _cryptpad-pads
            - _cryptpad-form
            - _cryptpad-file
            - _cryptpad-kanban
            - _cryptpad-diagram
            - _green-bar

body_classes: modular
header_image: 'stop_watching_all_of_us.jpg'

translation_banner:
    set: true
    last_modified: Avril 2024
    language: French
---
