---
title: 'Documentos'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](pads.png)

## Documentos de texto enriquecido
El editor de **texto enriquecido** permite crear archivos de texto cifrados de extremo-a-extremo con la característica de colaboración en tiempo real. El editor admite formato estándar. Todos los documentos pueden ser editados por diferentes personas al mismo tiempo.


---
![](sheet.png)

## Hoja de cálculo en tiempo real
Las **hojas de cálculo en tiempo real** brindan la posibilidad de crear planillas colaborativas. La aplicación soporta todos los formatos de texto estándar (LibreOffice/MS Office). Todo ello desarrollado por *"Only Office"* y permitiendo la edición grupal.
