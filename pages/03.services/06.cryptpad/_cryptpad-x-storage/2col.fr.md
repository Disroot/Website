---
title: 'Espace de stockage supplémentaire pour CryptPad'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Ajoutez du stockage à CryptPad
Si les 500MB GRATUITS ne suffisent pas, vous pouvez étendre le stockage de CryptPad.

Voici les prix **par an, frais de paiement inclus** :

||||
|---:|---|---:|
| 5Go |......| 11€ |
| 10Go |......| 20€ |
| 15Go |......| 29€ |
| 30Go |......| 56€ |
| 45Go |......| 83€ |
| 60Go |......| 110€ |


<br>
Les transactions au sein de l'UE sont soumises à une TVA (taxe sur la valeur ajoutée) supplémentaire de 21 %.

---

<br><br>

<a class="button button1" href="/forms/extra-storage-space">Demande de stockage supplémentaire pour CryptPad</a>
