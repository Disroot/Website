---
title: 'Spazio extra per CryptPad'
bgcolor: '#FFF'
fontcolor: '#7A7A7A'
wider_column: left
---

## Aggiungi spazio di archiviazione per CryptPad

Se 500MB di spazio gratuito non è abbastanza, puoi estendere la tua memoria di CryptPad.

Ecco i prezzi **all'anno, spese di pagamento incluse**:

||||
|---:|---|---:|
| 5GB |......| 11€ |
| 10GB |......| 20€ |
| 15GB |......| 29€ |
| 30GB |......| 56€ |
| 45GB |......| 83€ |
| 60GB |......| 110€ |

<br>
Le transazioni all'interno dell'UE sono soggette a un'IVA aggiuntiva (imposta sul valore aggiunto) del 21%.

---

<br><br>

<a class="button button1" href="/forms/extra-storage-space">Richiedi ulteriore spazio di archiviazione per CryptPad</a>

