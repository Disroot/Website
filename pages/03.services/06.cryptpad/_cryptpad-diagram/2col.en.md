---
title: 'Diagram'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](diagram.png)

## Encrypted diagramming tool
The **Diagram** application in CryptPad is an integration of *Draw.io*, a diagramming tool used to create and share flowcharts, organizational charts, network diagrams, and more.

---