---
title: 'Diagram'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](diagram.png)

## Strumento di diagramma criptato
L'applicazione **Diagram** di CryptPad è un'integrazione di *Draw.io*, uno strumento di diagramma utilizzato per creare e condividere diagrammi di flusso, organigrammi, diagrammi di rete e altro ancora.

---