---
title: 'Diagram'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](diagram.png)

## Herramienta de diagramación cifrada
La aplicación **Diagram** de CryptPad es una integración de *Draw.io*, una herramienta de diagramación utilizada para crear y compartir diagramas de flujo, organigramas, diagramas de red, etc.

---