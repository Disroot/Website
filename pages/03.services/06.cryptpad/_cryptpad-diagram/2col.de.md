---
title: 'Diagram'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](diagram.png)

## Verschlüsseltes Diagrammwerkzeug
Die **Diagramm-Anwendung** in CryptPad ist eine Integration von *Draw.io*, einem Diagramm-Tool zur Erstellung und Freigabe von Flussdiagrammen, Organigrammen, Netzwerkdiagrammen und mehr.

---