---
title: 'Diagram'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](diagram.png)

## Outil de diagramme crypté
L'application **Diagramme** de CryptPad est une intégration de *Draw.io*, un outil de création de diagrammes utilisé pour créer et partager des organigrammes, des diagrammes organisationnels, des diagrammes de réseau, et plus encore.

---