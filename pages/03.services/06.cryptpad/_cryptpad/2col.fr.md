---
title: CryptPad
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://cryptpad.disroot.org/register/">S'enregistrer</a>
<a class="button button1" href="https://cryptpad.disroot.org/">Créer des Pads</a>
<a class="button button1" href="http://jatqcuw3s2earcfzphl224ua3i7j2n76vid4vegguiaaavr5z46q4aad.onion">Tor</a>

---
![](cryptpad_logo.png)

## CryptPad
Le CryptPad de Disroot est propulsé par CryptPad. Il fournit une suite bureautique collaborative totalement chiffrée de bout en bout. Il vous permet de créer, de partager et de travailler ensemble sur des documents texte, des feuilles de calcul, des présentations, des tableaux blancs ou d'organiser votre projet sur un tableau kanban. Tout cela sans laisser d'informations sur les fichiers puisqu'ils sont chiffrées avant qu'elles ne quittent votre ordinateur.

CryptPad Disroot: [cryptpad.disroot.org](https://cryptpad.disroot.org)

Page d'accueil du projet: [https://cryptpad.org](https://cryptpad.org)

Code source: [https://github.com/cryptpad/cryptpad](https://github.com/cryptpad/cryptpad)
