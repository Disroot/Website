---
title: CryptPad
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://cryptpad.disroot.org/register/">Sign up</a>
<a class="button button1" href="https://cryptpad.disroot.org/">Create Pads</a>
<a class="button button1" href="http://jatqcuw3s2earcfzphl224ua3i7j2n76vid4vegguiaaavr5z46q4aad.onion">Tor</a>

---
![](cryptpad_logo.png)

## CryptPad
Disroot's CryptPad is powered by CryptPad. It provides a totally end-to-end encrypted collaborative office suite. It allows you to create, share and work together on text documents, spreadsheets, presentations, whiteboards or organize your project on a kanban board. All this with zero knowledge where data is encrypted before it leaves your computer.

Disroot CryptPad: [cryptpad.disroot.org](https://cryptpad.disroot.org)

Project homepage: [https://cryptpad.org](https://cryptpad.org)

Source code: [https://github.com/cryptpad/cryptpad](https://github.com/cryptpad/cryptpad)
