---
title: CryptPad
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://cryptpad.disroot.org/register/">Sign up</a>
<a class="button button1" href="https://cryptpad.disroot.org/">Create Pads</a>
<a class="button button1" href="http://jatqcuw3s2earcfzphl224ua3i7j2n76vid4vegguiaaavr5z46q4aad.onion">Tor</a>

---
![](cryptpad_logo.png)

## CryptPad

CryptPad di Disroot è alimentato da CryptPad. Fornisce una suite per ufficio collaborativa e crittografata completamente end-to-end. Ti consente di creare, condividere e lavorare insieme su documenti di testo, fogli di calcolo, presentazioni, lavagne o organizzare il tuo progetto su una lavagna kanban.

Disroot CryptPad: [cryptpad.disroot.org](https://cryptpad.disroot.org)

Homepage del progetto: [https://cryptpad.org](https://cryptpad.org)

Codice sorgente: [https://github.com/cryptpad/cryptpad](https://github.com/cryptpad/cryptpad)
