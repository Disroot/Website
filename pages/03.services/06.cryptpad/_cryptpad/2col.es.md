---
title: CryptPad
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://cryptpad.disroot.org/register/">Inscribirse</a>
<a class="button button1" href="https://cryptpad.disroot.org/">Crear Documento</a>
<a class="button button1" href="http://jatqcuw3s2earcfzphl224ua3i7j2n76vid4vegguiaaavr5z46q4aad.onion">Tor</a>

---
![](cryptpad_logo.png)

## CryptPad
**CryptPad** de **Disroot** está desarrollado por **CryptPad** y proporciona una suite colaborativa de ofimática totalmente [cifrada de extremo-a-extremo](https://es.wikipedia.org/wiki/Cifrado_de_extremo_a_extremo). Permite crear, compartir y trabajar con otras personas en documentos de texto, planillas, presentaciones, pizarras, incluso organizar proyectos en un tablero [Kanban](https://es.wikipedia.org/wiki/Kanban_(desarrollo)). Todo ello con "[conocimiento cero](https://blog.cryptpad.org/2017/03/24/What-is-Zero-Knowledge/)", ya que los datos son cifrados antes de dejar nuestra computadora.

CryptPad de Disroot: [cryptpad.disroot.org](https://cryptpad.disroot.org)

Sitio del Proyecto: [https://cryptpad.org](https://cryptpad.org)

Código fuente: [https://github.com/cryptpad/cryptpad](https://github.com/cryptpad/cryptpad)
