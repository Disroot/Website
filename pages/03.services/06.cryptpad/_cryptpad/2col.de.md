---
title: CryptPad
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://cryptpad.disroot.org/register/">Anmelden</a>
<a class="button button1" href="https://cryptpad.disroot.org/">Pads erstellen</a>
<a class="button button1" href="http://jatqcuw3s2earcfzphl224ua3i7j2n76vid4vegguiaaavr5z46q4aad.onion">Tor</a>

---
![](cryptpad_logo.png)

## CryptPad
Disroot's CryptPad wird bereitgestellt von CryptPad. Es stellt eine vollständig Ende-zu-Ende verschlüsselte, kollaborative Office Suite zur Verfügung. Es erlaubt Dir, Textdokumente, Tabellenkalkulationen, Präsentationen oder Whiteboards zu erstellen, zu teilen oder gemeinsam zu bearbeiten. Du kannst Dein Projekt auch mit einem Kanban-Board organisieren. All das vollkommen ohne eine mögliche Einsicht des Service-Providers, da die Daten verschlüsselt werden, bevor sie Deinen Computer verlassen.

Disroot CryptPad: [cryptpad.disroot.org](https://cryptpad.disroot.org)

Projekt Website: [https://cryptpad.org](https://cryptpad.org)

Quellcode: [https://github.com/cryptpad/cryptpad](https://github.com/cryptpad/cryptpad)
