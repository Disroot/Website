---
title: 'Cliente web de Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

![](en/webclient.png)

---

## Acceder a Nextcloud desde cualquier lugar

**Nextcloud** es accesible desde cualquier dispositivo a través del navegador web. Por lo tanto, podemos usar todas las aplicaciones del servidor directamente desde nuestra computadora sin instalar nada excepto un navegador.
