---
title: 'Nextcloud Webclient'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

![](en/webclient.png)

---

## Accedi a Nextcloud ovunque

**Nextcloud** è accessibile da qualsiasi dispositivo tramite browser. Puoi quindi utilizzare tutte le applicazioni presenti sul server direttamente dal tuo computer, senza installare altro che un browser. 
