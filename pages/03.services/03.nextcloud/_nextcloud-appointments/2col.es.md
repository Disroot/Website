---
title: 'Citas'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-appointments.jpg?lightbox=1024)

---

## Citas

La aplicación **Citas** agiliza la planificación dentro de la plataforma, permitiendo crear y compartir citas fácilmente. Con franjas horarias y recordatorios personalizables, optimiza la gestión del tiempo de personas y equipos, mejorando la productividad y la colaboración.