---
title: 'Appointments'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-appointments.jpg?lightbox=1024)

---

## Appointments

**Appointments** app streamlines scheduling within the platform, allowing easy creation and sharing of appointments. With customizable time slots and reminders, it optimizes time management for individuals and teams, enhancing productivity and collaboration.