---
title: 'Appointments'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-appointments.jpg?lightbox=1024)

---

## Rendez-vous

L'application **Rendez-vous** rationalise la planification au sein de la plateforme, en permettant de créer et de partager facilement des rendez-vous. Avec des plages horaires personnalisables et des rappels, elle optimise la gestion du temps pour les individus et les équipes, améliorant ainsi la productivité et la collaboration.