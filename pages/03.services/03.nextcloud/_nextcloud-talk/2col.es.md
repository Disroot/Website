---
title: 'Talk'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Elige tu cliente favorito'
clients:
    -
        title: Talk
        logo: en/talk_logo.png
        link: https://f-droid.org/en/packages/com.nextcloud.talk2/
        text:
        platforms: [fa-android]

---
## Talk

Llamadas en conferencia de audio/video. Para comunicarnos de forma segura con nuestras amistades o familia utilizando audio y video enriquecidos desde el navegador. Todas las llamadas son [peer 2 peer](https://es.wikipedia.org/wiki/Peer-to-peer), utilizando tecnología [WebRTC](https://es.wikipedia.org/wiki/WebRTC), así que el único atasco que podría producirse sería por la velocidad de nuestra internet local antes que por los recursos del servidor de **Disroot**.

Se puede crear un enlace público para compartir con personas que no tienen una cuenta de **Nextcloud**.

---

![](en/call-in-action.png?lightbox=1024)
