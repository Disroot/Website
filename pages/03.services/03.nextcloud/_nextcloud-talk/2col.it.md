---
title: 'Talk'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Scegli il tuo client preferito'
clients:
    -
        title: Talk
        logo: en/talk_logo.png
        link: https://f-droid.org/en/packages/com.nextcloud.talk2/
        text:
        platforms: [fa-android]

---
## Talk

Possibilità di effettuare delle audio/video conferenze sicure direttamente attraverso il browser. Tutte le chiamate sono peer2peer e utilizzano la teconologia WebRTC. Unico eventuale problema potrebbe essere legato alla grandezza della banda della tua connessione ed eventualmente delle risorse di **Disroot**.
È possibile creare un link pubblico da condividere con persone che non hanno un account **Nextcloud**.

---

![](en/call-in-action.png?lightbox=1024)
