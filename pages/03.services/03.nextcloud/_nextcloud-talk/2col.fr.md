---
title: 'Talk'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Choisissez votre client préféré'
clients:
    -
        title: Talk
        logo: en/talk_logo.png
        link: https://f-droid.org/en/packages/com.nextcloud.talk2/
        text:
        platforms: [fa-android]

---
## Talk

Conférences téléphoniques audio/vidéo. Communiquez en toute sécurité avec vos amis et votre famille à l'aide du son et de la vidéo de votre navigateur.  Tous les appels sont pair à pair et en utilisant la technologie WebRTC de sorte que le seul obstacle est votre vitesse Internet locale plutôt que les ressources du serveur de **Disroot**.

Vous pouvez créer un lien public que vous pouvez partager avec les personnes qui n'ont pas de compte **Nextcloud**.

---

![](en/call-in-action.png?lightbox=1024)
