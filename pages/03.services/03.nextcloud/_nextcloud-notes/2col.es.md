---
title: 'Notas'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Elige tu cliente favorito'
clients:
    -
        title: Notas
        logo: en/notes_logo.png
        link: https://f-droid.org/en/packages/it.niedermann.owncloud.notes/
        text:
        platforms: [fa-android]
---

![](en/nextcloud-notes.png?lightbox=1024)

---

## Notas

Crear notas, compartirlas y sincronizarlas con todos nuestros dispositivos.
