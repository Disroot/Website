---
title: 'Notizen'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Wähle Deinen bevorzugten Client'
clients:
    -
        title: Notes
        logo: en/notes_logo.png
        link: https://f-droid.org/de/packages/it.niedermann.owncloud.notes/
        text:
        platforms: [fa-android]
---

![](en/nextcloud-notes.png?lightbox=1024)

---

## Notizen

Erstelle Notizen und teile sie oder synchronisiere zwischen all Deinen Geräten.
