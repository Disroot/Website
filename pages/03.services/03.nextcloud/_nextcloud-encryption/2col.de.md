---
title: 'Nextcloud Verschlüsselung'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/NC_encryption.png?lightbox=1024)

---

## Verschlüsselung:

All Deine Dateien in der **Disroot**-Cloud sind mit Kryptoschlüsseln verschlüsselt, die aus Deinem Passwort generiert werden. Niemand, nicht mal die Systemadministratoren, kann den Inhalt Deiner Dateien sehen. Die Administratoren können nur die Dateinamen, die Dateigröße und den mime-Typ (welcher Art die Datei ist: Video, Text, etc.) sehen.

###### <span style="color:red">!!! Wichtiger Hinweis !!!</span>
**Falls Du Dein Passwort vergisst, hast Du keine Chance mehr, auf Deine Dateien zuzugreifen, es sei denn, Du hast vorher die "Password Recovery"-Option in Deinen persönlichen Einstellungen ausgewählt. Das wiederum bedeutet, dass Du den Administratoren eine generelle Erlaubnis zum Zugriff auf Deine Dateien erteilst. Am sichersten ist es, Du verlierst niemals Dein Passwort und sicherst Deine Dateien als Backup noch an mindestens einem weiteren Ort.**

*Sieh Dir zu dem Thema dieses [Tutorial](https://howto.disroot.org/de/tutorials/user/account/administration) an.*

**Nextcloud** nutzt lediglich serverseitige Verschlüsselung. Das heißt, dass Dein Schlüsselpaar (privat und öffentlich) für die Verschlüsselung auf dem Server gespeichert werden und potentiell gefährdet sein können durch Brute Force- oder MITM (Man in the Middle)-Attacken, falls der Server kompromittiert oder konfisziert wird.
