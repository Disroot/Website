---
title: 'Cifrado Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/NC_encryption.png?lightbox=1024)

---

## Cifrado:

Todos los archivos que son almacenados en la nube de **Disroot** están cifrados con claves generadas a partir de nuestra contraseña. Nadie, ni siquiera los administradores del sistema, puede ver el contenido de estos archivos, solo pueden ver el nombre del archivo, su tamaño y su mime-type (el tipo de archivo que es: video, texto, etc).

###### <span style="color:red">¡¡¡ADVERTENCIA IMPORTANTE!!!</span>
**En caso que olviden su contraseña no podrán recuperar sus archivos a menos que hayan seleccionado previamente la opción "Recuperación de contraseña" en la configuración "Personal". Sin embargo, esto implica dar permiso a los administradores para acceder a sus archivos. La manera más segura es nunca perder la contraseña y siempre tener un respaldo de sus archivos en algún otro lugar.**

*Por favor, consultar [este](https://howto.disroot.org/es/tutorials/user/account/administration) tutorial.*

**Nextcloud** emplea solo cifrado del lado del servidor. Esto significa que sus par de claves de cifrado (privada y pública) son almacenadas en el servidor y son vulnerables a [ataques de fuerza bruta](https://es.wikipedia.org/wiki/Ataque_de_fuerza_bruta) o [ataques de intermediario](https://es.wikipedia.org/wiki/Ataque_de_intermediario) en caso que el servidor se viera comprometido o fuera confiscado.
