---
title: 'Nextcloud Encryption'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/NC_encryption.png?lightbox=1024)

---

## Encryption:

All your files that are stored on the **Disroot** cloud are encrypted with encryption keys generated from your password. No-one, not even system administrators can see the content of your files. The administrators can only see the file's name, its size and its mime-type (what kind of file it is: video, text, etc).

###### <span style="color:red">!!! IMPORTANT WARNING !!!</span>
**In case you forget your password you won't be able to recover your files unless you have selected "Password recovery option" in your "Personal" settings. This however means that you give admins a general permission to access your files. The safest way is to never loose your password and always have your files backed up elsewhere.**

*Please refer to [this](https://howto.disroot.org/en/tutorials/user/account/administration) tutorial.*

**Nextcloud** uses only server side encryption. This means your encryption key pair (private and public) are stored on the server and are vulnerable to bruteforce attacks or MITM (man in the middle attacks) in case the server is compromised or confiscated.
