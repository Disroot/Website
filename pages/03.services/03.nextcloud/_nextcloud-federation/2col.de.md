---
title: 'Nextcloud Föderation'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---

## Föderation

Dank des föderativen Aufbaus kannst Du Deine Verzeichnisse mit jedem verlinken oder teilen, der irgendeine **Nextcloud** (oder **ownCloud**)-Instanz außerhalb von **Disroot** nutzt.

---

![](en/NC_federation.png?lightbox=1024)
