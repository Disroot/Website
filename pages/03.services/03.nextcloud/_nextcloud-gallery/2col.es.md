---
title: 'Galerías Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-gallery.png?lightbox=1024)

---

## Galerías

Para compartir galerías de fotos con amistades y familiares. Se puede otorgar acceso para subir imágenes, verlas y descargarlas. Podemos enviar un link a quien queramos, y controlar si pueden compartir esas fotos con otras personas.
