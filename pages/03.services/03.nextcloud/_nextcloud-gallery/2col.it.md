---
title: 'Gallerie di Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-gallery.png?lightbox=1024)

---

## Gallerie

Condividi delle gallerie di foto con i tuoi amici e i tuoi famigliari. Dai loro accesso cosicchè possano caricare, vedere e scaricare le fotografie.
