---
title: 'Deck'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Wähle Deinen bevorzugten Client'
clients:
    -
        title: Deck
        logo: en/deck_logo.png
        link: https://f-droid.org/de/packages/it.niedermann.nextcloud.deck/
        text:
        platforms: [fa-android]
---

![](en/nextcloud-deck.png?lightbox=1024)


---

## Deck

Deck hilft Dir, Deine persönliche oder Deine Team-Arbeit in **Nextcloud** zu organisieren. Du kannst Aufgaben hinzufügen, sie gliedern, teilen und kommentieren.
