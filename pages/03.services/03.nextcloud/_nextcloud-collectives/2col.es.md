---
title: 'Cuadernos'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: right
wider_column: left
---

## Cuadernos

La aplicación **Cuadernos** permite a las personas usuarias crear, editar y organizar contenido colectivamente, convirtiéndola en un gran recurso para construir una base de conocimiento compartida.

---

![](en/nextcloud-collectives.jpg?lightbox=1024)