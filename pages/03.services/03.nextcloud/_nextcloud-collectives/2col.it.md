---
title: 'Collectives'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: right
wider_column: left
---

## Collectives

L'app **Collectives** permette agli utenti di creare, modificare e organizzare contenuti in modo collaborativo, rendendola una risorsa ideale per costruire una base di conoscenza condivisa.

---

![](en/nextcloud-collectives.jpg?lightbox=1024)
