---
title: 'Collectives'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: right
wider_column: left
---

## Kollektive

Die App **Kollektive** ermöglicht die gemeinsame Erstellung, Bearbeitung und Organisation von Inhalten und ist damit eine praktische Lösung für den Aufbau einer gemeinsamen Wissensplattform.

---

![](en/nextcloud-collectives.jpg?lightbox=1024)