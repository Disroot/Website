---
title: 'Collectives'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: right
wider_column: left
---

## Collectives

L'application **Collectives** permet aux utilisateurs de créer, d'éditer et d'organiser du contenu de manière collaborative, ce qui en fait une excellente ressource pour la construction d'une base de connaissances partagée.

---

![](en/nextcloud-collectives.jpg?lightbox=1024)