---
title: 'Collectives'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: right
wider_column: left
---

## Collectives

**Collectives** app allows users to collaboratively create, edit, and organize content, making it a great resource for building a shared knowledge base.

---

![](en/nextcloud-collectives.jpg?lightbox=1024)