---
title: 'Nextcloud Teams'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-teams.png?lighbox=1024)

---

## Teams

Create your own groups of users/colleagues/friends to easily share content with. Your Teams can be public, private (require invite though visible to others) or secret (require password and invisible).
