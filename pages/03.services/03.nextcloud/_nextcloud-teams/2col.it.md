---
title: 'Nextcloud Teams'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-teams.png?lighbox=1024)

---

## Teams

Crea i tuoi gruppi di utenti, colleghi o amici per condividere facilmente contenuti. I tuoi Team possono essere pubblici, privati (richiedono un invito ma sono visibili agli altri) o segreti (richiedono una password e sono invisibili).




