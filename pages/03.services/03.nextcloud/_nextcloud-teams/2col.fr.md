---
title: 'Nextcloud Teams'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-teams.png?lighbox=1024)

---

## Teams

Créez vos propres groupes d'utilisateurs/collègues/amis pour partager facilement du contenu. Vos "Teams" peuvent être publics, privés (nécessite d'inviter bien que visible aux autres) ou secrets (nécessite un mot de passe et l'option invisible).
