---
title: 'Nextcloud Teams'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-teams.png?lighbox=1024)

---

## Teams

Erstelle Deine eigenen Gruppen von Nutzern/Kollegen/Freunden, um mit ihnen Inhalt einfach zu teilen. Deine Teams können öffentlich, privat (für andere sichtbar, benötigen jedoch eine Einladung) oder geheim (sind unsichtbar und benötigen ein Passwort) sein.
