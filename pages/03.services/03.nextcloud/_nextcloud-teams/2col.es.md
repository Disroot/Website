---
title: 'Nextcloud Teams'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-teams.png?lighbox=1024)

---

## Teams

Para crear nuestros propios grupos de usuaries/colegas/amigas y amigos y compartir fácilmente con ellos. Los equipos pueden ser públicos, privados (requiere invitación aunque es visible para otras personas) o secreto (requiere contraseña y es invisible).
