---
title: 'Nextcloud Sync'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Wähle Deinen bevorzugten Client'
clients:
    -
        title: Desktop
        logo: en/nextcloud_logo.png
        link: https://nextcloud.com/install/#install-clients
        text:
        platforms: [fa-linux, fa-windows, fa-apple]
    -
        title: Mobile
        logo: en/nextcloud_logo.png
        link: https://nextcloud.com/install/#install-clients
        text:
        platforms: [fa-android, fa-apple]
    -
        title: Webbrowser
        logo: en/webbrowser.png
        link: https://cloud.disroot.org
        text: 'Direkter Zugriff mit Deinem Browser'
        platforms: [fa-linux, fa-windows, fa-apple]

---

## Benutze Deinen bevorzugten Client

Greife auf jeder Plattform auf Deine Daten zu und organisiere sie. Benutze die **Desktop**-, **Android**- oder **iOS**-Clients oder die Weboberfläche, um mit Deinen Dateien zu arbeiten. Zuhause oder unterwegs. Synchronisiere Deine bevorzugten Verzeichnisse zwischen Deinen Geräten.

---

![](en/nextcloud-files.png?lightbox=1024)
