---
title: 'Nextcloud Sync'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Elige tu cliente favorito'
clients:
    -
        title: Escritorio
        logo: en/nextcloud_logo.png
        link: https://nextcloud.com/install/#install-clients
        text:
        platforms: [fa-linux, fa-windows, fa-apple]
    -
        title: Móvil
        logo: en/nextcloud_logo.png
        link: https://nextcloud.com/install/#install-clients
        text:
        platforms: [fa-android, fa-apple]
    -
        title: Navegador
        logo: en/webbrowser.png
        link: https://cloud.disroot.org
        text: 'Accede directamente desde tu navegador'
        platforms: [fa-linux, fa-windows, fa-apple]

---

## Sincronización Nextcloud

Para acceder y organizar nuestra información desde cualquier plataforma. Podemos utilizar los clientes para el **Escritorio**, **Android** o **iOS** para trabajar con nuestros archivos sin parar, o sincronizar continuamente nuestras carpetas favoritas entre la computadora de escritorio y la laptop.

---

![](en/nextcloud-files.png?lightbox=1024)
