---
title: Nextcloud
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Inscribirse a **Disroot**</a>
<a class="button button1" href="https://cloud.disroot.org/">Iniciar sesión</a>
<a class="button button1" href="http://3rhtbo7bb3o5qvx2iyirppxxtpwnlxkavdyjdckmuzh2uohibignufqd.onion">Tor</a>

---
![](Nextcloud_Logo.png)

El servicio en la **Nube** de **Disroot** está desarrollado por **Nextcloud**. Permite alojar y compartir nuestros archivos en línea y tiene otras funcionalidades como compartir calendarios, gestión de contactos, video llamadas, entre muchas más.

**Nextcloud** ofrece una solución para compartir, segura, confiable y conforme a estándares, compatible con cualquier sistema operativo.
Lo más importante es que ¡toda la información es almacenada en nuestra instancia cifrada!. Esto significa que nadie puede ver el contenido de nuestros archivos a menos que le demos permiso explícitamente. Ni siquiera los administradores del sitio.

Nube de Disroot: [https://cloud.disroot.org](https://cloud.disroot.org)

Página del proyecto: [https://nextcloud.com](https://nextcloud.com)

Código fuente: [https://github.com/nextcloud/server](https://github.com/nextcloud/server)
