---
title: 'Cookbook'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: right
wider_column: left
---

## Cookbook

Imagine having a personal recipe hub seamlessly integrated into your Nextcloud platform – that's exactly what the **Cookbook** app offers. It's your go-to tool for organizing, categorizing, and exchanging your favorite recipes with ease. It has intuitive features like ingredient lists, step-by-step cooking instructions, and the ability to upload images.

---

![](en/nextcloud-cookbook.jpg?lightbox=1024)