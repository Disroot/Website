---
title: 'Recetario'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: right
wider_column: left
---

## Recetario

Imagina tener un centro personal de recetas perfectamente integrado en tu plataforma Nextcloud: eso es exactamente lo que ofrece la aplicación **Recetario**. Es tu herramienta para organizar, categorizar e intercambiar tus recetas favoritas con facilidad. Cuenta con funciones intuitivas como listas de ingredientes, instrucciones de cocina paso a paso y la posibilidad de subir imágenes.

---

![](en/nextcloud-cookbook.jpg?lightbox=1024)