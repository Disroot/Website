---
title: 'Nextcloud Prüfsumme'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---

## Prüfsummen-App

Erstelle eine Hash-Prüfsumme einer Datei.
Mögliche Algorithmen: md5, sha1, sha256, sha384, sha512 und crc32.

---

![](en/checksum.gif)
