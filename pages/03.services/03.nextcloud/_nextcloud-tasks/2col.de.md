---
title: 'Nextcloud Aufgaben'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Aufgaben

Organisiere, verwalte und synchronisiere Deine Aufgaben auf allen Geräten.

---

![](en/OCtasks.png?lightbox=1024)
