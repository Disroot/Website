---
title: 'Tareas Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Tareas

Organizar, gestionar y sincronizar tareas entre todos nuestros dispositivos.

---

![](en/OCtasks.png?lightbox=1024)
