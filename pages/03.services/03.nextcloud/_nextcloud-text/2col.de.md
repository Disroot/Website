---
title: 'Nextcloud Text'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Text

Erstelle Markdown Dateien und bearbeite sie direkt online in einer Live-Vorschau. Du kannst sie auch teilen und entscheiden, ob andere sie bearbeiten dürfen.

---

![](en/nextcloud-text.png?lightbox=1024)
