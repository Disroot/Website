---
title: 'Nextcloud Text'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Texto

Para crea archivos [Markdown](https://es.wikipedia.org/wiki/Markdown), y editarlos directamente en línea con vista previa en vivo. También se pueden compartir y hacer que cualquiera pueda editarlos.

---

![](en/nextcloud-text.png?lightbox=1024)
