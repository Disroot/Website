---
title: 'Calendriers et contacts'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Choisissez votre client préféré'
clients:
    -
        title: GOA
        logo: en/goa.png
        link: https://wiki.gnome.org/Projects/GnomeOnlineAccounts
        text: 'Gnome online accounts'
        platforms: [fa-linux]
    -
        title: Kaddressbook
        logo: en/KDE.png
        link: https://kde.org/applications/office/org.kde.kaddressbook
        text:
        platforms: [fa-linux]
    -
        title: Thunderbird
        logo: en/thunderbird.png
        link: https://www.thunderbird.net/
        text:
        platforms: [fa-linux, fa-windows]
    -
        title: DAVx⁵
        logo: en/davx5.png
        link: https://f-droid.org/en/packages/at.bitfire.davdroid/
        text:
        platforms: [fa-android]

---

## Calendriers et contacts

Partagez facilement et rapidement vos calendriers avec d'autres utilisateurs ou groupes **Nextcloud** sur votre serveur **Nextcloud**. Stockez vos contacts dans **Nextcloud** et partagez-les entre vos périphériques afin d'avoir toujours accès aux informations les plus importantes, quel que soit le périphérique que vous utilisez.

---

![](en/nextcloud-cal.png?lightbox=1024)
