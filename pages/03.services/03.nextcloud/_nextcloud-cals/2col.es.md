---
title: 'Calendarios y Contactos'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Usa tu cliente preferido'
clients:
    -
        title: GOA
        logo: en/goa.png
        link: https://wiki.gnome.org/Projects/GnomeOnlineAccounts
        text: 'GNOME cuentas en línea'
        platforms: [fa-linux]
    -
        title: Kaddressbook
        logo: en/KDE.png
        link: https://kde.org/applications/office/org.kde.kaddressbook
        text:
        platforms: [fa-linux]
    -
        title: Thunderbird
        logo: en/thunderbird.png
        link: https://www.thunderbird.net/
        text:
        platforms: [fa-linux, fa-windows]
    -
        title: DAVx⁵
        logo: en/davx5.png
        link: https://f-droid.org/en/packages/at.bitfire.davdroid/
        text:
        platforms: [fa-android]


---

## Calendarios y Contactos

Podemos compartir nuestros calendarios con otras personas usuarias o grupos de **Nextcloud** en nuestro servidor o proveedor, fácil y rápidamente. También almacenar contactos y compartirlos entre nuestros dispositivos para tener siempre acceso a ellos, independientemente del equipo en el que estemos.

---

![](en/nextcloud-cal.png?lightbox=1024)
