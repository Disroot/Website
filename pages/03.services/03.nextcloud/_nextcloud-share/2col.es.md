---
title: 'Intercambio Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-files-comments.png?lightbox=1024)

---

## Control de intercambio y acceso

Se puede compartir archivos con otras personas usuarias dentro de **Disroot**, crear y enviar enlaces públicos protegidos con contraseña, permitirles subir archivos a nuestra nube y recibir notificaciones cuando una usuaria o usuario comparte archivos con nosotres directamente.
