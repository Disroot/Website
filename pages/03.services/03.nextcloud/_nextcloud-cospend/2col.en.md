---
title: 'Cospend'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-cospend.jpg?lightbox=1024)

---

## Cospend

The Nextcloud **Cospend** app is a collaborative expense management tool, facilitating shared financial responsibilities among groups by allowing users to create expense pools, track contributions and expenditures, and settle debts efficiently.