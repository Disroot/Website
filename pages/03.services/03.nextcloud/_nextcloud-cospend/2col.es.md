---
title: 'Cospend'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-cospend.jpg?lightbox=1024)

---

## Cospend

La aplicación **Cospend** de Nextcloud es una herramienta de gestión de gastos colaborativa que facilita las responsabilidades financieras compartidas entre grupos al permitir a las personas usuarias crear grupos de gastos, realizar un seguimiento de las contribuciones y los gastos, y liquidar deudas de manera eficiente.