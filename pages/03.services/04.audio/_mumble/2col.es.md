---
title: Audio
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

---

![mumble_logo](mumble.png?resize=100,100)


## Mumble

**Audio de Disroot** está desarrollado por **Mumble**, una aplicación de audio chat de alta calidad de sonido, baja latencia, libre y de código abierto. Fue pensado originalmente para [gamers](https://es.wikipedia.org/wiki/Jugador_de_videojuegos), pero puede ser utilizado para organizar reuniones, audio conferencias, etc.

**¡AVISO!**

No es necesario tener cuenta para utilizar **Mumble**. Pero se obtienen mayores derechos de gestión si se registramos nuestro nombre de usuarie.

Sitio del Proyecto: [https://www.mumble.info](https://www.mumble.info)

Código fuente: [https://github.com/mumble-voip/mumble](https://github.com/mumble-voip/mumble)
