---
title: Audio
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---


---

![mumble_logo](mumble.png?resize=100,100)


## Mumble

Audio de Disroot est géré par Mumble. Mumble est une application de chat vocal gratuite, open source, à faible latence et de haute qualité. C'était à l'origine destiné aux joueurs, mais il peut être utilisé pour organiser des réunions audio, des conférences, etc.

**NOTE !**

Vous n'avez pas besoin de compte pour utiliser Mumble. Mais vous avez plus de droits si vous enregistrez votre nom d'utilisateur.

Page d'accueil du projet: [https://www.mumble.info](https://www.mumble.info)

Code source: [https://github.com/mumble-voip/mumble](https://github.com/mumble-voip/mumble)
