---
title: Audio
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---


---

![mumble_logo](mumble.png?resize=100,100)


## Mumble

Disroot's Audio is powered by Mumble. Mumble is a free, open source, low latency, high quality voice chat application. It was originally intended for gamers, but it can be use to organize audio meeting, conference, etc.

**NOTE!**

You don't need any account to use Mumble. But you have more rights if you register your username.

Project homepage: [https://www.mumble.info](https://www.mumble.info)

Source code: [https://github.com/mumble-voip/mumble](https://github.com/mumble-voip/mumble)
