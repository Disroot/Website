---
title: Audio
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---


---

![mumble_logo](mumble.png?resize=100,100)


## Mumble

Disroots Audio-Anwendung wird mit *Mumble* realisiert. Mumble ist eine kostenlose, quelloffene Voice-Chat-Anwendung mit niedriger Latenz und hoher Sprachqualität. Ursprünglich war sie für Gamer gedacht, aber sie kann auch zur Organisation von Audio-Meetings, Konferenzen usw. verwendet werden.

**HINWEIS!**

Du brauchst kein Konto, um Mumble zu benutzen. Aber Du hast mehr Nutzerrechte, wenn Du Deinen Benutzernamen registrierst.

Projekt-Homepage: [https://www.mumble.info](https://www.mumble.info)

Quellcode: [https://github.com/mumble-voip/mumble](https://github.com/mumble-voip/mumble)
