---
title: 'Mumble Clients'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
clients:
    -
        title: Mumble
        logo: mumble.png
        link: https://www.mumble.info/
        text: Official website
        platforms: [fa-linux, fa-windows, fa-apple]
 
    -
        title: Plumble
        logo: plumble.png
        link: https://github.com/acomminos/Plumble
        text: An Android client
        platforms: [fa-android]

    -
        title: Mumla
        logo: mumla.png
        link: https://mumla-app.gitlab.io/
        text: A fork of Plumble
        platforms: [fa-android]


---

<div class=clients markdown=1>

</div>
