---
title: 'Clients Mumble'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
clients:
    -
        title: Mumble
        logo: mumble.png
        link: https://www.mumble.info/
        text:
        platforms: [fa-linux, fa-windows, fa-apple]

    -
        title: Plumble
        logo: plumble.png
        link: https://github.com/acomminos/Plumble
        text:
        platforms: [fa-android]

    -
        title: Mumla
        logo: mumla.png
        link: https://monal.im/
        text: Un fork de Plumble
        platforms: [fa-android]


---

<div class=clients markdown=1>

</div>
