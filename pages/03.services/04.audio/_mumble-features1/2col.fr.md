---
title: 'Fonctionnalités de Mumble'
wider_column: left
---

## Facile à utiliser

Téléchargez l'application Mumble disponible sur votre appareil (bureau, Android ou iOS), connectez-vous à mumble.disroot.org et vous pouvez déjà commencer à créer une chaîne audio et à parler aux gens !

## Une qualité audio exceptionnelle

Faible latence et suppression du bruit, donc idéal pour parler. Vous pouvez avoir environ 100 participants en simultané !

## Privé et sécurisé

Les communications sont chiffrées, et l'authentification se fait par défaut par clé publique/privée. Si vous utilisez le logiciel de bureau Mumble, vous pouvez donner des autorisations très spécifiques à vos chaînes, groupes et utilisateurs.

## Application mobile

Gérez votre projet en déplacement avec l'application mobile Android (disponible sur F-Droid) ou l'application iOS.


---

![](en/connected.png?lightbox=1024)

![](en/notification.png?lightbox=1024)

![](en/acl.png?lightbox=1024)
