---
title: 'Características de Mumble'
wider_column: left
---

## Fácil de usar

Descargamos la aplicación Mumble disponible para nuestro dispositivo (escritorio, Android o iOS), nos conectamos a mumble.disroot.org y ya podemos comenzar a crear canales de audio o unirnos a uno y charlar con personas.

## Gran calidad de audio

Baja latencia y supresión de ruido lo hacen grandioso para conversar. ¡Podemos tener alrededor de 100 participantes en simultáneo!

## Privado y seguro

Las comunicaciones son cifradas y la validación es, por defecto, a través de claves públicas/privadas. Si utilizamos el cliente para escritorio de Mumble, podemos otorgar permisos muy específicos a nuestros canales, grupos y usuarios y usuarias.

## Aplicación para móvil

Para gestionar proyectos sobre la marcha con la aplicación para móviles con Android (disponible en F-Droid) o iOS.


---

![](en/connected.png?lightbox=1024)

![](en/notification.png?lightbox=1024)

![](en/acl.png?lightbox=1024)
