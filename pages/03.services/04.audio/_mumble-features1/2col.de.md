---
title: 'Mumble Features'
wider_column: left
---

## Einfach zu nutzen

Lade eine Mumble App für dein Betriebssystem herunter (Desktop, Android oder iOS), verbinde dich mit mumble.disroot.org, schon kannst du einen Audiochannel öffnen und mit anderen Leuten sprechen!

## Hervorragende Audio-Qualität

Niedrige Latenz und Geräuschunterdrückung machen Mumble so großartig. Es können ca. 100 Teilnehmer simultan teilnehmen!

## Privat und sicher

Kommunikation ist standardmäßig verschlüsselt und mit public/private-key authentifiziert. Mit der Desktop App gibt es die Möglichkeit, die Berechtigungen für einen Kanal/Gruppe/Benutzer vielseitig einzustellen.

## Mobile App

Nutze Mumble auch unterwegs mit den Apps für Android (F-Droid) und iOS.


---

![](en/connected.png?lightbox=1024)

![](en/notification.png?lightbox=1024)

![](en/acl.png?lightbox=1024)
