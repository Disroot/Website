---
title: Scribe
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://scribe.disroot.org/">Scribe</a>

---

![](logo.png)

Scribe ist eine Plattform zum Sammeln von Links und für Diskussionen, die mit **Lemmy** betrieben wird. Die Inhalte sind in Communities organisiert, so dass es einfach ist, Themen zu abonnieren, die Sie interessieren, und andere zu ignorieren.


Disroot Scribe: [https://scribe.disroot.org](https://scribe.disroot.org)

Projekt Website: [https://join-lemmy.org/](https://join-lemmy.org/)

Quellcode: [https://github.com/LemmyNet/lemmy](https://github.com/LemmyNet/lemmy)

<hr>

Du musst auf [scribe.disroot.org](https://scribe.disroot.org/) einen eigenen Account erstellen, um diese Anwendung zu nutzen. Deine Disroot-Zugangsdaten werden nicht unterstützt.