---
title: Scribe
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://scribe.disroot.org/">Scribe</a>

---

![](logo.png)

Scribe es una plataforma de agrupación social de links y discusión, desarrollado por **Lemmy**. El contenido se organiza dentro de comunidades, así es fácil suscribirse a temas que nos interesan e ignorar otros.


Scribe de Disroot: [https://scribe.disroot.org](https://scribe.disroot.org)

Página del proyecto: [https://join-lemmy.org/](https://join-lemmy.org/)

Código fuente: [https://github.com/LemmyNet/lemmy](https://github.com/LemmyNet/lemmy)

<hr>

Es necesario crear una cuenta separada en [scribe.disroot.org](https://scribe.disroot.org/) para utilizar este servicio. Las credenciales de **Disroot** no están soportadas.