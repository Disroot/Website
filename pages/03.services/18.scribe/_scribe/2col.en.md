---
title: Scribe
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://scribe.disroot.org/">Scribe</a>

---

![](logo.png)

Scribe is a social link aggregation and discussion platform, powered by **Lemmy**. Content is organized into communities, so it is easy to subscribe to topics that you are interested in, and ignore others.


Disroot Scribe: [https://scribe.disroot.org](https://scribe.disroot.org)

Project Homepage: [https://join-lemmy.org/](https://join-lemmy.org/)

Source code: [https://github.com/LemmyNet/lemmy](https://github.com/LemmyNet/lemmy)

<hr>

You need to create a separate account on [scribe.disroot.org](https://scribe.disroot.org/) to use this service. **Disroot** credentials are not supported.