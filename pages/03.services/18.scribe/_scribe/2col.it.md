---
title: Scribe
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://scribe.disroot.org/">Scribe</a>

---

![](logo.png)

Scribe è una piattaforma di aggregazione di link sociali e di discussione, gestita da **Lemmy**. I contenuti sono organizzati in comunità, per cui è facile iscriversi agli argomenti che interessano e ignorare gli altri.


Disroot Scribe: [https://scribe.disroot.org](https://scribe.disroot.org)

Homepage del progetto: [https://join-lemmy.org/](https://join-lemmy.org/)

Codice sorgente: [https://github.com/LemmyNet/lemmy](https://github.com/LemmyNet/lemmy)

<hr>

Per utilizzare questo servizio è necessario creare un account separato su [scribe.disroot.org](https://scribe.disroot.org/). Le credenziali **Disroot** non sono supportate.
