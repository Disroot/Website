---
title: 'Características de Scribe'
wider_column: right
---

![](en/scribe_subscribe.png?lightbox=1024)

---

## Soporte para RSS
Ofrece soporte para fuentes RSS / Atom para los temas: Todos, Suscritos, Bandeja de entrada, Usuaries y Comunidad.

## Eliminar
Es posible eliminar todos nuestros datos, quitando todas las publicaciones y comentarios.

## Federado
Podemos suscribirnos a comunidades de cualquier servidor de **Lemmy** en el mundo. Y como utiliza el protocolo ActivityPub, esto significa que podemos comunicarnos con las plataformas y programas del Fediverso ("universo federado") como Mastodon, PeerTube, Akkoma, etc.

## Soporte para Publicación cruzada (Cross-posting)
Una característica que permite buscar publicaciones similares cuando estamos creando una. Es una funcionalidad fantástica para evitar duplicar publicaciones, preguntas y respuestas dentro de las comunidades.