---
title: 'Scribe Features'
wider_column: right
---

![](en/scribe_subscribe.png?lightbox=1024)

---

## Supporto RSS
Fornisce il supporto per i feed RSS/Atom per gli argomenti: Tutti, Sottoscritti, Posta in arrivo, Utenti e Comunità.

## Cancellazione
Possibilità di cancellare completamente i propri dati, rimuovendo tutti i post e i commenti.

## Federato
È possibile iscriversi alle comunità da qualsiasi server **Lemmy** nel mondo! E poiché utilizza il protocollo ActivityPub, questo significa che è possibile comunicare con le piattaforme e i software del Fediverso ("universo federato") come Mastodon, PeerTube, Akkoma, ecc.

## Supporto al cross-posting
Una funzione che consente di cercare post simili durante la creazione di un post. È ottimo per evitare la duplicazione di post, domande e risposte all'interno delle comunità.
