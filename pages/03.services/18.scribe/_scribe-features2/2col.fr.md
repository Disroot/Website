---
title: 'Caractéristiques de Scribe'
wider_column: right
---

![](en/scribe_subscribe.png?lightbox=1024)

---

## Support RSS
Prise en charge les flux RSS / Atom pour les rubriques : Tous, Abonnés, Boîte de réception, Utilisateur et Communauté.

## Suppression
Il est possible d'effacer complètement vos données, en supprimant tous les messages et commentaires.

## Fédéré
Vous pouvez vous abonner à des communautés à partir de n'importe quel serveur **Lemmy** dans le monde ! Et comme Lemmy utilise le protocole ActivityPub, cela signifie que vous pouvez communiquer avec les plateformes et logiciels Fediverse ("univers fédéré") comme Mastodon, PeerTube, Akkoma, etc.

## Prise en charge de l'affichage croisé
Une fonction qui vous permet de rechercher des messages similaires lorsque vous en créez un. Elle permet d'éviter la duplication des messages, des questions et des réponses au sein des communautés.