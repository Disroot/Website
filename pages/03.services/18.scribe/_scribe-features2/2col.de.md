---
title: 'Scribe Features'
wider_column: right
---

![](en/scribe_subscribe.png?lightbox=1024)

---

## RSS-Unterstützung
Es bietet RSS/Atom-Feed-Unterstützung für folgende Bereiche: Alle, Abonniert, Posteingang, Benutzer und Gemeinschaft.

## Datenlöschung
Es ist möglich, Ihre Daten vollständig zu löschen und alle Beiträge und Kommentare zu entfernen.

## Föderiert
Sie können Communities von jedem **Lemmy**-Server der Welt aus abonnieren! Und da das ActivityPub-Protokoll verwendet wird, können Sie mit den Fediverse-Plattformen ("federated universe") und Software wie Mastodon, PeerTube, Akkoma usw. kommunizieren.

## Unterstützung für Cross-Posting
Eine Funktion, mit der Sie nach ähnlichen Beiträgen suchen können, wenn Sie einen Beitrag erstellen. Dies ist sehr hilfreich, um doppelte Beiträge, Fragen und Antworten innerhalb von Gemeinschaften zu vermeiden.