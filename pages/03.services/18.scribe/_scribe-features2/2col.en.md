---
title: 'Scribe Features'
wider_column: right
---

![](en/scribe_subscribe.png?lightbox=1024)

---

## RSS support
It provides RSS / Atom feed support for the topics: All, Subscribed, Inbox, User and Community.

## Deletion
Possible to fully erase your data, removing all posts and comments.

## Federated
You can subscribe to communities from any **Lemmy** servers in the world! And since it uses the ActivityPub protocol, this means you can communicate with the Fediverse ("federated universe") platforms and software like Mastodon, PeerTube, Akkoma, etc.

## Cross-posting support
A feature that allows you to search for similar posts when you are creating one. It is great to avoid duplicating posts, questions and answers within communities.