---
title: 'Scribe Features'
wider_column: left
---

## Creare comunità
Una comunità può essere incentrata su qualsiasi argomento. Gli utenti possono pubblicare testi, link o immagini e discuterne con gli altri.

## Vota!
I post e i commenti possono essere votati o respinti, il che aiuta a far emergere gli elementi più interessanti.

## Moderazione
Capacità di moderazione e registri pubblici di moderazione.

---

![](en/scribe_home.png?lightbox=1024)
