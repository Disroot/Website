---
title: 'Scribe Features'
wider_column: left
---

## Communities erstellen
Eine Community kann sich auf ein beliebiges Thema konzentrieren. Die Nutzer können dann Texte, Links oder Bilder einstellen und mit anderen darüber diskutieren.

## Bewerten!
Beiträge und Kommentare können höher oder niedriger gestuft werden, was dazu beiträgt, die interessantesten Artikel an die Spitze zu bringen.


## Moderation
Moderationsfunktionen und öffentliche Moderationsprotokolle.

---

![](en/scribe_home.png?lightbox=1024)