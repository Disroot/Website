---
title: 'Scribe Features'
wider_column: left
---

## Create communities
A community can focus on any topic. Users can then post text, links or images and discuss it with others.

## Vote!
Posts and comments can be upvoted or downvoted, which helps to bring the most interesting items to the top.

## Moderation
Moderation abilities and Public Moderation Logs.

---

![](en/scribe_home.png?lightbox=1024)