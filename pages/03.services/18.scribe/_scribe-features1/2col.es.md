---
title: 'Características de Scribe'
wider_column: left
---

## Crear comunidades
Una comunidad puede enforcarse en cualquier temática. Las personas usuarias pueden publicar texto, enlaces o imágenes y discutir con otras.

## ¡Votar!
Las publicaciones y comentarios pueden votarse positiva o negativamente, esto ayuda a llevar más arriba los artículos más interesantes.

## Moderación
Opciones y Registro Público de moderación.

---

![](en/scribe_home.png?lightbox=1024)