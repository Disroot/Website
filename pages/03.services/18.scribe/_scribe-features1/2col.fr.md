---
title: 'Caractéristiques de Scribe'
wider_column: left
---

## Créer des communautés
Une communauté peut porter sur n'importe quel sujet. Les utilisateurs peuvent alors publier des textes, des liens ou des images et en discuter avec d'autres.

## Votez !
Les messages et les commentaires peuvent être notés à la hausse ou à la baisse, ce qui permet de faire remonter les éléments les plus intéressants.

## Modération
Capacités de modération et journaux de modération publics.

---

![](en/scribe_home.png?lightbox=1024)