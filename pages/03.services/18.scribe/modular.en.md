---
title: Scribe
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _scribe
            - _scribe-highlights
            - _scribe-features1
            - _scribe-features2

body_classes: modular
header_image: scribe_banner.jpg
---
