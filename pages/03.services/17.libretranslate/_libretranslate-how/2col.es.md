---
title: 'LibreTranslate Cómo'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## ¿Cómo funciona?

Es muy sencillo: solo hay que copiar y pegar el texto y elegir a qué idioma queremos que se traduzca. Listo. Incluso se puede enviar un archivo para traducir entero.
Es compatible con muchos idiomas y se basa en un modelo de traducción neuronal, que le permite producir traducciones más precisas que los antiguos modelos de traducción estadística.
 
No es necesario registrarse para traducir textos.

---

![](en/LibreTranslate.png?lightbox=1024)