---
title: 'LibreTranslate How'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Comment cela fonctionne-t-il ?

C'est très simple : il suffit de copier-coller votre texte et de choisir la langue dans laquelle vous souhaitez qu'il soit traduit. Et le tour est joué ! Vous pouvez même envoyer un fichier à traduire.
Il prend en charge de nombreuses langues et est basé sur un modèle de traduction neuronal, ce qui lui permet de produire des traductions plus précises que les anciens modèles de traduction statistique.
 
Vous n'avez pas besoin de vous enregistrer pour traduire des textes.

---

![](en/LibreTranslate.png?lightbox=1024)