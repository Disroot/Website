---
title: 'LibreTranslate How'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Wie funktionierts?


super einfach: füge einen Text ein und wähle die Sprache, in die übersetzt werden soll. Das war's schon! Du kannst sogar eine Datei hochladen, die übersetzt werden soll.

Das Tool unterstützt viele Sprachen und basiert auf einem neuronalen Übersetzungsmodell, das bessere Übersetzungen liefert als frühere statistische Übersetzungsmodelle.
 
Man braucht kein Disroot Konto, um diesen Service zu nutzen.

---

![](en/LibreTranslate.png?lightbox=1024)