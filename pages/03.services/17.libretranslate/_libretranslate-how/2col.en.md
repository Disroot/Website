---
title: 'LibreTranslate How'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## How does it work?

It is very simple: just copy paste your text and choose what language you want it to be translated into. That's it! You can even send a file you want to translate.
It supports many languages and is based on a neural translation model, which enables it to produce more accurate translations than older statistical translation models.
 
You don't need to register yourself to translate texts.

---

![](en/LibreTranslate.png?lightbox=1024)