---
title: 'LibreTranslate How'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Come funziona?

È molto semplice: basta copiare e incollare il testo e scegliere la lingua in cui si desidera che venga tradotto. Tutto qui! È anche possibile inviare un file da tradurre.
Supporta molte lingue ed è basato su un modello di traduzione neurale, che gli consente di produrre traduzioni più accurate rispetto ai vecchi modelli di traduzione statistica.
 
Non è necessario registrarsi per tradurre testi.

---

![](en/LibreTranslate.png?lightbox=1024)
