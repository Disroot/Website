---
title: Translate
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _libretranslate
            - _empty-bar
            - _libretranslate-how
body_classes: modular
header_image: libretranslate_banner.jpg

translation_banner:
    set: true
    last_modified: Novembre 2023
    language: French
---
