---
title: LibreTranslate
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://translate.disroot.org/">Translate</a>
<a class="button button1" href="http://3veapfc2xpg4ouflejs2r2y3z6nkrylqkx25ruxtqpznxeyfycvhhrqd.onion">Tor</a>

---

![](logo.png)

Disroot's Translate ist ein maschineller Übersetzungsdienst, der von **LibreTranslate** stammt. LibreTranslate unterstützt viele Sprachen. Es ist ein nützliches Tool für alle, die Texte in verschiedene Sprachen übersetzen möchten. Außerdem ist es ein nützliches Werkzeug für Entwickler, die eine Übersetzunganwendung schreiben wollen.


Man braucht kein Disroot Konto, um diesen Service zu nutzen.

Disroot Translate: [https://translate.disroot.org](https://translate.disroot.org)

Projekt Homepage: [https://libretranslate.com](https://libretranslate.com)

Source code: [https://github.com/LibreTranslate/LibreTranslate](https://github.com/LibreTranslate/LibreTranslate)

