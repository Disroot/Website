---
title: LibreTranslate
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://translate.disroot.org/">Translate</a>
<a class="button button1" href="http://3veapfc2xpg4ouflejs2r2y3z6nkrylqkx25ruxtqpznxeyfycvhhrqd.onion">Tor</a>

---

![](logo.png)

Disroot's Translate is a machine translation service, powered by **LibreTranslate**. LibreTranslate supports a lot of languages. It is an useful tool for those wishing to translate text between different languages. It is also a valuable tool for developers wishing to create translation applications.

You don't need any account on Disroot to use this service.

Disroot Translate: [https://translate.disroot.org](https://translate.disroot.org)

Project Homepage: [https://libretranslate.com](https://libretranslate.com)

Source code: [https://github.com/LibreTranslate/LibreTranslate](https://github.com/LibreTranslate/LibreTranslate)
