---
title: LibreTranslate
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://translate.disroot.org/">Traducir</a>
<a class="button button1" href="http://3veapfc2xpg4ouflejs2r2y3z6nkrylqkx25ruxtqpznxeyfycvhhrqd.onion">Tor</a>

---

![](logo.png)

Translate de Disroot es un servicio de traducción automática, impulsado por **LibreTranslate** que soporta muchos idiomas. Es una herramienta útil para la traducción de porciones de texto y también para quienes quieran crear aplicaciones de traducción.

No se necesita ninguna cuenta en Disroot para utilizar este servicio.

Translate de Disroot: [https://translate.disroot.org](https://translate.disroot.org)

Página del proyecto: [https://libretranslate.com](https://libretranslate.com)

Código fuente: [https://github.com/LibreTranslate/LibreTranslate](https://github.com/LibreTranslate/LibreTranslate)
