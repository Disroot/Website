---
title: LibreTranslate
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://translate.disroot.org/">Traduire</a>
<a class="button button1" href="http://3veapfc2xpg4ouflejs2r2y3z6nkrylqkx25ruxtqpznxeyfycvhhrqd.onion">Tor</a>

---

![](logo.png)

Disroot's Translate est un service de traduction automatique, basé sur **LibreTranslate**. LibreTranslate prend en charge un grand nombre de langues. C'est un outil utile pour ceux qui souhaitent traduire des textes entre différentes langues. C'est également un outil précieux pour les développeurs qui souhaitent créer des applications de traduction.
Vous n'avez besoin d'aucun compte sur Disroot pour utiliser ce service.

Disroot Translate: [https://translate.disroot.org](https://translate.disroot.org)

Page du projet: [https://libretranslate.com](https://libretranslate.com)

Code source: [https://github.com/LibreTranslate/LibreTranslate](https://github.com/LibreTranslate/LibreTranslate)
