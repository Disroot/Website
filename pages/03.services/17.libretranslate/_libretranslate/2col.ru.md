---
title: LibreTranslate
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://translate.disroot.org/">Translate</a>
<a class="button button1" href="http://3veapfc2xpg4ouflejs2r2y3z6nkrylqkx25ruxtqpznxeyfycvhhrqd.onion">Tor</a>

---

![](logo.png)

Disroot Translate - это сервис машинного перевода, основанный на **LibreTranslate**. LibreTranslate поддерживает большое количество языков. Это полезный инструмент, для тех, кто хочет переводить текст с одного языка на другой. Это также ценный инструмент для разработчиков, желающих создавать приложения для перевода.

Для использования этого сервиса не требуется учётная запись Disroot.

Disroot Translate: [https://translate.disroot.org](https://translate.disroot.org)

Домашняя страница: [https://libretranslate.com](https://libretranslate.com)

Исходный код: [https://github.com/LibreTranslate/LibreTranslate](https://github.com/LibreTranslate/LibreTranslate)
