---
title: LibreTranslate
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://translate.disroot.org/">Translate</a>
<a class="button button1" href="http://3veapfc2xpg4ouflejs2r2y3z6nkrylqkx25ruxtqpznxeyfycvhhrqd.onion">Tor</a>

---

![](logo.png)

Disroot's Translate è un servizio di traduzione automatica basato su **LibreTranslate**. LibreTranslate supporta molte lingue. È uno strumento utile per coloro che desiderano tradurre testi tra lingue diverse. È anche uno strumento prezioso per gli sviluppatori che desiderano creare applicazioni di traduzione.

Non è necessario alcun account su Disroot per utilizzare questo servizio.

Disroot Translate: [https://translate.disroot.org](https://translate.disroot.org)

Homepage del progetto: [https://libretranslate.com](https://libretranslate.com)

Codice sorgente: [https://github.com/LibreTranslate/LibreTranslate](https://github.com/LibreTranslate/LibreTranslate)
