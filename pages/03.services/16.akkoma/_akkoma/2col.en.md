---
title: Akkoma
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Sign up for **Disroot**</a>
<a class="button button1" href="https://fe.disroot.org/">Log in</a>

---

![akkoma_logo](akkoma.png?resize=100,100)


## Akkoma
**Akkoma** is a microblogging server software being part of interoperable Fediverse.

Fediverse is a federated network of platforms utilizing ActivityPub protocol allowing for content exchange between servers. The network consists of millions of users spread across thousands of servers run by independent operators. Our server is one of them.

Disroot Akkoma: [https://fe.disroot.org](https://fe.disroot.org)

Project homepage: [https://akkoma.social/](https://akkoma.social/)

Source code: [https://akkoma.dev/AkkomaGang/akkoma](https://akkoma.dev/AkkomaGang/akkoma)
