---
title: 'Write what you have in mind!'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---
# Write what you have in mind!

*You can post, publish photos, videos or music. With Akkoma, you're not restricted to 150 characters! Express yourself and let others know about you.*
