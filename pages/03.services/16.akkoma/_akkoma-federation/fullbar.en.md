---
title: 'Federation'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

# Federation


*With over 50 million users spread across thousands of servers operated by independent administrators, Fediverse is the biggest decentralized, open alternative to commercial corporate offerings. It allows for flexibility and freedom on multiple levels.*
