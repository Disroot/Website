---
title: 'No Advertisement, influencers, sponsored posts'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---
# No Advertisement, influencers, sponsored posts

While large centralized platforms depend on giant datacenters that need enormous amount of energy to operate, with federated networks such as Fediverse, the energy consumption is more democratized and decentralized. Rather than maintaining giant datacenters, servers are smaller and self-contained. This greatly reduces the power needs for such network to operate. 

---

# From people to people

Since the profit is not the motor behind the development, people working on various services and apps within Fediverse Realm create software they want to use in the first place. No algorithms pushing you content platform owners want to you to see, no addictive features to keep you glued to the screen if you don't want to, no micro transactions. Just software created by people you can easily reach to give feedback, request feature or simply join in collaborative effort to make your favorite app even better!  

