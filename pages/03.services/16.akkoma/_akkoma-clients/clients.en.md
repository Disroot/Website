---
title: 'Akkoma clients'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: center
clients:
    -
        title: Mangane (Web + PWA)
        logo: mangane.png
        link: https://fe.disroot.org
        text:
        platforms: [fa-firefox]
    -
        title: Fedilab
        logo: fedilab.png
        link: https://fedilab.app/
        text:
        platforms: [fa-android]

    -
        title: Husky
        logo: husky.png
        link: https://husky.adol.pw/
        text:
        platforms: [fa-android]

    -
        title: Whalebird
        logo: whalebird.webp
        link: https://whalebird.social/
        text:
        platforms: [fa-linux]

    -
        title: Fedistar
        logo: fedistar.png
        link: https://fedistar.net/
        text:
        platforms: [fa-windows, fa-apple, fa-linux]

    -
        title: Amaroq
        logo: amaroq.jpg
        link: https://itunes.apple.com/us/app/amaroq-for-mastodon/id1214116200
        text:
        platforms: [fa-apple]


---

<div class=clients markdown=1>

</div>
