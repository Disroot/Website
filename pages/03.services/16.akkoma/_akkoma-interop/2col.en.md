---
title: 'Interoperability'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---
# Interoperability

Although our server (Akkoma) is essentially a microblogging platform, ActivityPub (Or Fediverse) offers range of services. From microblogging like Akkoma or Mastodon, through social network similar to Facebook such as Friendica or Hubzilla, image posting service like PixelFed or Youtube like platform called PeerTube. You can interact with all of those natively no matter which platform you are using (imagine being able to like or comment on your friend's post on Instagram using your Twitter account).


---

![](akkoma_interop.png?display=thumbnail)
