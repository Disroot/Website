---
title: 'Akkoma'
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _akkoma
            - _akkoma-highlights
            - _akkoma-federation
            - _akkoma-freedom
            - _akkoma-noads
            - _akkoma-interop
            - _akkoma-write
            - _akkoma-clients_header
            - _akkoma-clients
            - _akkoma-timelines_tags
            - _akkoma-dms_notifs
            - _akkoma-webview

body_classes: modular
header_image: Free_Speech_Fear_Free_at_Prelinger_Library.jpg
---
