---
title: 'Freedom of speech and environment'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---
# Freedom of speech

Your fate is not determined by a single entity/owner of the network as each server is free to implement their own set of moderation rules or lack of. This on one hand could lead to excessive hate speech or abuse, or on the other end moderation falling into censorship. But unlike commercial, centralized offerings you have a choice to use a server that best suits you. 

---

# Environmental impact

While large centralized platforms depend on giant datacenters that need enormous amount of energy to operate, with federated networks such as Fediverse, the energy consumption is more democratized and decentralized. Rather than maintaining giant datacenters, servers are smaller and self-contained. This greatly reduces the power needs for such network to operate. 
