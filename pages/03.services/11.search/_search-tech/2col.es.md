---
title: Búsqueda
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: left
---

# ¿Cómo funciona?

**SearXNG** no registra ni crea un índice de sitios por sí mismo. Cuando escribimos una consulta en el buscador, **SearXNG** la deriva a un número de otros buscadores como Google, DuckDuckGo, Bing, etc., y devuelve los resultados de esos motores en forma agrupada.

**SearXNG** tal vez no ofrezca resultados tan personalizados como Google, pero eso es porque no genera un perfil acerca nuestro ni comparte nuestros detalles personales, ubicación o la de nuestra computadora con ningún motor de búsquedas a los que deriva las consultas. Esto brinda mucha mejor privacidad y actúa como un 'escudo' contra los grandes motores corporativos que te espían.

Podemos ver [aquí](https://search.disroot.org/preferences) qué motores de búsqueda es posible utilizar para conseguir resultados de nuestras consultas.
