---
title: Recherche
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: left
---

# Comment ça marche?

**SearXNG** ne tient pas ou ne crée pas d'index des sites Web par lui-même. Lorsque vous tapez votre requête dans le champ de recherche, **SearXNG** relaie cette requête à un certain nombre d'autres moteurs de recherche comme google, duckduckgo, bing etc. et vous renvoie les résultats de ces moteurs sous forme agrégée.

**SearXNG** peut ne pas vous offrir des résultats aussi personnalisés que Google, mais c'est parce qu'il ne génère pas de profil sur vous et qu'il ne partage aucune de vos informations personnelles, votre emplacement ou votre ordinateur, avec les moteurs de recherche auxquels il transmet vos requêtes. Cela offre une bien meilleure protection de la vie privée et agit comme un "bouclier" contre les grands moteurs d'entreprise qui vous espionnent.

Vous pouvez voir [ici](https://search.disroot.org/preferences) quels moteurs de recherche peuvent être utilisés pour obtenir des résultats sur une requête.
