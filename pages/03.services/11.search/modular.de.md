---
title: 'Suche'
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _search
            - _search-one-search-to-rule-all
            - _search-tech
            - _empty-bar
body_classes: modular
header_image: search_banner.jpg

translation_banner:
    set: true
    last_modified: April 2024
    language: German
---
