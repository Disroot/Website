---
title: Ricerca
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://search.disroot.org">Vai a Searx'ing</a>
<a class="button button1" href="http://mycroftproject.com/search-engines.html?name=disroot.org">Installa il plugin per il browser</a>
<a class="button button1" href="http://bzg6fq2cbzrp52z5xkmggsiqhfc4zb4ouq3g7y6b2yfdnuud6yajpyqd.onion">Tor</a>

---

![](searx_logo.png)

# Piattaforma anonima multi motore di ricerca

Searx è un metamotore, significa che aggrega i risultati di altri motori di ricerca e lo fa senza salvare informazioni sugli utenti.

Disroot's Search è un motore di ricerca come Google, DuckDuckGo e Qwant, alimentato da **SearXNG**. Ciò che lo rende unico rispetto agli altri è che si tratta di un motore di metaricerca, che aggrega i risultati di altri motori di ricerca senza memorizzare alcuna informazione sui suoi utenti.

Non è necessario avere un account Disroot per utilizzare questo servizio.

Ricerca Disroot : [https://search.disroot.org](https://search.disroot.org)

Codice sorgente: [https://github.com/searxng/searxng](https://github.com/searxng/searxng)