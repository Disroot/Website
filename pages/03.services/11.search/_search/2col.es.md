---
title: Búsqueda
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://search.disroot.org">Ir a SearXNG</a>
<a class="button button1" href="http://mycroftproject.com/search-engines.html?name=disroot.org">Instalar complemento para el navegador</a>
<a class="button button1" href="http://bzg6fq2cbzrp52z5xkmggsiqhfc4zb4ouq3g7y6b2yfdnuud6yajpyqd.onion">Tor</a>

---

![](searx_logo.png)

# Plataforma multimotor de búsqueda anónima

**Búsqueda** de **Disroot** es un motor de búsquedas como Google, DuckDuckGo, Qwant, desarrollado por **SearXNG**. Lo que lo hace único en comparación con otros, es que se trata de un buscador multimotor, agregando los resultados de otros motores de búsqueda mientras que no guarda ninguna información de sus usuaries.

No se necesita una cuenta de **Disroot** para utilizar este servicio.

Búsqueda de Disroot: [https://search.disroot.org](https://search.disroot.org)

Código fuente: [https://github.com/searxng/searxng](https://github.com/searxng/searxng)
