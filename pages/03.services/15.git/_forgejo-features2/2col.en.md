---
title: 'Git Features'
wider_column: left
---

![](en/forgejo_dashboard.png?lightbox=1024)

![](en/forgejo_activity.png?lightbox=1024)


---

## Issue tracking
Manage bug reports, feature requests and feedback to your projects.

## Tags, assignees, milestones, comments
Manage your repository efficiently by easily determining your milestones, sorting out your issues, and discussing and assigning tasks to fellow team members.

## Wiki
Create a wiki to document your project, and use Markdown to write it.

## Mobile app
Manage your project on the go with the Android mobile app (available on F-Droid).
