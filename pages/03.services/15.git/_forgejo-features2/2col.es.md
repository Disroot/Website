---
title: 'Características de Git'
wider_column: left
---

![](en/forgejo_dashboard.png?lightbox=1024)

![](en/forgejo_activity.png?lightbox=1024)


---

## Seguimiento de problemas
Gestionar problemas, solicitud de funcionalidades y comentarios para nuestros proyectos.

## Etiquetas, asignaciones, metas, comentarios
Permite administrar los repositorios de manera eficiente determinando fácilmente las metas, ordenando los problemas, discutiendo y asignando tareas a las demás personas de un equipo.

## Wiki
Creación de wikis para explicar nuestros proyectos que utiliza Markdown.

## Aplicación para el móvil
Podemos gestionar nuestros proyectos sobre la marcha con la aplicación para móviles con Android (disponible en F-Droid).
