---
title: 'Fonctionnalités Git'
wider_column: left
---

![](en/forgejo_dashboard.png?lightbox=1024)

![](en/forgejo_activity.png?lightbox=1024)


---

## Suivi des problèmes
Gérer les problèmes, les demandes de fonctionnalités et les commentaires sur vos projets.

## Balises, attributaires, étapes, commentaires
Gérez efficacement votre dépôt en déterminant facilement vos étapes, en triant vos problèmes, en discutant et en attribuant vos tâches aux autres membres de l'équipe

## Wiki
Créez un wiki pour expliquer votre projet, et utilisez Markdown.

## Application mobile
Gérez votre projet en déplacement avec l'application mobile android (disponible sur fdroid).
