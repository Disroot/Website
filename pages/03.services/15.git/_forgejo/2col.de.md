---
title: Git
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://git.disroot.org/user/sign_up">Registrieren</a>
<a class="button button1" href="https://git.disroot.org/">Anmelden</a>
<a class="button button1" href="http://kgtz2pmmov5jfvn3z4mqryffjnnw6krzrgxxoyaqhqckjrr4pckyhsqd.onion">Tor</a>

---

![forgejo_logo](forgejo.png?resize=100,100)


## Forgejo

**Disroot's Git** wird mit **Forgejo** betrieben. **Forgejo** ist eine gemeinschaftsbetriebene, mächtige, einfach zu benutzende und leichtgewichtige Lösung für Code-Hosting und Projekt-Kollaboration. Es baut auf der GIT-Technologie auf, welche aktuell das meistgenutzte moderne Versionskontrollsystem der Welt ist.

Disroot Git: [https://git.disroot.org](https://git.disroot.org)

Projekt-Seite: [https://forgejo.org/](https://forgejo.org/)

Quellcode: [https://codeberg.org/forgejo/forgejo](https://codeberg.org/forgejo/forgejo)

<hr>

Du musst auf [git.disroot.org](https://git.disroot.org/) einen eigenen Account erstellen, um diese Anwendung zu nutzen. Deine **Disroot**-Zugangsdaten werden nicht unterstützt.

<hr>

🔑 **ssh-Fingerabdruck**: SHA256:B8RHZmR8N7oyt0DG04jn+SWDDRpFrQh4F2Vo3PfUNqY