---
title: Git
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://git.disroot.org/user/sign_up">Sign up</a>
<a class="button button1" href="https://git.disroot.org/">Log in</a>
<a class="button button1" href="http://kgtz2pmmov5jfvn3z4mqryffjnnw6krzrgxxoyaqhqckjrr4pckyhsqd.onion">Tor</a>

---

![forgejo_logo](forgejo.png?resize=100,100)


## Forgejo

**Disroot's Git** service is powered by **Forgejo**. **Forgejo** is a community driven, lightweight, powerful, and easy to use solution to code hosting and project collaboration. It's built around the Git version control system which is the most widely used version control system in the world today.

Disroot's Git service: [https://git.disroot.org](https://git.disroot.org)

Project homepage: [https://forgejo.org/](https://forgejo.org/)

Source code: [https://codeberg.org/forgejo/forgejo](https://codeberg.org/forgejo/forgejo)

<hr>

You will need to create a separate account on [git.disroot.org](https://git.disroot.org/) to use this service. **Disroot** account credentials are not supported.

<hr>

🔑 **ssh fingerprint**: SHA256:B8RHZmR8N7oyt0DG04jn+SWDDRpFrQh4F2Vo3PfUNqY
