---
title: 'Git Funktionalitäten'
wider_column: left
---

## Einfach zu benutzen
Sieh Dir die öffentlichen Repositorys an, finde das, an welchem Du arbeiten willst und klone es auf Deinem Computer. Oder erstelle Dein eigenes! Du kannst es auf öffentlich oder privat setzen.

## Schnell und leichtgewichtig
**Forgejo** bietet eine sehr schnelle und leichtgewichtige Erfahrung. Keine aufgeblähte, überentwickelte Funktionalitäten-Sammlung. Nur Sachen, die grundlegend wichtig sind, um zusammen mit anderen an Deinem Opensource-Projekt zu arbeiten!

## Benachrichtigungen
Erhalte Emails, wenn ein Problem gelöst, ein Pull-Request erstellt wurde, etc.

## Sicher
Nutze SSH-Schlüssel, 2-Faktor-Authentifizierung und GnuPG, um Dein Repository zu schützen.


---

![](en/forgejo_explore.png?lightbox=1024)

![](en/forgejo_secure.png?lightbox=1024)
