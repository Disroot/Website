---
title: 'Fonctionnalités Git'
wider_column: left
---

## Facile à utiliser
Consultez les dépôts publics, trouvez celui sur lequel vous voulez travailler et clonez-le sur votre ordinateur. Ou créez le vôtre ! Vous pouvez le définir comme un dépôt public ou privé.

## Rapide et léger
**Forgejo** offre une expérience très rapide et légère. Pas de fonctions surchargées et surdimensionnées. Seulement les choses essentielles pour collaborer sur votre projet open source !

## Notifications
Recevoir des courriels lorsqu'un problème a été résolu, qu'une demande d'extraction a été créée, etc.

## Sécurisé
Utilisez une clé ssh, l'authentification à deux facteurs et GnuPG pour sécuriser votre dépôt.

---

![](en/forgejo_explore.png?lightbox=1024)

![](en/forgejo_secure.png?lightbox=1024)
