---
title: 'Git Features'
wider_column: left
---

## Easy to use
Explore public repositories, find the one you want to work on and clone it on your computer. Or create your own! You can make it either public or private.

## Fast and lightweight
**Forgejo** offers a very fast and light experience. There isn't any bloated, over-engineered feature set. Only things that are essential to collaborate on your open source project!

## Notifications
Receive emails when an issue was solved, a new pull request was opened, someone replied to a thread, etc.

## Secure
Use an SSH key, two factor authentication and GnuPG to secure your repository.


---

![](en/forgejo_explore.png?lightbox=1024)

![](en/forgejo_secure.png?lightbox=1024)
