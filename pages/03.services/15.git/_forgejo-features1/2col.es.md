---
title: 'Características de Git'
wider_column: left
---

## Fácil de usar
Revisar los repositorios públicos, encontrar un proyecto en el que se quiera trabajar y clonarlo en nuestra computadora. ¡O podemos crear el nuestro propio! Puede configurarse como un repositorio público o privado.

## Rápido y liviano
**Forgejo** ofrece una experiencia muy veloz y liviana. Sin características infladas o configuraciones demasiado técnicas. ¡Solo las cosas que son esenciales para colaborar en un proyecto de código abierto!

## Notificaciones
Recibir correos cuando un problema fue resuelto, una solicitud es creada, etc.

## Seguro
Uso de claves SSH, autenticación en dos pasos y GnuPG para asegurar nuestro repositorio.


---

![](en/forgejo_explore.png?lightbox=1024)

![](en/forgejo_secure.png?lightbox=1024)
