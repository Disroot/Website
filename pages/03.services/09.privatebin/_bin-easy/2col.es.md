---
title: 'Fácil'
wider_column: right
---

![](en/privatebin01.gif)

---

# Fácil de usar
<ul class=disc>
<li>Pegar el texto, hacer click en “Enviar”, compartir la dirección URL.</li>
<li>Establecer un tiempo caducidad: 5 minutos, 10 minutos, 1 hora, 1 día, 1 semana, 1 mes, 1 año o nunca.</li>
<li>Opción “Quemar después de leer”: Lo pegado es destruido luego de ser leído.</li>
<li>Dirección URL de borrado única generada para cada pegado.</li>
<li>Coloreado de sintaxis para 54 idiomas (utilizando highlight.js), con soporte para combinaciones (html/css/javascript).</li>
<li>Conversión automática de direcciones URL en vínculos cliqueables (http, https, ftp y magnet).</li>
<li>Un solo botón para clonar un pegado existente.</li>
<li>Discusiones: Se puede habilitar una discusión en cada pegado.</li>
</ul>
