---
title: 'Chat'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Chat
Se puede utilizar el chat de texto junto con la conferencia de audio/video.

---

![](chat.png)
