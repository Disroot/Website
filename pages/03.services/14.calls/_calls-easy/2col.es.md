---
title: 'Fácil de usar'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Fácil de usar
Simplemente iniciar una reunión y compartir el enlace con otras personas.
Ni siquiera se necesita una cuenta para usarlo y puede ejecutarse en el navegador sin instalar ningún programa adicional en la computadora.

---

![](easy.png)
