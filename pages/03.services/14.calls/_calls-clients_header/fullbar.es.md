---
title: 'Clientes para Jitsi'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

# Usar nuestra aplicación preferida
Hay un montón de clientes para el escritorio/la web/el móvil para elegir. Podemos utilizar el que más nos guste.
