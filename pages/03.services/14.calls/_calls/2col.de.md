---
title: Calls
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://calls.disroot.org/">Konferenz beginnen</a>
<a class="button button1" href="http://oubguq76ii5svwyplbheorayf2nmoxy7inyd43a24adq24sy7jahjvyd.onion">Tor</a>

---

![](jitsi_logo.png?resize=150,150)

**Disroot's Calls-Dienst** ist eine Videokonferenz-Software auf der Basis von **Jitsi-Meet**. Der Service bietet Dir qualitativ hochwertige Video- und Audiokonferenzen mit so vielen Partnern, wie Du möchtest. Er ermöglicht es Dir außerdem, Deinen ganzen Desktop oder nur einzelne Fenster an andere Teilnehmer des Gesprächs zu streamen.


Disroot Calls: [https://calls.disroot.org/](https://calls.disroot.org/)

Projektseite: [https://jitsi.org/jitsi-meet/](https://jitsi.org/jitsi-meet/)

Quellcode: [https://github.com/jitsi/jitsi-meet](https://github.com/jitsi/jitsi-meet)
