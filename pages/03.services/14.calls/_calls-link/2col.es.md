---
title: 'Enlace personalizado'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](link.png)

---

# Enlace personalizado
Podemos elegir nuestro propio nombre de sala de conferencia, que será su dirección.
