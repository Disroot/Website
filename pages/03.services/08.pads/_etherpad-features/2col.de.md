---
title: 'Etherpad Funktionen'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Farbliche Markierungen

Alle Änderungen im Text sind mit einer Autorenfarbe markiert, die jedem einzelnen Nutzer des Pads zugeordnet ist. So ist es einfach herauszufinden, wer am Text was geändert hat.

## Textformatierung

Fett, kursiv, Absätze, Listen, Aufzählungen, etc.

## Chat

Online-Chat für alle Nutzer, die an dem Text arbeiten.

## Verlauf

Überprüfe die Änderungen des Textes im zeitlichen Ablauf mit Hilfe eines Zeitschiebers.

---


![](en/etherpad-example.png?lightbox=1024)
