---
title: 'Características de Ethepad'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Resaltado en color

Todos los cambios en el texto son marcados con colores de autoría asignados a cada usuarie en el bloc, haciendo sencillo encontrar quién editó qué.

## Estilo de texto

Negrita, itálica, párrafos, listas, viñetas, etc.

## Chat

Chat en línea para todas las personas que están trabajando en el texto.

## Historia

Para revisar los cambios realizados con un deslizador de tiempo.

---

![](en/etherpad-example.png?lightbox=1024)
