---
title: 'Características de Ethepad'
text_align: left
wider_column: right
---

![](en/etherpad-history.gif)

---

## Importar / exportar Blocs de notas

Importar y exportar blocs de notas en varios formatos (texto plano, HTML, Etherpad, Mediawiki).

## Guardar correcciones

Guardar una versión en particular de nuestro bloc de notas.

## Marcadores

Guardar localmente una lista de marcadores de los blocs visitados en el almacenamiento local del navegador.

## Comentarios

Una barra lateral para agregar comentarios que están vinculados directamente al texto.
