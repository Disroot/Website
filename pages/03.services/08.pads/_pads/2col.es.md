---
title: Blocs de notas
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://pad.disroot.org/">Abre un bloc</a>
<a class="button button1" href="http://b6mttjczryfoyz2go65hyjl5k6xfqqacpvz3ameurvraijxg5sv2z2id.onion">Tor</a>

---

![](etherpad.png)
## Edición colaborativa en verdadero tiempo real
Los blocs de **Disroot** están desarrollados por **Etherpad**. Un bloc es un texto en línea que podemos editar colaborativamente, en tiempo real, directamente en el navegador web. Los cambios de todas las personas se reflejan instantáneamente en todas las pantallas.

 Podemos escribir artículos, comunicados de prensa, listados de pendientes, etc, junto con amistades, colegas de estudio o trabajo.

No es necesaria una cuenta de **Disroot** para utilizar este servicio.

Se puede usar [**Padland**](https://f-droid.org/en/packages/com.mikifus.padland/) en **Android** para abrir o crear directamente blocs de **Disroot** en nuestro dispositivo.

Blocs de Disroot: [https://pad.disroot.org](https://pad.disroot.org)

Página del proyecto: [http://etherpad.org](http://etherpad.org)

Código fuente: [https://github.com/ether/etherpad-lite](https://github.com/ether/etherpad-lite)
