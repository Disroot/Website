---
title: 'Disapp, new donation page and planned registrations'
date: 10/11/2018
media_order: michael_scott.jpg
taxonomy:
  category: news
  tag: [disroot, news, adndroid, donations, apps]
body_classes: 'single single-post'

---

The last two weeks had some amazing surprises and developments. It was absolutely and positively motivating for us.


**1. Disroot app**

Many people have been asking for Disroot mobile app and we repeatedly failed to see the possibility of such app since we run multitude of services that, in most cases, provide their own separate app. Massimiliano was the one who actually saw the potential of a Disroot app and decided to take the challenge with an unexpected approach. He developed the Disroot "Swiss army knife" app that helps and guides disrooters to recommended apps, tips and tutorials on how to set everything up. The app will pick the best (in our opinion) app for email, chat, etc and for those services that do not have a dedicated app it will open them in the webview window. The app also provides directions to all community tutorials we've gathered over the years to help people use provided services. We are very thankful to Massimiliano for his brilliant idea and the hard work he put into it. The app is available from the one and only truly Free/Libre Android store: [here](https://f-droid.org/en/packages/org.disroot.disrootapp/)


**2. Changing notification email and Screen Name**

This is something we wanted to do since a while now and we finally managed. Since not everyone uses their Disroot email, and we in no way want to force people to do so, we wanted to make it possible for disrooters to use their email address of choice for all incoming notifications from services provided by the Disroot platform. As of today, you can login to [https://user.disroot.org](https://user.disroot.org) and update your profile with new notification email and also Screen Name.
At this moment only Nextcloud is configured to accept the changes. Once you update your profile, Nextcloud will be sending your notification emails to the given address. \o/
!Note! Due to a bug in nextcloud software although your screen name won't change for you on the interface on Nextcloud, your new name will be shown to people you share files with, calendars etc. We hope this will be solved soon.
As time progresses and we test other services we will be adding those too.


**2. Registration issue.**

Last two weeks we spent on thinking how to solve the issue of registrations. We've had few brainstorm sessions, some open discussions eg. [here](https://forum.disroot.org/t/how-to-solve-the-abuse-problem-on-disroot-org-platform/4229/23) and countless private talks, chats, etc. The outcome of all the discussions is satisfying and, most importantly, brings a possible solution to the problem. We will be busy with that in the next two weeks and hopefully we will re-open registrations soon after.


**3. New donation page.**

Last weeks Antilopa was busy punching all the numbers from all over the place (Paypal, Patreon, bank etc) into new accounting system that will help us have better overview on the current situation, the amount of donations we receive and our current costs. We still cant believe the outcome and are still recalculating it over and over. The result makes us very happy and we would like to thank everyone who decided to chip in and donate money to our project until now. But please don't quit with satisfaction yet - for long term sustainability we need you to keep this trend. As the platform grows so do the costs  and the amount of work we invest in it.
The new [donation page](https://disroot.org/en/donate) has more detailed information and hope to be as transparent as can be. We will be updating it each month as we do our administration. In the new donation page we will regularly put our short and and long term goals for the project.  At the moment we are still experimenting with the idea, so more goals will show up later (including eventually the ultimate goal and dream to be able to work fully on Disroot and quit our day jobs..)  

For now we have accomplished one goal we wanted to share with you:  

**Goal 1 - Donate to projects - Unlocked.**

We heavily depend on people creating the amazing software we provide to you every day. Without the hard work of those developers there would be no Disroot. It's therefore logical to support these people and projects financially as much as we can, and to help build a self sustainable model for libre/open source software.
As of next month we have decided to donate on regular basis to the projects listed below according to this formula:
As long as the donations we receive double our costs we will share 15% of our surplus with developers of software we use to provide disroot services.
At the moment we are considering donating to the following projects: Diaspora, Taiga, Nextcloud, Conversejs, Etherpad, Ethercalc, Privatebin, Lufi, Searx and Framasoft

All this is made possible by your generous donations! We are very grateful to everyone that decided to keep buying us a coffee. Make the donations flowing :) We are very thirsty for caffeine. :P


The next two weeks we will probably spend most of our time on implementing the new registration system to get back up on our feet. Additionally we will work on small improvements here and there and maybe play and experiment with something new for a change.


Ciao!


**PS.**

PS. This summer, we have upgraded our password store policy. We therefor would like to recommend you to update your password (even to the same one if you will). Once updated it will be stored in more secure way then before. You can do that by logging in to [https://user.disroot.org](https://user.disroot.org) and select "Change password" option. If you are changing to new password (which we recommend you do periodically anyway), please follow these instructions, specially when you use Nextcloud files: [https://howto.disroot.org/en/basics/how-to-change-your-disroot-user-password](https://howto.disroot.org/en/basics/how-to-change-your-disroot-user-password)
