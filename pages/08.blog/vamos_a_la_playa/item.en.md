---
title: 'Vamos a la playa'
date: '13-08-2019 17:00'
media_order: vamos.jpg
taxonomy:
  category: news
  tag: [disroot, news, CoreTeam]
body_classes: 'single single-post'

---


Is it just me or we have the hottest summer ever? We haven't posted anything about our last sprints, and this time we are also late. Of course we can blame it on the weather (thank you Climate change), but with all seriousness we have not spend that time on the beach as title would suggest, although probably we should have. The reason for this was that, charged with new energy with new members of the gang, we simply over estimated our capacity and took way more then we can carry on ourselves. This resulted in many tasks started but not as many finished on time. Lesson learned this time, we have decided on the current sprint titled **"No more than two User Stories per person"** to change that and try to create environment where we do not take a too heavy load. Hopefully this will prevent us from creating situation where we live under constant stress, trying to deliver what we promised but not managing to finish everything on time, which leads to burn-out, depressions, frustrations and other nasty dark things humans have to cope with. The new rule prevents that and creates much easier and happier environment. We hope it will work out good, about which you should probably read in a week time when we finish another sprint.
<br><br>
In the mean time, enjoy great weather, go to the beach before the catastrophic consequences of human existence kill us all.
