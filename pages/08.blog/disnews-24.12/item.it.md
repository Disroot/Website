---
title: 'DisNews 24.12 - Buon solstizio'
date: '17-12-2024'
media_order: crowd.png
taxonomy:
  category: news
  tag: [disroot, news]
body_classes: 'single single-post'

---
Buon solstizio a tutti!

Vi auguriamo un solstizio caldo, luminoso e felice. Che sia la notte più lunga o più corta dell'anno per voi, speriamo che possiate trarne il meglio e godervi questo evento astronomico e celebrando il cambiamento stagionale.

"Da Hobby a Missione: Disroot Ha Bisogno di Te!"

Cosa succede quando il tuo progetto amatoriale diventa essenziale per migliaia di persone? Non è più solo un hobby. Quest'anno, ci siamo resi conto che Disroot è a un punto di svolta. Il progetto è cresciuto oltre ciò che possiamo gestire nel nostro tempo libero e la nostra determinazione.

Per questo motivo, abbiamo iniziato a compensare economicamente muppeth con €1.000 al mese, con l'obiettivo di aumentare gradualmente questa somma — grazie alle vostre donazioni — fino a permettere un lavoro a tempo pieno su Disroot. Questo è un passo cruciale per garantire il futuro del progetto.

Ma c'è una sfida: stiamo mancando l'obiettivo finanziario di quest'anno. Ancora peggio, non riusciamo più a supportare i progetti open source su cui facciamo affidamento come facevamo una volta. Sebbene le donazioni siano aumentate dal 2020, il numero di sostenitori è rimasto lo stesso. A tutti voi fedeli contributori, grazie — ci avete permesso di andare avanti! 🙏

Tuttavia, abbiamo bisogno che più persone si uniscano. Anche piccoli contributi fanno una grande differenza quando più persone partecipano! Se apprezzate i servizi che forniamo e non avete ancora iniziato a supportarci, ora è il momento di agire. Insieme, possiamo sostenere una vera alternativa ai servizi delle grandi aziende tecnologiche (dove voi siete il prodotto).

🎉 Bonus: Sosteneteci e richiedete degli adesivi per mostrare il vostro amore per Disroot nella vita reale! Rendiamo il futuro di Disroot più luminoso, insieme. 💪

👉 Sentitevi liberi di condividere questo post e aiutarci a diffondere il messaggio!

#SupportDisroot #OpenSource #CommunityPower

Ci vediamo nel 2025!
