---
title: 'DisNews 24.12 - Happy Solstice'
date: '17-12-2024'
media_order: crowd.png
taxonomy:
  category: news
  tag: [disroot, news]
body_classes: 'single single-post'

---

Happy solstice everyone! 

We wish you a warm, bright, and happy solstice. Whether it is the longest or the shortest night of the year for you, we hope you make the best out of it and enjoy this astronomical event and celebration of seasonal change.

"From Hobby to Mission: Disroot Needs You!"

What happens when your hobby project becomes essential to thousands of people? It's no longer just a hobby. This year, we've realized Disroot is at a turning point. The project has grown beyond what we can maintain in our free time with duct tape and determination alone.

That's why we've started financially compensating muppeth with €1,000 per month, with the goal of increasing this gradually — through your donations — until we can pay full-time work on Disroot. This is a critical step to securing the project's future.

But here's the challenge: we're falling short of this year's financial target. Even worse, we're unable to support the open-source projects we rely on as much as we used to. While donations have increased since 2020, the number of supporters has stayed the same. To all of you loyal contributors, thank you — you've kept us going! 🙏

However, we need more people to join. Small contributions make a big difference when more people chip in! If you value the services we provide and haven't started supporting yet, now is the time to act. Together, we can sustain a real alternative to big tech services (where you are the product).

🎉 Bonus: Support us and ask for stickers to show your love for Disroot in real life! Let's make the future of Disroot brighter, together. 💪

👉 Feel free to share this post and help us spread the word!

#SupportDisroot #OpenSource #CommunityPower

See you in 2025!
