---
title: 'DisNews #11 - Lotta allo spam ad Halloween'
date: '17-11-2023'
media_order: our_tears_are_the_same.jpg
taxonomy:
  category: news
  tag: [disroot, news, spam, cryptpad, libretranslate, lacre, chatcontrol]
body_classes: 'single single-post'

---
Ciao Disrooters.

Vi siamo mancati? Siamo stati silenziosi ma non pigri. L'ultima volta avevamo scritto che il mondo diventa ogni mese più pazzo. Purtroppo, sembra che avevamo ragione. In attesa dell'imminente invasione di UFO (cos'altro può sorprenderci, vero?), ecco alcuni punti salienti di ciò che ci ha visti impegnati in questo ultimo periodo.

# Nuovi benefici: spazio di archiviazione extra sul Cryptpad
Ospitiamo Cryptpad da diversi anni e ne siamo davvero felici. È un ottimo insieme di servizi di collaborazione per coloro che tengono alla privacy, poiché tutti i dati sono crittografati end-to-end.

Recentemente, alcuni Disrooters ci hanno contattato per sapere se potevano avere uno spazio di archiviazione extra su Cryptpad, come facciamo per la posta e il cloud. Quindi, per prima cosa abbiamo esteso l'archiviazione gratuita **di default da 250MB a 500MB per tutti \o/**.

Abbiamo anche deciso di consentire agli utenti di allocare lo spazio di archiviazione extra pagato come desiderano tra cloud, posta e ora anche Cryptpad. Ad esempio, se un Disrooter fa una donazione per 10 GB di spazio di archiviazione, questo utente può decidere di avere 6 GB per il cloud, 2 GB per la posta e 2 GB per Cryptpad. Questi gigabyte extra verranno aggiunti allo spazio di archiviazione predefinito fornito al momento dell'iscrizione dell'account (quindi, nell'esempio precedente, l'utente avrebbe 2,5 GB di archiviazione extra su Cryptpad).

Consultate la nostra [pagina dei vantaggi](https://disroot.org/perks) per vedere i prezzi del deposito extra.

# Forgejo: supporto per le email in arrivo
Forgejo è il software che alimenta la nostra [istanza git](https://git.disroot.org). Il mese scorso abbiamo aggiunto una nuova funzionalità chiamata "email in arrivo". Questa funzione consente di eseguire diverse azioni via e-mail, come rispondere a problemi o menzioni. È particolarmente utile se si vuole rimanere attivi sul proprio progetto mentre si è in viaggio.

# Nuovi servizi
Per Halloween :), abbiamo distribuito due nuovi servizi.

Innanzitutto, abbiamo sostituito ConverseJS con **Movim** come servizio di chat xmpp. È molto più facile da usare, come molti di voi chiedevano. Oltre all'ottima interfaccia, fornisce anche una forma di funzionalità di social network federato, come le comunità e il blogging. Il tutto federato con altri nodi Movim/XMPP e in futuro parte del fediverso che alimenta la nostra istanza [akkoma instance](https://fe.disroot.org) e migliaia di altre nella rete. Vorremmo ringraziare tutte le persone coinvolte nel progetto [ConverseJS]( https://conversejs.org/) per il supporto e lo sviluppo di questa soluzione di webchat. In particolare vorremmo inviare un ringraziamento particolare a [jcbrand](https://mastodon.xyz/@jcbrand) per tutto il suo lavoro e supporto.

Il secondo è un servizio di traduzione che si trova al link [translate.disroot.org](https://translate.disroot.org). È basato sul software [LibreTranslate](https://github.com/LibreTranslate/LibreTranslate). La nostra istanza è ancora giovane e forse instabile, quindi se notate qualcosa che merita un feedback, sentitevi liberi di farlo. Ci aiuterà a rendere le cose stabili e fantastiche!

# Lotta allo spam
Come al solito, la guerra senza fine allo spam continua. Stiamo facendo del nostro meglio per combattere questa piaga. Questa volta abbiamo affrontato soprattutto lo SPAM in uscita da Disroot stesso. Abbiamo a che fare con un gran numero di utenti che creano account Disroot per inviare spam, phishing e altri abusi. Questo danneggia la nostra reputazione come dominio e fa sì che le e-mail inviate da Disroot in generale vengano considerate da altri come spam. Abbiamo aggiunto alcune misure per combattere questa attività senza danneggiare la privacy degli utenti e vediamo alcuni progressi in questo campo. Richiede costanza e l'aggiunta di un altro compito alla routine quotidiana, ma siamo sicuri che darà risultati. Ci auguriamo che Disroot diventi una seccatura troppo grande per gli abusivi, in modo che perdano interesse e si allontanino per cercare prede più facili.

Inoltre, abbiamo attivato una funzione su [webmail](https://webmail.disroot.org) che consente agli utenti di contrassegnare le e-mail come spam. Le e-mail contrassegnate vengono inoltrate automaticamente a noi, che possiamo utilizzarle per migliorare i filtri antispam sui nostri server. Se ricevete dello spam, contrassegnatelo come tale. Se invece ne ricevete molto, spostatelo nella cartella della posta indesiderata e scriveteci all'indirizzo support@disroot.org. Ci occuperemo noi della posta indesiderata per voi e la invieremo al nostro sistema di filtraggio dello spam, sempre affamato. Se si utilizza un client nativo, anche l'inoltro dello spam a spam.report@disroot.org e l'inoltro di ham (falso positivo) a ham.report@disroot.org è sufficiente.

# Nuove norme UE per distruggere la privacy degli utenti
Ancora una volta la nostra privacy e la nostra sicurezza su Internet vengono messe in discussione. Sembra che i governi non riescano a sopportare il fatto che Internet sia il nostro ultimo baluardo libero per lo scambio di informazioni e idee e stiano facendo il possibile per trasformare questo mezzo di comunicazione in un mezzo controllato, proprio come la radio, i giornali o la televisione.

Per prima cosa stanno arrivando alla vostra corrispondenza. La legislazione sul ChatControl, come risultato della guerra contro la pornografia infantile e l'adescamento, sta progettando di distruggere qualsiasi messaggistica con crittografia end-to-end, poiché le autorità richiedono ai fornitori di servizi di spiare tutti i messaggi per filtrare quelli sospetti. Distrugge i diritti fondamentali per la privacy della corrispondenza e manca totalmente l'obiettivo, poiché gli abusatori di minori continueranno a trovare il modo di distribuire i loro contenuti. È un approccio fallimentare come quello di risolvere il problema dei senzatetto con una legge o di vietare i coltelli da cucina perché potrebbero essere usati per fare del male o uccidere altre persone.

In secondo luogo, l'UE, a porte chiuse, sta cercando di spingere una legislazione che costringerà tutti i browser web (e non solo) a implementare autorità di certificazione e chiavi crittografiche selezionate dai governi dell'UE. In sostanza, ciò consentirebbe al governo (o a chiunque altro) in possesso di tali certificati di intercettare i dati crittografati scambiati con il sito web e di visualizzarli in chiaro in tempo reale. Ad esempio, le e-mail che inviate, i post che caricate, le immagini che visualizzate, le cose che scaricate o caricate, le persone con cui comunicate. In sostanza, tutto ciò che fate su Internet potrebbe essere visualizzato in tempo reale dallo Stato. Non si tratta di uno scherzo, né di un'inverosimile teoria della cospirazione. È la realtà!

La lotta per le nostre libertà continua fino a quando l'UE cercherà di implementare regolamenti presi direttamente dagli incubi totalitari. Consultate i link qui sotto e fatevi coinvolgere. Si tratta della nostra privacy e della nostra libertà:

  - [https://stopchatcontrol.eu/](https://stopchatcontrol.eu/)

  - [https://www.eff.org/deeplinks/2023/11/article-45-will-roll-back-web-security-12-years](https://www.eff.org/deeplinks/2023/11/article-45-will-roll-back-web-security-12-years)

  - [https://last-chance-for-eidas.org/](https://last-chance-for-eidas.org/)

  Date anche un'occhiata all'ottimo [video](https://yewtu.be/watch?v=TwGRS8IldH0) di Nicco Loves Linux su questo argomento.

# Cosa sta per succedere?
Nei prossimi due mesi vogliamo concentrarci sulla pulizia di fine anno. Cercheremo di ripulire, rispondere e finalizzare tutto ciò che è urgente nella coda dei ticket. Cercheremo quindi di rispondere a tutti i vecchi ticket in sospeso. Speriamo di dare il benvenuto al nuovo anno con una bella tabula rasa e con il minor arretrato possibile. Incrociamo le dita. 

Abbiamo cercato di resuscitare l'idea del dominio personalizzato XMPP, ma ci siamo scontrati con un muro nel processo e non siamo riusciti a renderlo operativo. La cosa positiva è che finalmente ci stiamo lavorando e cercheremo di trovare una soluzione al problema attuale. Speriamo che sia una questione risolvibile. Se siete interessati a seguire lo sviluppo di questa funzione o volete partecipare attivamente, controllate [la pagina di questo problema](https://git.disroot.org/Disroot/Disroot-Project/issues/319).

Entro la fine dell'anno speriamo di poter finalmente testare Lacre (il nostro approccio alla crittografia end-to-end delle caselle di posta). @pfm ha fatto parecchi progressi e pensiamo di essere pronti, quindi restate sintonizzati per ulteriori dettagli in arrivo.

Stay cool!

Il core team di Disroot
