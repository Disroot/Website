---
title: 'DisNews #11: Peleando contra el SPAM en Halloween'
date: '17-11-2023'
media_order: our_tears_are_the_same.jpg
taxonomy:
  category: novedades
  tag: [disroot, novedades, spam, cryptpad, libretranslate, lacre, chatcontrol]
body_classes: 'single single-post'

---
Hola, Disrooters.

¿Nos extrañaron? Bueno, silenciosos pero no perezosos. La última vez, escribimos que el mundo se estaba volviendo más loco cada mes. Lamentablemente, parece que no podíamos estar más en lo cierto. Mientras esperamos una inminente invasión extraterrestre (¿qué más falta para sorprendernos, no?), aquí van algunos puntos destacados sobre lo que estuvimos haciendo.


# Nuevo beneficio: almacenamiento extra en Cryptpad
Venimos alojando Cryptpad desde hace varios años ya y estamos muy contentos con eso. Es un gran conjunto de servicios de colaboración para quienes valoran la privacidad ya que todos los datos están cifrados de extremo-a-extremo.

Recientemente, algunes Disrooters nos contactaron para ver si podían tener almacenamiento extra en Cryptpad, como lo hacemos para el correo y la nube. Así que primero ampliamos **el almacenamiento gratuito por defecto de 250MB a 500MB para todes \o/**.

También decidimos permitir que la persona usuaria asigne como desee el almacenamiento extra que paga entre la nube, el correo y ahora también Cryptpad. Por ejemplo, si alguien dona para tener almacenamiento de 10 GB, podría decidir tener 6GB para la nube, 2GB para correo y 2GB para Cryptpad. Estos gigas adicionales se añadirán sobre el espacio de almacenamiento predeterminado que se obtienen con el registro de una cuenta (siguiendo el ejemplo anterior, la persona tendría 2,5 GB de almacenamiento adicional en Cryptpad).

Revisen nuestra [página de Beneficios](https://disroot.org/perks) para ver los costos del almacenamiento adicional.


# Forgejo: soporte para correos entrantes
Forgejo es el software que motoriza nuestra [instancia de git](https://git.disroot.org).
El mes pasado añadimos una nueva característica llamada "correo entrante". Esta funcionalidad permite que varias acciones puedan ejecutarse por correo electrónico, como responder a incidencias o menciones. Es especialmente útil si quieren mantenerse actives en sus proyectos mientras están en movimiento.


# Nuevos servicios
Para Halloween :), implementamos dos nuevos servicios.

Primero, remplazamos **ConverseJS** con **Movim** como servicio de chat XMPP. Es mucho más amigable con les usuaries, que es algo que bastantes de ustedes estaban pidiendo. Además de su fantástica interfaz, brinda también algunas características de red social federada como comunidades y blog. Todo esto federa con otros nodos de Movim/XMPP y en el futuro lo hará con nuestra instancia del Fediverso, impulsada por [Akkoma](https://fe.disroot.org) y miles de otras en la red. Queremos agradecer a toda la gente involucrada en el proyecto [ConverseJS]( https://conversejs.org/) por mantener y desarrollar esta solución de webchat. Nos gustaría enviar nuestro cariño y saludos a [jcbrand](https://mastodon.xyz/@jcbrand) por todo su trabajo y apoyo. ¡Gracias por todo el pescado!

Segundo, un servicio de traducción que pueden encontrar en [translate.disroot.org](https://translate.disroot.org). Está impulsado por el software [LibreTranslate](https://github.com/LibreTranslate/LibreTranslate). Nuestra instancia está verde y probablemente con alguna inestabilidad, así que si notan algo que valga la pena mencionar, por favor, siéntanse libres de hacerlo. ¡Nos ayudará a mantener las cosas estables y geniales!


# Peleando contra el SPAM
Como ya es habitual, la batalla interminable contra el spam continúa. Estamos tratando de combatir esta plaga. Esta vez hemos abordado principalmente el SPAM saliente desde Disroot misma. Estamos lidiando con una cantidad importante de abusadores creando cuentas de Disroot para enviar spam, hacer phishing y otros abusos. Esto perjudica nuestra reputación como dominio y termina generando que correos electrónicos enviados desde Disroot en general, sean vistos por otros como spam. Añadimos algunas medidas para combatir esta actividad sin dañar la privacidad de les usuaries y observamos algunos avances en el campo. Requiere constancia y suma una tarea más a la rutina diaria, pero estamos seguros que dará resultados. Esperamos que Disroot se vuelva una molestia para esta escoria abusiva, que pierdan el interés y se vayan a buscar una presa más fácil.

Adicionalemnte, habilitamos una función en el  [webmail](https://webmail.disroot.org) que permite a las personas usuarias marcar correos como spam. Estos son automáticamente reenviados a nosotros y podemos utilizarlos para seguir enseñando a los filtros antispam de nuestros servidores. Si reciben algún spam, por favor, márquenlo como tal. Si reciben un montón, muévanlos a la carpeta de Correo Basura y escríbannos a support@disroot.org. Nos encagaremos de los correos no deseados y alimentaremos a nuestro siempre hambriento sistema de filtrado de spam. Si usan un cliente nativo de correo, reenviar el spam a spam.report@disroot.org y los falsos positivos a ham.report@disroot.org también servirá.


# La nueva normativa de la UE destruirá la privacidad de les usuaries
Una vez más, nuestra privacidad y seguridad en internet están amenazadas. Parece que los gobiernos no pueden soportar el hecho de que sea nuestro último bastión libre para intercambiar información e ideas y están haciendo lo posible por convertir ese medio en uno controlado, como la radio, la prensa o la televisión.

Primero van a por sus correspondencias. La legislación de ChatControl, que es el resultado de la guerra contra la pornografía infantil y el acoso de menores, planea destruir cualquier tipo de mensajería cifrada de extremo-a-extremo, ya que las autoridades exigen a los proveedores de servicios que husmeen en todos los mensajes para filtrar los sospechosos. Destruye derechos fundamentales a la privacidad de la correspondencia y no logra el objetivo, porque los abusadores de menores seguirán encontrando la forma de distribuir sus contenidos. Es una estrategia errada, como querer resolver el problema de las personas sin hogar con una legislación o prohibir los cuchillos de cocina porque podrían usarse para dañar o matar a otras personas.

En segundo lugar, la UE está tratando de impulsar a puerta cerrada una legislación que obligará a todos los navegadores web (y no solo a ellos) a implementar autoridades de certificación y claves criptográficas seleccionadas por los gobiernos de la UE. Esto permitiría fundamentalmente al gobierno (o a cualquiera) que esté en posesión de dichos certificados interceptar los datos cifrados que ustedes intercambien con el sitio web y verlos en tiempo real. Por ejemplo, los correos electrónicos que envíen, las publicaciones, las cosas que suban o descarguen, las imágenes que vean, las personas con las que se comuniquen. Esencialmente, todo lo que hacen en internet podría ser visto en tiempo real por el Estado. Esto no es broma ni una teoría conspirativa descabellada. ¡Es nuestra realidad!

La lucha por nuestras libertades continuará en la medida que la UE intente implantar normativas sacadas directamente de pesadillas totalitarias. Consulten los siguientes enlaces (en inglés) y participen. Al fin y al cabo, se trata de su privacidad y sus libertades:

  - [https://stopchatcontrol.eu/](https://stopchatcontrol.eu/)

  - [https://www.eff.org/deeplinks/2023/11/article-45-will-roll-back-web-security-12-years](https://www.eff.org/deeplinks/2023/11/article-45-will-roll-back-web-security-12-years)

  - [https://last-chance-for-eidas.org/](https://last-chance-for-eidas.org/)

También pueden ver este gran [video](https://yewtu.be/watch?v=TwGRS8IldH0) de Nicco Loves Linux sobre el tema.


# ¿Qué viene ahora?
Los próximos dos meses queremos enfocarnos en una buena limpieza de fin de año. Intentaremos limpiar, responder y finalizar todo lo que sea urgente o fácil de cerrar en nuestro tablero de incidencias, tickets de soporte y todo lo empezado pero sin terminar. También intentaremos responder a todos los tickets pendientes más antiguos. Esperamos dar la bienvenida al nuevo año con una pizarra bien limpia y el menor retraso posible. Crucen los dedos. 

Hemos intentado resucitar la idea del dominio XMPP personalizado pero nos topamos con un muro en el proceso y no hemos conseguido ponerlo en marcha. Lo bueno, sin embargo, es que por fin estamos trabajando en ello e intentaremos encontrar una solución al problema que tenemos actualmente. Esperemos que sea un asunto solucionable. Si les interesa seguir el desarrollo de esta característica o participar activamente, vean [esta página de incidencias](https://git.disroot.org/Disroot/Disroot-Project/issues/319).

También esperamos empezar finalmente, antes que termine el año, las pruebas de Lacre (nuestro proyecto de cifrado de extremo-a-extremo del buzón de correo). @pfm ha avanzado bastante y pensamos que estamos listos, así que manténganse al tanto para más detalles muy pronto.


¡Mantengan la calma!

- El equipo de Disroot
