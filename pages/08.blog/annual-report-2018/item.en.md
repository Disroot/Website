---
title: 'Happy New Year! - Disroot Annual Report 2018'
date: '11-01-2019 20:00'
media_order: beets3.jpg
taxonomy:
  category: news
  tag: [disroot, news, matrix, donations, report]
body_classes: 'single single-post'

---

Hi there Disrooters! We would like to take this opportunity to wish you all a great year.  2018 was packed with excitement and plenty of work here at Disroot, all about which you can read in our annual report. Make yourself some hot-coco, sit comfortable and read through **[the report!](https://disroot.org/annual_reports/AnnualReport2018.pdf)**


There are few additional announcements we would like to share:

**Communication**

Last year we decided to stop sending announcements on changes via email. This was because we found it both too intrusive and time consuming to do so. Especially so since we decided to change the way we work. Since September we are working in two-week sprints instead of the four-week long sprints we had before. We announce the developments via the blog and social media posts every two weeks. Planning on a bi-weekly basis gives us better control and overview. We can now better estimate the planned work load and not overload the sprint, what often led to sprints dragging well over 4 weeks, ruining our planning and weakening our control over the project.
Therefor we will do as follows:
    - Every two weeks (more or less) we will publish a post with changes and plans for next sprint just like we did in the last quarter of the year.
    - Every quarter of the year we will send out email announcement to all users with recap of work done in that period of time.
In this way we give detailed overview and insight for those who want to follow development of disroot in pretty much real time and for all others we provide general none intrusive overview every quarter of the year.

**Inactive matrix user purge:**

As we informed in [the blog post](https://disroot.org/en/blog/matrix-closure) form 7th September 2018, we have decided to phase out matrix. Few months ago we started denying access for new users as well as purging public room history longer then 4 months. Now it is time to enter the second phase of the process which is deactivating inactive users. We will roll this out in two steps.

1. Next week, we will deactivate all users that did not login for a whole year.
2. In first week of February we will deactivate all matrix accounts that did not login for a period longer than 4 months. This procedure will be run on regular basis.


**Changes in email and nextcloud storage**

As of January 1st we've decided to cut the free storage quotas in half for all new users (1GB mailbox storage and 2GB cloud). We need to make this change as our resources are depleting too fast. The current amount of donations that are coming in is not enough to sustain the project's infrastructure for much longer. We've heard many disrooters voicing similar suggestions in the past and after some reflection we now agree.

**Hey it's the beginning of the year. Time to cleanup.**

It's a good time to look at your usage at Disroot and try to clean up a little. Check you email and remove all those hundreds of useless notification emails, get rid of bullshit ads and other post you would most likely throw in to the trash if it would come by regular post.
And what about all those pictures from our camera phones? Thanks to nextcloud they can all be directly uploaded to your cloud. To help clean up there too we've installed this great little [app](https://cloud.disroot.org/apps/keeporsweep/) in Nextcloud called [Keep or Sweep](https://apps.nextcloud.com/apps/keeporsweep). Now go delete those pocket pics :P
