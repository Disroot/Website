---
title: 'Caída causada por un cable de alimentación'
media_order: cables.jpeg
published: true
date: '22-02-2021 13:00'
taxonomy:
    category:
        - novedades
    tag:
        - disroot
        - novedades
        - caída
body_classes: 'single single-post'

---

Quienes siguen nuestra página de [estado](https://state.disroot.org) probablemente saben que el domingo migramos todos nuestros servidores a un nuevo armario rack. La operación se hizo sin problemas y las cosas volvieron a estar en línea en una hora.

El domingo, alrededor de la medianoche (CET), uno de nuestros servidores se cayó brevemente. Como los discos están cifrados, todos los servicios que están en esa máquina estuvieron fuera de servicio hasta que uno de los administradores se despertó para ver la situación. Rápidamente restauramos el servidor y todo volvió a estar en línea, pero solo por un rato.<br>
Después de haber puesto las cosas en línea, enviamos un correo electrónico al centro de datos preguntando si era posible que alguien hubiera desconectado por error nuestro servidor (cosas que pasan). Mientras nos alegrábamos porque los servicios volvían a estar en línea y actualizábamos nuestras redes sociales y nuestro sitio de estado, notamos que que el servidor se había reiniciado de nuevo. Al parecer, uno de los ingenieros del centro de datos se acercó al rack, vio que uno de los cables no estaba enchufado del todo y trató de empujarlo. Mientras lo hacía se dio cuenta que el servidor se reiniciaba.<br>
Para no causar más caídas inesperadas, **Muppeth** se apresuró a ir al centro de datos para ver cuál era el problema. Preparado con cinta adhesiva, ganchitos y un martillo (siempre hay que llevar un martillo, nunca se sabe), procedió a inspeccionar la situación. Efectivamente, uno de los cables estaba ligeramente fuera. Empujando el cable en su posición parece que se resolvió el problema. El cable se asienta ahora muy bien en el enchufe y no parece querer ir a ninguna parte.


Nos disculpamos profundamente por el tiempo de inactividad y esperamos que su semana haya comenzado mejor para ustedes que para nosotros.<br>
Nos deseamos a nosotros y a todxs ustedes que el resto de la semana sea productiva, relajada y feliz.
