---
title: 'DisNews 24.11 - Lacre beta abierta'
date: '13-12-2024'
media_order: lacre.png
taxonomy:
  category: novedades
  tag: [disroot, novedades, lacre]
body_classes: 'single single-post'

---

Hola,

**Aviso Importante!**

Primero, una información importante para todas las personas usuarias del correo. Es posible que hayan recibido varios correos electrónicos informándoles que deben actualizar su cuenta de Disrrot o eliminar correos del buzón. Queremos advertirles que esos correos electrónicos **SON FRAUDULENTOS** y **NO DEBEN NUNCA** hacer clic en ninguno de los enlaces que aparecen en ellos. Nunca enviamos este tipo de mensajes e incluso si en el futuro les enviáramos notificaciones informándoles que su buzón está lleno, nunca les proporcionaremos enlaces para que hagan clic. Todo lo que haremos será enviarles información para que puedan conectarse a sus correos electrónicos como hacen siempre para comprobarlo. Siempre pueden ponerse en contacto con nosotros para asegurarse de que el mensaje que han recibido es legítimo. ¡Internet está llena de gente que intenta aprovecharse de todos nosotros!

Ahora, pasemos al tema principal.

Ha llegado el momento de pasar a la siguiente fase de las pruebas de Lacre. Nuestra solución integral para el cifrado de correo electrónico entrante está lista para ser probada a mayor escala. Durante las últimas semanas hemos estado ejecutando Lacre en el servidor de correo electrónico de Disroot para comprobar algunos errores desagradables y casos extremos antes de que más gente empiece a usarlo. Con solo un puñado de claves subidas por valientes madrugadores hemos conseguido encontrar algunos pequeños problemas molestos y solucionarlos. 

Ahora estamos listos para seguir adelante. Hemos creado una dirección de correo electrónico específica: 
`lacre_keys@disroot.org` donde pueden enviar sus claves públicas GnuPG (PGP). Las claves se añadirán manualmente al sistema y todos los correos electrónicos entrantes dirigidos a ustedes se cifrarán automáticamente por defecto. Decidimos hacerlo de esta manera, en lugar de abrir la función de carga automática a todo el mundo, para evitar cargas accidentales de claves por parte de personas que no estén bien versadas en GnuPG o en cifrado en general. Lacre proporciona un cifrado de extremo a extremo real y sin compromisos que tiene sus pros y sus contras. Si no saben lo que están haciendo (y accidentalmente borran su par de claves en un cliente o pierden las claves, por ejemplo) pueden perder irrevocablemente el acceso a sus correos cifrados con Lacre. Este método no nos permite, ni a nosotros ni a nadie, descifrar los correos ya cifrados sin sus claves secretas en el dispositivo. Así que para aplicar la máxima precaución en estos primeros días de Lacre en Disroot, queremos proporcionar la beta actual solo a aquellas personas que estén familiarizadas con los fundamentos del cifrado.

El objetivo de esta prueba es ver cómo funcionan las cosas bajo carga, si encontramos más casos extremos en los que los correos electrónicos no se cifran por una razón u otra, o si las cosas simplemente se rompen. Mantendremos informadas regularmente por correo a todas las personas que participen. Publicaremos todos los problemas posibles, si por alguna razón desactivamos el cifrado o si hubo alguna mejora digna de mención. También intentaremos recabar sus opiniones para ayudar a mejorar el servicio.

Una vez que estemos seguros de la implementación actual, planeamos abrir un servicio que permita la autogestión de las claves de cifrado (cargar nuevas claves, eliminar claves, etc.). Esto requerirá mejoras en la integración. El plan es no comprometer su seguridad (almacenando la clave privada en el servidor de una forma u otra) pero al mismo tiempo asegurarnos de que hasta sus abuelas puedan disfrutar del buzón cifrado. Es un largo camino, con pequeños pasos lo conseguiremos. 

Así que, por favor, suban sus claves y ¡disfruten de su buzón cifrado!

