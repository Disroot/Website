---
title: 'DisNews 24.11 - Beta aperta di Lacre'
date: '13-12-2024'
media_order: lacre.png
taxonomy:
  category: news
  tag: [disroot, news, lacre]
body_classes: 'single single-post'

---

Ciao,

**Nota Importante!**

Prima di tutto, un'informazione importante per tutti gli utenti e-mail. Potresti aver ricevuto diverse e-mail che ti invitano ad aggiornare il tuo account Disroot o a rimuovere messaggi dalla casella di posta. Vorremmo avvisarti che queste e-mail **SONO UNA TRUFFA** e **NON DEVI MAI** cliccare sui link forniti in esse!  Non inviamo mai messaggi di questo tipo e, anche se in futuro dovessimo inviare notifiche per informarti che la tua casella di posta è piena, non includeremo mai link da cliccare. Ci limiteremo a inviarti un'informazione, così potrai accedere alla tua e-mail come fai di solito per controllare. Puoi sempre contattarci per assicurarti che il messaggio ricevuto sia legittimo. Internet è pieno di persone che cercano di approfittarsi di noi tutti!

Ora passiamo all'argomento principale.

È giunto il momento di passare alla fase successiva del test di Lacre. La nostra soluzione completa per la crittografia delle e-mail in arrivo è pronta per essere testata su una scala più ampia. Nelle ultime settimane abbiamo eseguito Lacre sul server di posta elettronica di Disroot per individuare fastidiosi bug e casi limite prima che più persone iniziassero a utilizzarlo. Con solo una manciata di chiavi caricate dai coraggiosi pionieri, siamo riusciti a scoprire alcuni piccoli problemi fastidiosi e a risolverli.

Siamo ora pronti a procedere. Abbiamo creato un indirizzo e-mail dedicato: `lacre_keys@disroot.org` dove puoi inviare la tua chiave pubblica GnuPG (PGP). Le chiavi saranno quindi aggiunte manualmente al sistema, e tutte le e-mail in arrivo indirizzate a te dovrebbero essere automaticamente crittografate per impostazione predefinita.
Abbiamo deciso di procedere in questo modo, invece di abilitare la funzione di caricamento automatico per tutti, per evitare che vengano caricate accidentalmente chiavi da persone che non hanno familiarità con GnuPG o la crittografia in generale.
Lacre offre una crittografia end-to-end autentica e senza compromessi, che ha i suoi vantaggi e svantaggi. Se non sai esattamente cosa stai facendo (e cancelli accidentalmente la tua coppia di chiavi sul client o perdi le chiavi), potresti perdere irrevocabilmente l'accesso alle e-mail crittografate con Lacre. 
Questo approccio non consente a noi, o a chiunque altro, di decifrare le e-mail già crittografate senza la tua chiave segreta sul tuo dispositivo. Per questo motivo, vogliamo applicare la massima cautela in questi primi giorni di Lacre su Disroot, fornendo l'attuale beta solo a chi ha familiarità con i principi base della crittografia.

L'obiettivo di questo test è verificare come il sistema si comporta sotto carico e se riusciamo a identificare ulteriori casi limite in cui le e-mail non vengono crittografate per qualche motivo, o se il sistema presenta malfunzionamenti. Terremo tutti i partecipanti regolarmente informati tramite e-mail. Pubblicheremo aggiornamenti su eventuali problemi riscontrati, sulle motivazioni per cui potremmo dover disabilitare temporaneamente la crittografia o su eventuali miglioramenti degni di nota. Cercheremo anche di raccogliere il vostro feedback per migliorare il servizio.

Una volta che saremo sicuri dell'attuale implementazione, abbiamo in programma di aprire un servizio che consenta la gestione autonoma delle chiavi di crittografia (caricamento di nuove chiavi, rimozione delle chiavi, ecc.). Questo richiederà miglioramenti nella fase di onboarding. L'obiettivo è non compromettere la vostra sicurezza (come il salvataggio delle chiavi private sul server in qualsiasi forma) e, allo stesso tempo, fare in modo che persino la vostra nonna possa beneficiare della crittografia della casella di posta. È un percorso lungo, ma passo dopo passo ci arriveremo.

Quindi, per favore, caricate le vostre chiavi e godetevi la vostra casella di posta crittografata!

