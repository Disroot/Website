---
title: 'DisNews 24.11 - Lacre open beta'
date: '13-12-2024'
media_order: lacre.png
taxonomy:
  category: news
  tag: [disroot, news, lacre]
body_classes: 'single single-post'

---

Hi there,

**Important Note!**

First of all, an important information to all E-mail users. You might have been getting several emails informing you to update your Disroot account or remove mails from mailbox. We would like to warn you - those E-mails **ARE SCAM** and **YOU SHOULD NEVER** click any links provided in those! We never send such messages and even if we would send notifications in the future, informing you that your mailbox is full, we will never provide links for you to click on. All we will do is send you information, so you can log in to your E-mail as you always do to check. You can always contact us to make sure the message you received is legit. The internet is filled with people trying to take advantage of us all!

Now, on to the main topic.

It is time to move to the next phase of Lacre testing. Our end-to-end solution to incoming E-mail encryption is ready to be tested on a broader scale. For the past weeks we have been running Lacre on the Disroot email server to check some nasty bugs and edge cases before more people start using it. With just a handful of uploaded keys from brave early birds we managed to find some little annoying issues and to take care of those. 

We are now ready to move on. We've launched a dedicated E-mail address: `lacre_keys@disroot.org` where you can submit your GnuPG (PGP) public key. Keys will then be manually added to the system and all incoming E-mails addressed to you should be automatically encrypted by default. We decided to do it this way, rather than opening auto-upload feature to everyone, to prevent accidental key uploads from people who aren't well versed in GnuPG or encryption in general. Lacre provides a true and uncompromised end-to-end encryption which has it's pros and cons. If you don't know what you're doing (and accidentally delete your key pair on a client or loose keys) you may irrevocably loose access to your E-mails encrypted with Lacre. This approach does not allow us, or anyone else for that matter, to decrypt already encrypted E-mails without your secret key on your device. So to apply as much caution in those early days of Lacre on Disroot, we want to provide current beta to only those who are familiar with the basics of encryption.

The focus of this test is to see how things work under load and whether we find more edge cases where E-mails don't get encrypted for one reason or another, or if things simply break. We will keep everyone participating informed on regular basis via E-mail. We will post about all possible issues, whether for some reason we disable encryption or if there was any improvements worth mentioning. We will also try to gather some feedback from you to help improve the service.

Once we are confident with current implementation, we plan to open a service allowing for self-management of encryption keys (uploading new keys, removing keys, etc). This will require improvements in onboarding. The plan is to not compromise on your security (storing private key on the server in one way or another) but at the same time make sure that even your grandma can enjoy mailbox crypto. It's a long way, with small steps we will get there. 

So please, upload your keys, and enjoy your encrypted mailbox!

