---
title: 'DisNews #8 - ¡Primer servidor nuevo y Feliz Año Nuevo!'
date: '25-12-2022'
media_order: roos.jpg
taxonomy:
  category: news
  tag: [disroot, novedades, servidor, lacre, cifrado]
body_classes: 'single single-post'

---
¡Hola!, casi el final de otro año más. Queremos desearles a todxs un ¡Feliz Solsticio, geniales celebraciones de Año Nuevo y un grandioso, asombroso e increíble 2023!

A continuación, lo más sobresaliente de lo sucedido en Disroot durante el último mes y medio.

## Nuevo servidor: Roos

Anteriormente anunciamos la compra de nuevos servidores para Disroot. Estos nuevos equipos remplazarán a los actuales que ya alcanzaron hace tiempo el final de su vida útil y es momento de hacer la actualización. Como esta operación lleva bastante trabajo, especialmente para minimizar el tiempo de interrupción, decidimos remplazar un servidor a la vez. El 13 de noviembre empezamos con el primero. Roos ha remplazado al servidor responsable del manejo de las bases de datos (Nelly). Es un verdadero impulso en la performance del hardware y aunque todavía nos falta algo de trabajo de optimización, ya pueden sentir la diferencia. El nuevo servidor nos permitirá futuras mejoras en el rendimiento así como también agregar más servicios.


## Lacre

Estamos muy contentos de anunciar que nuestro trabajo en Lacre (nuestro proyecto de buzón de correos cifrado de extremo-a-extremo) ha alcanzado la versión 0.1 y está casi listo para probarse en el campo de batalla en Disroot. Como saben, anunciamos pruebas beta abiertas unas semanas atrás. Sin embargo, debido a un bug (error) que notamos durante las pruebas alfa cerradas en el servidor de Disroot (sí, ya lo implementamos pero ha sido deshabilitado desde entonces), decidimos posponerlas hasta que el problema esté resuelto. @pfm junto a otros colaboradores de la comunidad están trabajando duro para reproducirlo y encontrar una solución (agradecimientos especiales a *@darhma, @Onnayaku,* y *@l3o*). El error es uno bastante complicado de resolver porque es difícil de reproducir y @pfm está perdiendo la cabeza con él. Aunque ha estado trabajando sin descanso y está cerca de resolverlo. Esto significa que pronto intentaremos testear el cifrado de extremo-a-extremo del buzón en Disroot, y tal vez configurarlo de manera permanente. Si están en el Fediverso, ¡[denle](https://edolas.world/users/pfm) algo de amor!

## Akkoma/Pleroma, ¿Estamos en Mastodon? ¿Quién es John Mastodon y qué es esto del Fediverso?

Uno de los pilares de Disroot es apoyar servicios federados. La federación crea redes descentralizadas de proveedores de servicios permitiendo a las personas usuarias interactuar entre ellas aún cuando estén alojadas en servidores diferentes. Uno de los ejemplos principales de servicios federados es el correo electrónico. Lxs usuarixs de Disroot pueden enviar correos a cualquier usuarix alojadx en cualquier otro lado (p.ej. Tutanota, Posteo, Riseup o incluso los corporativos como Gmail or Yahoo!). No hay un propietario central del servicio de email y por lo tanto todos los proveedores tienen que cooperar unos con otros y permitir la intercomunicación. Nadie se imagina teniendo que crear cuentas separadas en Disroot, Gmail, y cientos de miles de otros servicios para comunicarse.

Desde los albores de Disroot, estuvimos brindando redes sociales federadas. Desde 2020, hemos buscado alguna nueva plataforma de red social basada en un nuevo protocolo interoperable llamado ActivityPub. Probamos una cantidad de servicios como Hubzilla, Mastodon, Pleroma y al final nos decidimos por Akkoma, que es un fork de Pleroma. Pero lo más importante es que es compatible con la familia entera del protocolo ActivityPub. Este es un protocolo de red que permite que una multitud de servicios como microblogging, hospedaje de video, compartir archivos, etc., puedan comunicarse unos con otros. Esto quiere decir que no solo lxs usuarixs de Akkoma pueden comunicarse entre ellxs sino que pueden hacerlo con todo el Fediverso, una red que utiliza principalmente el protocolo ActivityPub y permite la intercomunicación entre diferentes plataformas como, por ejemplo, Mastodon/Pleroma/Akkoma (microblogging como Twitter), PeerTube (publicación de video como YouTube), PixelFed (publicación de fotografías como Instagram), y muchos otros. Imaginen tener una cuenta de Instagram interactuando con una publicación de Facebook o comentar en Reddit sin la necesidad de salir de Instagram o tener una cuenta en Reddit. ActivityPub es un protocolo que está creciendo muy rápidamente con toda clase de servicios nuevos emergiendo.

Queremos invitar a todxs ustedes a nuestra instancia de microblogging en el Fediverso llamada [FEDIsroot](https://fe.disroot.org). Este es nuestro primer embarque en la red y estaremos brindando más servicios del Fediverso en el futuro. Nuestra instancia de Akkoma está funcionando bien, aunque todavía hay algunos pocos problemas aquí y allá que necesitamos resolver en las próximas semanas (por ejemplo, los nombres de usuarix con puntos no son aceptados debido a un problema en el que ya estamos trabajando). Si experimentan cualquier inconveniente con fe.disroot.org, tienen sugerencias, etc., por favor utilicen nuestros canales de comunicación para contactarnos. Sus comentarios y sugerencias son muy importantes, especialmente para un servicio tan joven como este.

**Les deseamos a todxs un increíble 2023 \o/**