---
title: 'FLISoL 2021'
media_order: flisol_2021.jpg
published: true
date: '17-04-2021 00:30'
taxonomy:
    category:
        - noticias
    tag:
        - flisol
body_classes: 'single single-post'

---
# FLISoL Online 2021

La **Comunidad Latina de Tecnologías Libres**, el **Club de Software Libre** y **Libera Tu Radio** organizan e invitan a una nueva edición online del **FLISoL**, **Festival Latinoamericano de Instalación de Software Libre**, uno de los mayores eventos de instalación y difusión del software libre del mundo.

El festival comienza el **domingo 18 de Abril, a las 6 PM (UTC -3)** y se extenderá hasta el sábado 24.

#### **Este año la emisión será la primera realizada íntegramente con software libre**

El evento está dirigido a todas las personas interesadas en conocer más acerca del software y la cultura libre. En el encuentro hay charlas, ponencias y talleres en torno al Software Libre, en toda su gama de expresiones: artística, académica, empresarial y social.

Más información sobre el evento (charlas, horarios, etc) en:

## [**caminoalflisollibre2021.softlibre.com.ar**](https://caminoalflisollibre2021.softlibre.com.ar/)
