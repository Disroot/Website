---
title: 'Discontinuing RSS service'
media_order: paperpile.jpg
published: true
date: '05-04-2021 17:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - nextcloud
        - news
        - rss
body_classes: 'single single-post'
---
Ok, this one is probably going to annoy some of you, but hopefully our argument will be convincing.

We have decided to stop offering **Nextcloud News** (RSS reader) or any alternative online RSS reader.<br>
The reason for this decision is purely the hardware resources needed to maintain it and the fact that the service is greatly abused and misused by a large majority of users.

For a while now we are struggling with situation where we pull a lot of news items (about 1 million a week), out of which about 10k-20k of them are set to read after three weeks. That means that most of the items we pull and store on the server are never read. This is because it is so easy to keep adding more websites to read but in fact never read them (_top users have few thousand sites they pull items from_).

Situation got so bad that the space in the database for news items alone exceeds the amount needed to store all file cache, calendars, contacts, project boards (you name it) that are currently stored on the server by factor of two. We have reached out to the developers but they were not interested in finding a solution as they simply did not intend to have this app working at such scale. Thus, we have turned to alternatives and considered deploying **TTRSS** service as a replacement. This service does offer better admin settings where we could easily get rid of old unread items, however while working on the deployment, we have spoken with few people that already provide this service and learned that, in the long run, the situation might not be that much better as issue with performance and database space have also here a great impact.

This leads us to one conclusion. As users of RSS ourself, we realize we don't really need online service to do things for us. All [RSS readers](https://alternativeto.net/browse/search/?q=feed%20reader&license=opensource), available on all platforms and operating systems, do provide option to use RSS locally. That means that your app/client would do the work online service does directly on the device (keep list of feeds, pull new items, allow for reading, etc). Seeing how litter-prone online solutions are, we don't see much of a reason for a middle man in the form of online services. With OPLM files, which you can easily export/import, you can use the same feed list on multiple devices, or have possibility to store as backup. The only advantage of having an online RSS solution is the fact you can hide your IP address from the websites you pull items from (though if you pull images or other elements your client's IP will be logged anyway), but we think this is not a good enough reason to create such overhead.

This is the reason we have decided to shut the service down and promote using your clients locally (just like we do). Here is what is going to happen to allow as much time for all of you to migrate to other online services or go local:

  - As of now, all feed items (articles) have been wiped from our database
  - By default News app is disabled for new users
  - We won't be pulling new items
  - ⚠️ **We will shutdown News app completely by 31st May 2021** so you have enough time to export your OPLM file before that.

<br>
You can check how to export your feeds [here](https://howto.disroot.org/en/tutorials/user/gdpr/nextcloud/news)
