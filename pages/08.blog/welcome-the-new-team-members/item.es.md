---
title: 'Bienvenidos, nuevos miembros del Equipo'
date: '23-06-2019 0:10'
media_order: penguins.jpg
taxonomy:
  category: news
  tag: [disroot, news, CoreTeam, xmpp, nextcloud]
body_classes: 'single single-post'

---

Esta vez estamos trayendo varios anuncios y algunas grandes noticias que no podemos esperar para compartir con ustedes.

## Mejoras en la performance de Nextcloud
Las últimas dos semanas nos estuvimos enfocando en arreglar algunos problemas de larga data. Nuestra instancia de Nextcloud parecía estar consumiendo más recursos de los que debía. Después de semanas de batallar y buscar la causa principal en nuestra configuración de Nextcloud, finalmente hemos podido encontrarla y arreglarla. El problema estaba relacionado con lo que parece ser un bug en la aplicación Marcadores que generaba la creación de millones de archivos de caché, provocando un enorme esfuerzo para nuestro CPU. Luego de simplemente quitar todos los archivos de caché de marcadores, el problema se detuvo y Nextcloud no ha vuelto a producir nuevos cachés desde entonces. A su turno, nuestra carga de CPU ha disminuido 5 veces, lo cual es una gran noticia. Nextcloud está funcionando muy rápido de nuevo y nuestros niveles de estrés han vuelto al punto donde podemos realmente enfocarnos en otras cosas.

## Hubzilla todavía no está feliz
Desafortunadamente, no podemos decir lo mismo de nuestra instancia de Hubzilla. Como algunos de ustedes saben, durante el último año, más o menos, hemos estado experimentando con una nueva plataforma de servicios federados llamada Hubzilla. Nuestro objetivo es llevarla a un nivel tal que podamos brindarla a una audiencia más amplia de Disrooters. Sin embargo, en las últimas semanas estamos luchando con la estabilidad de esta plataforma. Parece que hemos identificado la causa principal, pero todavía no conseguimos arreglarla. El trabajo relativo a esto continuará a lo largo de los próximos sprints. Queremos poner más de nuestro tiempo y energía en este proyecto porque pensamos que Hubzilla es LA experiencia en redes sociales y nos encantaría promoverla.

## Página de XMPP y campamento de bots (botcamp)
Antilopa trabajó en nuevos diseños para la página de XMPP que ahora incluye más información acerca de nuestro querido protocolo de chat, nuestra configuración particular y un listado de clientes XMPP. Los cambios se ven grandiosos y los estaremos implementando al resto de las páginas en los próximos sprints.
**[[https://disroot.org/en/services/xmpp](https://disroot.org/en/services/xmpp)]**

<br>
También hemos comenzado a implementar bots de XMPP. Los bots parecen usuarios o usuarias comunes en el chat, pero son en realidad programas. Pueden hacer un montón de cosas útiles, como recordar y acordarse cosas por ti, llevar el registro de tus reuniones o decirte el pronóstico del tiempo, y muchas otras completamente inútiles como enviarte fotos de gatitos, generar memes e incluso insultarte. Todos los bots pasan el rato en: ** botcamp@chat.disroot.org**,  donde puedes probarlos. En este momento, no los puedes invitar a tu sala así sin más, necesitas solicitarlo a los administradores (la mejor vía es la sala ** disroot@chat.disroot.org**), pero esperamos encontrar pronto una solución para eso también, así podrás invitar bots a tu sala sin intervención de los admins. Como mencionamos en la publicación anterior, si estás buscando un hogar para tu bot, estamos corriendo una instancia especial de XMPP dedicada a hospedar bots.

## Un mejorado Equipo de Disroot
Estamos muy orgullosos y extremadamente felices de anunciar que nuestro equipo ha crecido. Desde el comienzo, hace casi 4 años atrás, el equipo de Disroot consistió en dos personas: Muppeth y Antilopa. Durante los últimos dos años, un grupo de Disrooters fanáticamente dedicados nos estuvo ayudando de toda clase de maneras. Empezando con las traducciones, escribiendo manuales, creando Hubzilla y aplicaciones de Disroot, ayudando a testear características y depurando errores y mucho, mucho más. Sentimos que eran parte de la familia y se sintió natural que oficialmente se unieran al equipo recibiendo toda la fama y la gloria, pero también compartiendo la carga de trabajo y la responsabilidad por el funcionamiento y bienestar de la plataforma. Estamos muy agradecidos por todo el trabajo que han hecho hasta ahora. Nos gustaría presentarles a los nuevos miembros del Equipo Disroot: Fede, Maryjane, Meaz y Massimiliano. Para todos nosotros es una nueva experiencia y esto definitivamente abre un nuevo capítulo para el proyecto. En las próximas semanas, nos enfocaremos en encontrar la mejor manera de coordinar y planificar el trabajo, compartir conocimientos y tareas, desarrollar un proceso de toma de decisiones óptimo y toneladas de otros procedimientos.

## Declaración de Objetivos (Misión)
Sip, finalmente le encontramos la vuelta. Desde el momento en que oficialmente registramos nuestra organización sin fines de lucro, quisimos escribir una declaración de objetivos definiendo nuestras ideas. Después de dos años, al fin hemos creado el documento. Esta declaración de objetivos define nuestras ideas para el Proyecto Disroot.org, sus principios, nuestra visión sobre el financiamiento, los procesos de toma de decisión y nuestras ambiciones para el futuro. Puedes leerla [aquí](https://disroot.org/es/mission-statement)
<br><br><br>
Como pueden ver, hay bastantes cosas pasando. Con nuevos miembros y energía en extremo alta, nos esforzaremos por brindar la mejor experiencia siempre, mientras nos divertimos mucho haciéndolo.
<br><br>
Si no lo has hecho todavía, por favor, piensa en donar. La contribución mensual de “un café de tu elección" para tu querida plataforma de servicios web es verdaderamente necesaria porque elegimos NO financiarla haciendo negocios con los datos de las personas. Es también importante recordar que si todos los usuarios y las usuarias pudieran donar el equivalente a una taza de café al mes, eso no solo nos ayudaría a mantener la plataforma funcionando y a mejorar los servicios para todos (para las personas que pueden colaborar económicamente y para aquellas que no), también nos permitiría ampliar el alcance del proyecto. Por favor, revisa nuestra [página de donaciones](https://disroot.org/en/donate) para más detalles.
<br><br>

Y hablando de ampliar el alcance, si mantienes una comunidad, un proyecto interesante o algo que te gustaría compartir con otros Disrooters, envíanos un correo a ** discover @ disroot.org** y te daremos reconocimiento en nuestras publicaciones del blog.
