---
title: 'DisNews #10 - Migración a los nuevos servidores completada; Lacre, pruebas en el horizonte'
date: '13-06-2023'
media_order: White-blue-white.png
taxonomy:
  category: novedades
  tag: [disroot, novedades, servidores, lacre, sequoia]
body_classes: 'single single-post'

---
Hola, Disrooters.

El tiempo vuela... 
Les damos la bienvenida a la décima entrega de DisNews. La mitad del año ha quedado detrás nuestro y hasta ahora no nos han invadido los aliens. Aunque el mundo se está volviendo más y más loco cada mes. Esperamos que estén bien, cuidándose y cuidando de sus personas más cercanas.

Primero que todo, algo que nos olvidamos de mencionar en la última DisNews: ¡el reporte anual de 2022 ha sido publicado! Pueden verlo [**aquí**](https://disroot.org/annual_reports/AnnualReport2022.pdf).

Como algunas personas notaron, no hemos publicado el reporte de 2021. Claro, podríamos culpar a la COVID, el impacto que tuvo en nuestras vidas personales, lo traumático que fue en general este período y lo difícil que fue recordar el reporte anual. ¿A quién no le gustaría olvidar que este año siquiera existió? Sin embargo, la verdad es más simple que eso: un perro se comió el borrador de nuestro reporte.

Al margen de eso, pueden revisar el reporte de 2022 y disfrutar.

## Nuevos servidores

Como pudieron leer en una publicación anterior (DisNews #5), compramos nuevo hardware. Desde principios de este año hemos estado ocupados configurando los servidores, instalándolos en el centro de datos y migrando todos los servicios. Este fue un proceso que demandó tiempo pero está casi completo. Todos los servidores dedicados están colgando en los bastidores y encendidos. Todos los servidores virtuales que hospedan todos los servicios han sido migrados a las nuevas bestias. Lo único que queda por hacer es la migración de los contenedores más grandes de datos que son los buzones de correo y los archivos de Nextcloud. Como se trata de conjuntos de datos inmensos (no solo por su tamaño sino sobre todo por la cantidad de archivos), llevará algún tiempo completar su migración. Queremos asegurarnos de que no se produzca ninguna pérdida de datos durante esta operación. Otra cosa importante es hacerlo con un tiempo de interrupción mínimo o, en la medida de lo posible, imperceptible para ustedes, Disrooters. Con suerte, todos los datos se moverán sin que lo noten. Dicho esto, **si guardan algún archivo importante en Nextcloud, durante este periodo asegúrense de tener a mano una copia de seguridad adicional**.

Con el nuevo hardware a nuestra disposición, ahora podemos concentrarnos en mejorar el rendimiento, que es de lo que nos ocuparemos en los próximos meses.

## Mejoras en la implementación

A medida que la plataforma crece, también lo hace la responsabilidad de prestarles servicios sin problemas causados por la puesta en marcha de nuevas funciones o actualizaciones de código que no se hayan probado. El lema *"Probamos en producción"* es algo que ya no aceptamos, por lo que estamos trabajando en flujos de trabajo que minimicen los posibles problemas. Una de las cosas que ya introdujimos el año pasado son las ventanas de mantenimiento. Cada dos martes lanzamos actualizaciones en las que trabajamos previamente. Esto nos ha proporcionado un flujo de trabajo que marca el pulso de nuestra actividad en Disroot. Además nos permite planificar mejor las prioridades y la rutina de mantenimiento. A ustedes, Disrooters, les permite saber qué se está actualizando y comprobar las mejoras anteriores a través de la página del [registro de cambios](https://disroot.org/changelog). Para mejorar aún más las pruebas previas a la implementación, hemos abandonado los entornos de máquinas virtuales en nuestras computadoras locales y, en su lugar, hemos invertido en pequeñas máquinas de escritorio físicas alojadas localmente en nuestras casas que ejecutan una configuración muy similar a la de los servidores de Disroot. Tras el trabajo inicial de configuración, actualización de la documentación (algo que también nos estamos tomando más en serio ahora) y transferencia de conocimientos, podemos preparar los cambios en nuestros equipos locales y probarlos en los de los demás antes de que la actualización llegue a los servidores de producción de Disroot.

Aunque falta todavía, nos acerca un poco más a la situación en la que podremos ofrecer a cualquiera la posibilidad de levantar su propio nodo al estilo Disroot. Después de todo, ya estamos corriendo tres nodos independientes.

## Otros cambios destacables

- Trajimos de vuelta el tablero de aplicaciones en [https://search.disroot.org](https://search.disroot.org) / [https://apps.disroot.org](https://apps.disroot.org) que estaba ausente desde que cambiamos de **Searx** a su bifurcación que se llama **Searxng**.

- Hemos mejorado la paleta de colores para nuestros propios temas (especialmente el oscuro) en los que también estaremos trabajando para implementarlos en todos los servicios y brindar una experiencia más unificada en toda la plataforma.

- Lacre: **@pfm** pudo arreglar el problema que inicialmente nos impidió realizar pruebas alfa en Disroot. Fue toda una batalla. El dragón fue abatido abriendo las puertas a las pruebas en Disroot nuevamente. Pronto anunciaremos la próxima prueba de cifrado de extremo-a-extremo de los buzones de correo de Disroot.

- ¡Ah! Una más sobre Lacre. **@wiktor** uno de los desarrolladores destacados de **Sequoia** ha impulsado el soporte inicial para Lacre. Sequoia es una re-implementación más segura y robusta de PGP. ¡Estamos muy felices y agradecidos con **@wiktor** por su trabajo!
