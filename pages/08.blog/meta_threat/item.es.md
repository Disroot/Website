---
title: 'Nuestra posición sobre el nuevo "ThreaTs" de Meta'
date: '07-07-2023'
media_order: threat.png
taxonomy:
  category: novedades
  tag: [disroot, novedades, meta, threads]
body_classes: 'single single-post'

---

Meta (la empresa antes conocida como Facebook y propietaria de Instagram, WhatsApp y la "red social" Facebook) acaba de lanzar una nueva plataforma llamada Threads que pretende "matar" a Twitter. Toda guerra  (real o virtual) trae bajas. Y en esta que están librando las grandes corporaciones por el control de las comunicaciones, pensamos que el Fediverso, y especialmente Mastodon, estarán entre esas bajas.

Cuando Meta anunció su lanzamiento, también declaró que la nueva plataforma sería compatible con el protocolo ActivityPub, el mismo protocolo que motoriza todo el Fediverso. Esto significa que Threads será interoperable con otros servidores de la red, incluido el nuestro. Para algunes parece un sueño hecho realidad: por fin todas las personas, independientemente de dónde "vivan" sus cuentas, podrán comunicarse entre sí. Suena muy bien sobre el papel, sí...

Sin embargo, si tenemos presente lo que nos ha enseñado la historia, parece que estamos repitiendo el cuento de las ovejas invitando al lobo a cenar.

Meta es una de esas corporaciones que tienen una reputación terrible y muy conocida. Y es muy probable que la mayoría de ustedes estén aquí porque buscaban escapar de las "plataformas" de las "grandes corporaciones tecnológicas" como esta. Bueno, adivinen qué... Como el lobo del cuento, las grandes corporaciones están poniendo un pie en nuestra puerta de nuevo.

Pensamos que el ingreso de Meta al Fediverso tendrá consecuencias muy negativas a largo plazo. Threads se convertirá desde el primer día en el nodo más grande e influyente de la red federada. De hecho, será más grande que todos los servidores actuales juntos. Y al menos para nosotros, esto significa que pueden tomar la iniciativa en el desarrollo de nuevas funcionalidades y forzar a otros participantes a implementarlas (para bien o para mal). Ya sean características que esperamos desde hace tiempo (como la migración de cuentas), otras que perjudican la privacidad de las personas usuarias o la introducción de espacio para publicidad, que otros servidores tarde o temprano tendrán que implementar para seguir siendo compatibles con el resto de la red. El resto de la red, en este escenario, sería ese otro servidor único con miles de millones de usuaries.

Por supuesto, en este momento solo podemos especular sobre los planes que Meta tiene para nosotres y cómo se desarrollará todo, pero dado su historial, no podemos esperar que venga de buena fe. El hecho de que ActivityPub sea un protocolo abierto no impide que Meta lo utilice, pero como red de servidores dentro del Fediverso no tenemos por qué comprometernos con ellos y permitirles la entrada a nuestros espacios.

Por eso hemos decidido que no queremos cooperar con Meta a ningún nivel y por lo tanto no federar con ellos. No federar con Threads es, probable y sencillamente, la única forma que tiene una instancia u organización con los valores que defendemos de expresar su posición y decir que no están de acuerdo y que no quieren tener nada que ver con lo que Meta hace y cómo lo hace a través de sus productos: promoviendo e induciendo la generación de interacciones y hábitos condicionados para la producción y extracción de datos.

El único valor agregado que Threads está aportando son sus usuaries. Nos encantaría ver a toda la población de la Tierra utilizando ActivityPub, porque resuelve el problema de la centralización, el monopolio y brinda la posibilidad a las pequeñas organizaciones, grupos, familias o particulares, de autoalojar su plataforma social y tener el control sobre sus datos.

Sin embargo, pensamos que este cambio tiene que producirse de forma orgánica, a medida que la gente entienda los conceptos de federación y descentralización, los considere una parte importante de su vida digital y construya una red equilibrada y propia, libre de influencias de las grandes corporaciones tecnológicas. Hasta entonces, los miles de millones de usuaries proporcionados por Meta destruirán ese concepto.

Queremos que ustedes estén en el Fediverso, ¡pero no en los términos dictados por Meta, Alphabet u otros!

¿Qué significa esto para ustedes, estimadas y estimados Disrooters?

Que cuando Threads entre en el Fediverso, no podrán interactuar con sus usuaries. Nuestro servidor no federará con Meta. Si esta decisión representa un problema para ustedes, gracias a la naturaleza (todavía) diversa de la red, quizás puedan encontrar un nuevo hogar en un servidor que haya decidido dar a Meta el beneficio de la duda.

Y aunque hemos decidido no federar con el nuevo producto de Meta, respetamos plenamente la autonomía y las decisiones de otros nodos que decidan federar. Nuestras "armas" apuntan y siempre han apuntado a los señores de la guerra de los gigantes tecnológicos. No las estamos apuntando a otros pares del Fediverso. No vamos a bloquear instancias que decidan federar con Meta. Tienen derecho a hacerlo, y aunque pensamos que están cometiendo un gran error, apoyamos plenamente su decisión.

Al mismo tiempo, esperamos, deseamos y necesitamos que no nos volvamos unos nodos contra los otros dentro de la red que tanto nos costó y nos cuesta sostener. Si hay algo que Meta y demás hacen muy bien es promover la fragmentación de la red en bandos que luchen entre sí antes antes incluso de que Threads se convierta en una verdadera **AMENAZA**. Así que apoyen a sus administradores, apoyen a sus usuaries, aprecien la diversidad de la red y hagan uso de ella.

No sabemos qué nos deparará el futuro, pero una cosa sí sabemos. Este movimiento de Meta demuestra que las redes federadas son el futuro y las corporaciones lo ven como una amenaza para su maquinaria de hacer dinero. No vienen en son de paz, sino para quedarse con la torta antes de que otras lo hagan y tomar el control para mantener su monopolio y sus beneficios. Debemos mantener nuestros espacios y seguir haciendo un trabajo increíble sin ellas.

No las necesitamos. Nos ha ido muy bien hasta ahora.

**¡Mantengamos alejados a los lobos!**

