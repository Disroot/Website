---
title: 'Kit Covid19'
media_order: antivir_mask.jpg
published: true
date: '31-03-2020 22:30'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - noticias
        - covid19
body_classes: 'single single-post'

---

Bien, parece que el mundo se volvió completamente loco estas últimas semanas. Nuestras ciudades parecen pueblos fantasmas y todxs estamos tratando de evitar el contacto con otrxs para evitar la propagación del virus. Mientras todo este caos sucede, desafortunadamente empezamos a ceder nuestras libertades en línea a los gigantes tecnológicos. Nuestrxs hijxs ahora son obligadxs a usar Google Classroom, y nosotrxs a utilizar herramientas como MS-Teams, Zooms, Slacks, Discord y demás. Pareciera que cuando hay miedo y pánico renunciamos a nuestras vidas virtuales. Lo peor es que lo mismo puede pasar pronto en nuestra "vida real". Necesitamos mantener las cosas bajo control. Estamos lanzando el Kit Covid19 para ayudar y proveer servicios a aquellxs que buscan alternativas y no quieren quedar atrapadxs en jardines vallados. Decidimos agregar algunos servicios adicionales que no requieren crear una cuenta. Con suerte, continuaremos mejorando los nuevos servicios experimentales (Mumble y CryptPad) y también sumando nuevos sobre la marcha, basándonos en sus comentarios, sugerencias y solicitudes. Pueden encontrar la **página del Kit Covid19** aquí: [https://disroot.org/covid19](https://disroot.org/covid19). Por supuesto, los registros y los demás servicios continúan funcionando como de costumbre.

Además, estamos comenzando un proyecto experimental con el que queremos ayudar a configurar y mantener plataformas personalizadas, individuales, para organizaciones y pequeños negocios que están buscando un mejor proveedor de servicios, más ético, transparente y basado en software libre y de código abierto. Todavía lo estamos armando, así que si están interesadxs, por favor, envíennos un [correo](mailto:support@disroot.org).


Mientras tratamos de hacer lo mejor para ayudar con todas sus necesidades, no debemos olvidar que no somos lxs únicxs por aquí. La internet debería permanecer descentralizada y distribuida así la base de usuarixs se extiende más equitativamente a través de la red. Recuerden, nosotrxs solo somos un pez en el océano. Echen un vistazo a [https://libreho.st](https://libreho.st) o [https://chatons.org](https://chatons.org) y encuentren un proveedor que se ajuste mejor a sus necesidades.
