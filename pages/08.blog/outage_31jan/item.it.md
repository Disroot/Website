---
title: 'Grave interruzione - Mail, Nextcloud e XMPP'
date: '31-01-2022'
media_order: sorry.jpg
taxonomy:
  category: news
  tag: [disroot, news, maintenance, downtime, issues]
body_classes: 'single single-post'

---

Ieri, 30 gennaio, abbiamo eseguito dei lavori di manutenzione al datacenter. L'attività principale è stata la sostituzione di una batteria della cache del controller raid difettosa che si era rotta pochi giorni prima. Sebbene la procedura sia andata come previsto, durante il processo di riavvio del server, necessario per eseguire la sostituzione delle parti, il file system si è danneggiato. Per riportare il server online abbiamo naturalmente dovuto riparare il filesystem. Questa operazione ha richiesto più di 12 ore, durante le quali i servizi di e-mail, chat XMPP e Nextcloud sono risultati inaccessibili. Durante questo periodo si è pure verificata una perdita di dati. Significa che potresti aver perso un file o un'e-mail che hai caricato poco prima del nostro riavvio del server.

Ci scusiamo per il disservizio e per il futuro cercheremo di migliorare la comunicazione durante i periodi di manutenzione. Speriamo di ridurre al minimo interruzioni così lunghe grazie al nuovo hardware che ci stiamo accingendo ad acquistare. Ciò ci consentirà di ricostruire l'attuale infrastruttura aggiungendo un po' di ridondanza (per quanto possiamo permetterci ovviamente).

Sebbene ora siamo di nuovo operativi, vediamo ancora alcuni _inode_ corrotti nella partizione dati in cui archiviamo e-mail e file cloud. Ciò significa che dovremo eseguire altri controlli su quella partizione del disco. Dovremo quindi disattivare nuovamente il servizio per alcune ore. Questi lavori di manutenzione sono previsti per sabato 5 febbraio 2022 a partire dalle 0:00 CET. Pensiamo che non ci vorrà molto tempo, ma è saggio essere pronti per qualche ora di fermo macchina. Il sabato sera (fuso orario europeo) sembra essere il momento con il traffico sul server più basso, quindi speriamo di avere un impatto sul minor numero di persone possibile. 

**Ci scusiamo nuovamente per l'interruzione e vi ringraziamo per la comprensione e il supporto.**
