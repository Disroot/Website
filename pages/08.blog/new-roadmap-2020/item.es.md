---
title: 'Nueva Hoja de Ruta 2020'
media_order: road.jpg
published: true
date: '19-01-2020 18:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - noticias
        - hojaderuta
body_classes: 'single single-post'
---


¡Hola a todxs lxs Disrooters! ¿Están todxs bien, descansadxs y preparadxs para el nuevo año? Nosotrxs lo estamos, y no podemos esperar por lo que viene en nuestro camino. Queremos comenzar el año planificando y decidiendo en qué vamos a estar ocupados en los próximos meses.

## El Plan 2020

Esta vez tenemos algunos planes ambiciosos para este año, como siempre. Hemos identificado algunos temas en los que queremos enfocarnos.

1.  **Mejoras en el Correo**

    Queremos concentrarnos en una cantidad de mejoras en nuestro servicio de Correo. Comenzando por una nueva y mejor solución de webmail, mayor promoción de clientes nativos de Correo (aplicaciones) y enfocarnos en el soporte completo de **Autocrypt** en el webmail como campaña para su difusión (Autocrypt es cifrado de correo de extremo-a-extremo sencillo e independiente del lado de usuarix/aplicación) no solo para lxs usuarixs sino también para lxs desarrolladorxs de clientes de correo, para promover una más amplia adopción de la solución. A medida que nos estamos haciendo más "populares", estamos siendo atacados con más frecuencia por SPAM, phishing y otras cosas desagradables que arruinan la experiencia del correo. Queremos poner el foco en mejores soluciones anti-spam/virus también.

2.  **Cifrado del Buzón**

    Una de las cosas que no hemos podido lograr el año pasado es el cifrado del buzón. Este año esperamos finalmente poder conseguirlo. Inicialmente pensamos en cifrado del lado del servidor (donde sus claves privadas son almacenadas), sin embargo, recientemente hemos empezado a conversar más y más sobre la posibilidad de utilizar **GnuPG** (GPG/PGP) como una forma de cifrar de extremo-a-extremo los correos almacenados (cifrado de buzón). Esperamos dar con una solución que no solo servirá a lxs Disrooters, ya que sería totalmente abierta y permitiría que cualquier proveedor de servidores de correo electrónico lo utilizara. No más silos cerrados. Si lo lograremos, el tiempo dirá. Como plan b, trabajaremos en el cifrado de los buzones de correo del lado del servidor.

3.  **Mejor administración**

    Uno de los temas de la última Hoja de Ruta para el 4º trimestre de 2019 fue que empezamos a buscar maneras de mejorar la administración y la forma de gestionar los tickets de soporte, las solicitudes personalizadas y todo tipo de tareas rutinarias que consumen cada vez más tiempo. También nos damos cuenta que algunas de las tareas a las que ustedes se enfrentan (almacenamiento adicional, dominios personalizados, etc.) no se sienten muy profesionales y definitivamente toman demasiado tiempo en ser procesadas. Queremos abordar todas esas cosas y mejorar tanto como podamos, para que sus peticiones sean procesadas de forma rápida y profesional, y para que nosotros no sintamos que todas las tareas rutinarias diarias son un tedio y tengamos más tiempo para concentrarnos en cosa más entretenidas como la mejora de los servicios que corremos.

4. **Incorporar más administradores**

    Durante los últimos años, **Disroot** fue dirigido por **Muppeth** y **Antilopa**, quienes se encargaban de todo lo relacionado con la plataforma (desde el sitio web, las tareas diarias, la administración, el soporte, la administración del sistema, el mantenimiento y todo lo demás). Desde hace unos meses el equipo de **Disroot** creció con fuerza adicional (**Fede** y **Meaz** en el equipo central y **Massimiliano** y **Maryjane** como voluntarios que ayudan de cerca). Ya en la última Hoja de Ruta hemos empezado a definir y redistribuir algunas de las tareas como los tickets de soporte, la gestión del proyecto de traducciones y guías, la construcción del sitio web y demás. Queremos continuar este año con la formación y la incorporación de nuevos miembros del equipo central para permitirles el acceso a más servidores de producción, para ayudar con la solución de problemas, el mantenimiento y otras tareas y solicitudes que requieren el acceso a los servidores.

5. **Lanzar Hubzilla**

    Algunos de ustedes saben que nos hemos enamorado de **Hubzilla** hace ya dos años. Pensamos que es una brillante pieza de software adelantada a su tiempo, con un gran potencial, que ofrece funcionalidades que ninguna otra red proporciona. En el momento en que descubrimos la genialidad de **Hubzilla**, quisimos que se convirtiera en nuestra red social número uno. **Hubzilla** necesita mucho amor, y mucho trabajo para convertirse en un servicio que podamos proporcionar como la red social de **Disroot**. Después de dos años no hemos logrado que nuestra instancia de prueba llegue al punto en que estemos satisfechos con ella. Conseguir que **Hubzilla** alcance el estado deseado, como meta y prioridad, ayudará a que podamos proveerla a todxs lxs Disrooters.

6. **Financiamiento**

    Decir que **Disroot** no se ha convertido en nuestro trabajo diario sería una mentira. Pasamos más tiempo con **Disroot** que con nuestros trabajos cotidianos. Y como necesitamos mantener ambos, sin mencionar nuestras familias, otros proyectos, hobbies y tal vez algo de vida social (que en este momento no existe), necesitamos cambiar esta situación. Llegar al punto en que **Disroot** se convierta en nuestra principal ocupación parece estar todavía muy lejos, pero tenemos que empezar a trabajar seriamente para conseguirlo. El año pasado ha superado nuestras expectativas más entusiastas y nos ha mostrado que el sueño de la independencia financiera es realista. Por eso queremos dedicar un tiempo a explorar las formas de hacer este sueño realidad.


## ¿Qué sigue?

Hemos dividido nuestros planes en Hojas de Ruta de tres meses de duración. En este momento, estamos ocupados trabajando en la preparación de todos los temas y seguimos trabajando en las tareas que no hemos logrado realizar durante los últimos meses. Además, estamos refinando todos los planes con más detalle para mejorar nuestra experiencia de trabajo.
