---
title: 'DisNews #5 - New servers; Moving Spam mails; New Prices; Lacre; Chat Control'
date: '16-06-2022'
media_order: graffitti.jpg
taxonomy:
  category: news
  tag: [disroot, news, servers, spam, prices, lacre, chatcontrol]
body_classes: 'single single-post'

---

Hi there Disrooters!

As usual, time flies! We've been quite busy these past months, especially in personal lifes. But we've also managed to have some nice new things around. Let's see them!


# New servers

Thanks to all the donations we've received over the past years, we're pleased to tell you that we were able to buy three new servers! These servers will replace our current ones. And since the process of deploying them in the rack is going to take a while, we want to use this moment to totally re-build and improve our infrastructure based on our last 7 years experience of running Disroot. Once the servers are all up and running we will all enjoy a greater performance. New hardware should secure Disroot's needs for years to come. We can't wait to start using them and we really hope you will enjoy them as much as we will.

**So many, many thanks to everyone who were supporting us financially throughout the years. You are the heros carring this project forward.**

Obviously, our cash reserve have dropped considerably. So we would be really happy if you could participe in helping raise our funds again. Please, don't forget the price of a simple cup of coffee can make a difference. Don't wait for that money from a Nigerian prince to finally cash out and donate now!

Please, visit our [donation](https://disroot.org/donate) page to see our current financial overview and join the group of Disrooters that are our regular caffeine donors!

Your donations will allow the Disroot project to stay sustainable, independent and be prepare in case something unforseen would happen.


# Moving Spam mails

As you probably know, fighting SPAM is a tough and never ending battle that seems like it can't be won. Everytime you think you have it under control, spammers find new ways to keep you busy. Recently, we have changed the way our system works. One of these changes was to add a "[SPAM Warning]" label in the subject of emails that are found to be suspicious by our anti-spam system. Until now we haven't done any additional actions on such emails. However as we loosen up some of the settings that were straight rejecting emails instead of labeling them, the amount of spam has increased as you may have noticed. Many of you suggested we should automatically move emails marked as spam to the "Junk" folder. For long time we have been opposing that idea because we think that users should be able to decide what to do with their spam. But the recently important increase of incoming spam, due to settings changes and generally more intense activity of spammers as we get more "popular", made us change our aproach. Especially for those users with less experience or time to setup a filter and are currently "swimming" in spam.

So starting from today, we've decided that **all emails marked as spam will be automatically moved to the "Junk" folder**. So if you can't find some emails you've been looking or waiting for, please check that folder.


# New prices

Now that is a big change. We have decided to update our pricing system . We did it for two reasons: transparency and simplicity.

**Our prices now include the transaction fees** which will allow Disrooters to know right away how much they will have to donate, instead of discovering additional fees when they get the invoice. Additionally we don't have to calculate what the fees are for each invoice and payment method anymore (_especially as the payment platforms sometimes change them_). This will also give Disrooters better flexibility in changing the payment method, because we won't have to create a new invoice that way.

From now on, **payment will be annually only**. It will not only allow us to spend less time on recreating invoices each month, but will also be less costly regarding transaction fees for Disrooters.

Now what is most important for you, these are our **new prices** to be paid annually:

|    |     |    |
|----|:---:|----|
|11€ |➡️|5GB |
|20€ |➡️|10GB| 
|29€ |➡️|15GB|
|56€ |➡️|30GB|
|83€ |➡️|45GB|
|110€|➡️|60GB|

**It is important to note that for residents of countries in the European Union we have to add VAT** (Value Added Tax) **on top of these prices, which is 21%**.

Lastly, we also have decided to make it possible for users to **allocate this extra storage as they wish** between cloud and mail. For example, if a Disrooter donates for 10GB storage, this user could decide to have 8GB for cloud and 2GB for mail. These extra gigabytes will be added on top of the default storage space given on account sign-up.


# Lacre

There is some great development happening on **Lacre, our end-to-end encrypted mailbox project**. **Pfm** has been very busy and involved with it and we have reached some important milestones. He has finalized updating backend codebase to _python3_ and introduced bunch of _unit tests_ which allow for easier programming and better code quality. We have finalized the procedure for server deployment which allowed us to commit first proper field tests. Things are actually moving forward, and we can't wait for Lacre to hit Disroot server, however this will still take some time. We want to be sure the system is robust, optimized and most of bugs and issues are ironed out before.

Our special thanks goes to **NLNET** for their financial support on this project.

https://lacre.io/


# Chat control

Last, but not least, we wanted to add a few words about some worrying changes in the European Union legislation that will affect everyone in the world.

Indeed, the European Commission is working on a legislation called **Chat control**, which would compel online platforms to scan content such as text, images and videos created by users in order to detect child sexual abuse related material. This will effectively create a dangerous precedent where every message, image or video shared on the internet will be subject to surveillance and censorship. It opens gates to governments and dictatorships of present and future to track, eliminate and silence opposition of any sort (child abuse is just a wrapping paper). It gives those in control of AI scanning software ultimate power and control over population knowing who says what to whom and when. Above all, it totally undermines people's freedoms and our right to privacy. This new law if translated to real world means essentially that every private conversation you have with anyone, should be recorded and send to authorities as preventive measure against crime and not complying with such regulation would be punishable.

This is totally unacceptable in 21st century Europe and should be pushed back!

Governments are trying very hard to end private messaging and the freedom the internet gives us in all kind of ways. They use different wrapping paper around the censorship attempts, trying to sell it to the public as a preventive and protective measures, yet in many cases they fail to do so. And although we, the people, have pushed back, it looks like sooner or later those scumbags will have it their way.

Unfortunatelly it looks like this is the moment.

If you value your privacy do whatever you can to help stop this madness. Contact your European deputies and commissioners, organize demonstrations, make your voice heard! If we loose our right to private communication we will shortly after loose all the little freedoms we have left! Act now because it might be already too late!

If you want to understand better and get some arguments to oppose, check this analysis by **Patrick Breyer**, a former judge and current member of European parliament (https://www.patrick-breyer.de/en/posts/messaging-and-chat-control/)



The Disroot Team