---
title: 'DisNews #5 - Nuevos servidores; Moviendo el Spam; Nuevos precios; Lacre; Control de Chat'
date: '16-06-2022'
media_order: graffitti.jpg
taxonomy:
  category: news
  tag: [disroot, news, servidores, spam, precios, lacre, chatcontrol]
body_classes: 'single single-post'

---

¡Hola, Disrooters!

Como siempre, ¡el tiempo vuela! Hemos estado bastante ocupados estos últimos meses, sobre todo en nuestras vidas personales. Pero también nos las hemos arreglado para tener algunas cosas nuevas muy buenas. ¡Veámoslas!


# Nuevos servidores

Gracias a todas las donaciones que hemos recibido durante los últimos años, estamos muy contentos de contarles que ¡hemos podido comprar tres nuevos servidores!. Estos servidores sustituirán a los actuales. Y ya que el proceso de instalarlos en el rack va a llevar algún tiempo, queremos aprovechar este momento para reconstruir y mejorar totalmente nuestra infraestructura basándonos en nuestra experiencia de los últimos 7 años manteniendo Disroot. Una vez que los servidores estén en funcionamiento, todxs disfrutaremos de un rendimiento mucho mejor. El nuevo hardware debería cubrir las necesidades de Disroot durante los próximos años. Estamos ansiosos por empezar a usarlos y esperamos en serio que los disfruten tanto como lo haremos nosotros.

**Así que muchas, muchas gracias a todas las personas que nos han apoyado económicamente a lo largo de estos años. Ustedes son los héroes y heroínas que sacan adelante este proyecto.**

Obviamente, nuestras reservas de efectivo han disminuido de manera considerable. Así que seríamos muy felices si pudieran participar ayudando a la recuperación de nuestros fondos de nuevo. Por favor, no olviden que el precio de una simple taza de café puede hacer la diferencia. No esperen a que por fin se haga efectiva la transferencia de dinero de algún príncipe nigeriano y ¡donen ahora!

Por favor, visita nuestra página de [donaciones](https://disroot.org/es/donate) para ver el resumen financiero actual y ¡únete al grupo de Disrooters que son nuestros donantes regulares de cefeína!

Sus donaciones permitirán al proyecto Disroot seguir siendo sostenible, independiente y estar preparado en caso de algún imponderable.


# Moviendo el Spam

Como probablemente saben, luchar contra el SPAM es una batalla dura e interminable que parece que no se puede ganar. Cada vez que piensas que lo tienes controlado, los spammers encuentran nuevas formas de mantenerte ocupadx.

Recientemente, hemos cambiado el funcionamiento de nuestro sistema. Uno de estos cambios ha sido añadir una etiqueta "[SPAM Warning]" (Aviso de SPAM) en el asunto de los correos electrónicos que nuestro sistema antispam considera sospechoso. Hasta ahora no hemos realizado ninguna acción adicional sobre dichos correos. Sin embargo, al "relajar" algunos de los ajustes que rechazaban directamente este tipo de correos y en vez de eso etiquetarlos, la cantidad de spam ha aumentado como ya habrán podido notar. Muchxs de ustedes han sugerido que movamos automáticamente los correos marcados como spam a la carpeta de "Correo basura (Junk)". Durante mucho tiempo nos hemos estado oponiendo a esa idea porque pensamos que lxs usuarixs deben poder decidir qué hacer con su spam. Pero el reciente e importante aumento del spam entrante, producto de los cambios en la configuración y a una actividad más intensa de los spammers a medida que nos hacemos más "conocidos", nos ha hecho cambiar nuestro enfoque. Pensando sobre todo en aquellxs usuarixs con menos experiencia o tiempo para configurar un filtro y que actualmente están "nadando" en spam.

Por lo que decidimos que a partir de hoy **todos los correos marcados como spam se moverán automáticamente a la carpeta de "Correo basura"**. Así que, si no pueden encontrar algún correo que estaban buscando o esperando, por favor, revisen esa carpeta.


# Nuevos precios

Este sí es un gran cambio. Hemos decidido actualizar nuestro sistema de precios y lo hicimos por dos razones: transparencia y simplicidad.

**Nuestros precios incluyen ahora las tasas de transacción** lo que permitirá a lxs Disrooters saber de inmediato cuánto tendrán que donar, en lugar de descubrir gastos adicionales cuando reciben la factura. Además, ya no tenemos que calcular cuáles son las tasas para cada factura y método de pago (_especialmente porque las plataformas de pago a veces las cambian_). Esto también otorgará a lxs Disrooters una mejor flexibilidad para cambiar el método de pago, porque de esa manera no tendremos que crear una nueva factura.

A partir de ahora, **el pago solo será anual**. Esto nos permitirá no solo pasar menos tiempo recreando facturas cada mes, sino que también será menos costoso para lxs Disrooters respecto a las tasas de transacción.

Ahora lo más importante para ustedes, estos son nuestros **nuevos precios** a pagar anualmente:

|    |     |    |
|----|:---:|----|
|11€ |➡️|5GB |
|20€ |➡️|10GB| 
|29€ |➡️|15GB|
|56€ |➡️|30GB|
|83€ |➡️|45GB|
|110€|➡️|60GB|

**Es importante notar que para los residentes en países de la Unión Europea tenemos que añadir el IVA** (Impuesto al Valor Agregado) **sobre estos precios, que es del 21%**.

Por último, tambien hemos decidido que lxs usuarixs puedan **asignar este almacenamiento extra como deseen** entre la nube y el correo. Por ejemplo, si una Disrooter paga por 10GB de almacenamiento, esta usuaria podría decidir tener 8GB para la nube, y 2GB para el correo. Estos gigabytes extra se añadirán al espacio de almacenamiento por defecto que se da al registrar la cuenta.

# Lacre

Hay grandes avances en el desarrollo de **Lacre, nuestro proyecto de buzón cifrado de extremo a extremo**.  **Pfm** ha estado muy ocupado e involucrado con ello y hemos alcanzado algunos hitos importantes. Ha finalizado la actualización del código base del [backend](https://es.wikipedia.org/wiki/Front_end_y_back_end) a [_python3_](https://es.wikipedia.org/wiki/Python) y ha introducido un montón de [_pruebas unitarias_](https://es.wikipedia.org/wiki/Prueba_unitaria) que permiten una programación más fácil y una mejor calidad del código. Hemos finalizado el procedimiento de implementación del servidor lo que nos ha permitido realizar las primeras pruebas de campo. Las cosas están avanzando realmente y no podemos esperar a que Lacre llegue al servidor de Disroot, aunque eso va a demorar algún tiempo todavía. Antes queremos asegurarnos de que el sistema sea robusto, esté optimizado y que la mayoría de los errores y problemas estén resueltos.

Nuestro especial agradecimiento a **NLNET** por su apoyo financiero en este proyecto.

https://lacre.io/


# Control de Chat

Por último, pero no menos importante, queríamos añadir unas palabras sobre algunos cambios preocupantes en la legislación de la Unión Europea que afectarán a todo el mundo.

En efecto, la Comisión Europea está trabajando en una legislación llamada **Control de chat**, que obligaría a las plataformas online a escanear contenidos tales como textos, imágenes y videos creados por usuarixs para detectar material relacionado con abuso sexual infantil. Esto ciertamente sentará un peligroso precedente para que cada mensaje, imagen o video compartido en Internet sea objeto de vigilancia y censura. Abre las puertas a gobiernos y dictaduras presentes y futuros para rastrear, eliminar y silenciar cualquier tipo de oposición (la lucha contra el abuso infantil es solo "un papel de regalo"). Le otorga a los que controlan el software de IA de escaneo el poder y el control definitivos sobre la población, para que sepan quién dice qué a quién y cuándo. Sobre todo, socava totalmente las libertades de las personas y nuestro derecho a la privacidad. Esta nueva ley, si se traslada al mundo real, significará esencialmente que cada conversación privada que tengas con cualquier persona, deberá ser grabada y enviada a las autoridades como medida preventiva contra el crimen, y el no cumplimiento con dicha regulación sería punible.

¡Esto es totalmente inaceptable en la Europa del siglo XXI y debería ser rechazado!

Los gobiernos están intentando por todos los medios acabar con la mensajería privada y la libertad que nos da Internet. Utilizan diferentes "envoltorios" alrededor de los intentos de censura, tratando de venderlos al público como medidas preventivas y de protección, sin embargo en la mayoría de los casos fallan. Y aunque nosotrxs, el pueblo, nos hemos opuesto, parece que tarde o temprano esos canallas se saldrán con la suya.

Desgraciadamente parece que este es el momento.

Si valoras tu privacidad, haz lo que puedas para ayudar a detener esta locura. Ponte en contacto con tus diputados y comisionados europeos, organiza manifestaciones, haz que se escuche tu voz. Si perdemos nuestro derecho a la comunicación privada, poco después perderemos todas las pocas libertades que nos quedan. ¡Actúa ahora porque puede que ya sea demasiado tarde!

Si quieres entender mejor y obtener algunos argumentos para oponerte, consulta este análisis de Patrick Breyer, ex juez y actual diputado del Parlamento Europeo (https://www.patrick-breyer.de/en/posts/messaging-and-chat-control/) (en inglés).


El Equipo de Disroot