---
title: 'Spring update'
date: 06/26/2017
taxonomy:
  category: news
  tag: [disroot, news, matrix, nextcloud]
body_classes: 'single single-post'

---



**Dear disrooters,**
It’s been a while since you heard from us last time. At least some of you. Last months we’ve been very bad in informing you about the changes. We blame matrix for it 😛 We’ve been very active on our latest additional service — where quite a nice community has formed — and kept on postponing the usual updates forever. We will do our best that it doesn’t happen again, and to keep you up to date on all communication channels from now on.
Last months have been truly busy at Disroot. The entire palette of emotions was accompanying us through that time. From uber awesomeness and fun, to sweat, tears, frustration and broken keyboards.
Here are some things we’ve been working on:
---------------------

## Matrix is live!
Like we announced in previous posts, we’ve deployed Matrix as Disroot’s default chat. From the very beginning we were positively surprised by the new community that was forming around the young chat platform and the active involvement of Disrooters. A lot of you helped us by adding interesting chat–rooms from other servers, find and fix a lot of bugs and issues – not only on matrix itself but on all other services on the Disroot platform. Thanks a lot guys!
That said, our experience with Matrix wasn’t all sweets and candy. Matrix has caused us a lot of stress and work as we were experiencing federation issue with the biggest server hosted by matrix.org. Messages from users on matrix.org side would lag for hours or not ever reach our server, causing a lot of frustration for users and making the whole federation concept feel like a fail. However, together with matrix.org developers we’ve managed to troubleshoot the issue and for last weeks matrix has been stable and the federation to matrix.org was re-established. As this was a never-ending three-week-long nightmare, we keep an eye on it so if it ever occurs again we know what to do.
For all of you who would like to join the Matrix, say hi, or just chat about anything, pop into our Disroot room (#disroot:disroot.org), any other public room or just create your own and invite your friends.
-------------------

## Howtos and Tutorials
We also spent few weeks in january/february writing lots of documentation: [https://disroot.org/tutorials/](https://disroot.org/tutorials/). Thanks to help from ‘maryjane’ and ‘dosch’ we’ve managed to cover most of the services offered by Disroot. There is still long way to go as we are still missing some needed tutorials and we would like to create instructional videos for all those that don‘t like reading that much. Its definitely a step in the right direction and something we were missing for a long time. Please have a look and if you feel inspired to write some Howtos, don’t hesitate and just do it. If you want to coordinate it with us, ask on our matrix room or use any other means to contact us [https://disroot.org/contact](https://disroot.org/contact)
------------------------
## Nextcloud 12.
We’ve just updated Nextcloud to the glorious version 12. We’ve also updated to php7 and latest Nginx which enables us to use HTTP/2. Most importantly, there is a noticeable performance boost with the new update (we are still tweaking the settings).
As always there are lots of new functionalities and User Interface. You can read in detail about it here: https://nextcloud.com/blog/welcome-to-nextcloud-12/
Changes worth noting:

  - **App order** – To order your apps, you now need to do it in your personal settings.
  - **Calls** – Apart from Screen notification about new call invites (soon on mobile too) you can also share screen with others. Makes things much better when having a meeting or when you troubleshoot your mother’s Linux desktop
  - **Circles** – Private cloud at your service. Create groups of friends you want to share files with, or public group anyone can join. The app is still in early stages so expect a lot of changes and new features soon.
  - **Sharing directly to your social networks** – Now you can share to your Diaspora*, g+, Facebook, Twitter and Email
  - **Working together** – possibility to lock the file you are working on so others won’t overwrite your changes if you work in a group.
--------------------------

## [https://state.disroot.org](https://state.disroot.org)
For a while now we were looking for possibility to inform you guys about the state of all the services and possible issues or updates. Also we wanted to have an easy way to inform about scheduled downtimes and upgrades. Thanks to Cachet project we can do just that. It gives a pretty overview of the state of all services we run, plus it shows all changes on a timeline. From now on, if you want to know about the upcoming downtimes (services unavailable), issues, updates etc in one place, check state.disroot.org. You can subscribe to RSS feed or email to get notified about everything (or only services you are interested in) directly without a need to enter the website. We’ve also created a Matrix room at #state:disroot.org where we re-post things from cachet and in the future we hope to be able to re-post to diaspora too.

--------------------------
## Becoming a legal entity
As most of you know,  Disroot is currently run by few people personally responsible for the entire project. We would like to turn it into a non-profit organizations as it will make our life easier in terms of legal advice, taxes, possible future subpoenas, asking for funding, collecting donations and selling merchandise or extra storage. Seeing as Disroot’s user–base is growing faster and faster we need to secure our financial and legal status in order to handle the load and growth. Last months we did quite a progress on things and we hope, by this time next month Disroot will become a foundation.
A special thanks for the party organizers that threw a very successful benefit rave for us! The cash will help us with the process of becoming a foundation and will cover some of our costs from last year \♥/


As always, if you wish to donate money or hardware to the project visit: [https://disroot.org/donate/](https://disroot.org/donate/) We’ve made a short financial overview for 2016 which doesn’t look so good. in order for 2017 to be better, we need you to grab your wallets and throw some money at us. Donating even 1euro a month (or whatever your currency is) makes huge change. If everyone does that we will never have to worry about the financial state of the project.
If you can, please don’t use PayPal. We would like to promote an alternative awesome open source project called liberapay.com where you can support us and other projects with your contributions.

### And in this summery mood, we wish you happy days and self gardened vegetables.
