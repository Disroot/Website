---
title: 'DisNews #2 - Yearly report; Moving to gitea; lacre.io; Ansible and gitea;'
date: '27-05-2021 0:10'
media_order: israestine.jpg
taxonomy:
  category: news
  tag: [disroot, news, yearly_report, report, gitea, lacre, ansible]
body_classes: 'single single-post'

---

Two months has passed since our last newsletter. As always, ton of things to share with all of you.

## Yearly report

Something we wanted to avoid this year but looks like it's a new tradition. Seems like January was just last week, but we are at the end of May. Here, our last year's report published in the middle of this year. Better later then never some say. And yet, let's hope we do a better job from now on and publish these year reports in January. Nonetheless, here it is, a short summary of the craziness the year 2020 has been for everyone. Enjoy! Let's all try to forget about 2020 as soon as we can. you can read the report [here](https://disroot.org/en/annual_reports/AnnualReport2020.pdf)


## Disroot - Project board. Moving everything to Gitea

During the last months we have been using our git instance more and more. To the point we have come to realize we should move all our work coordination there to have everything in one place. And so, we have also decided to move the main Project-Board from Deck to Gitea. We currently coordinate all the work through Gitea anyway and keeping all things in one place makes much more sense for us internally. This will also give a better picture for everyone, adding to the transparency and openness we wanted to focus on this year.


[Disroot Project](https://git.disroot.org/Disroot/Disroot-Project) board works as a general board where we, and all Disrooters, submit issues, feedback, discussions, feature requests etc. We then distribute those issues, across all the projects we host (website, themes, ansible roles, changelogs etc). It's a central point of coordination and central point for Disrooters to submit feedback. Our [Changelog](https://git.disroot.org/Disroot/CHANGELOG) repository is meant for our internal use in terms of creating and coordinating works for our bi-weekly maintenance windows as well as a way for all of you to see what is on the plate currently. All changes from changelog are now also published on our [website](https://disroot.org/en/changelog) so that you can follow past changes on the platform.

## Ansible roles

Since the beginning of the year we have been working on finishing ansible roles and publishing them. It's been a hell of a work for the last year or so, and now finally we are moving forward. Seeing the results of all the work so far is very satisfying. Not only it could help others to set things up easily, but also added tremendous improvements in regards to stability of the platform. It also helps a lot to test upgrades prior to any work on production, workout new features and changes before they hit the servers and serves a better way to coordinate the work between us internally. You can find repositories [here](https://git.disroot.org/Disroot-Ansible)

## Lacre

We are starting with the work on the mailbox encryption. We have decided to rename the project from just gpg mailgate to "Lacre". Lacre stands for letter wax seal in Portuguese and Spanish. We really like the new name and ideas for logos/art work that comes with it. Currently we are starting slowly by assesing the work and preparing the test environment. [https://lacre.io](https://lacre.io)


We want to send our shoutout to **@pfm** who volounteered to help with the development part of the project. We are very grateful for the offer and we are looking forward to getting the mailbox encryption landing on Disroot. It is still long way ahead of us, but we are very happy and excited that we have started moving forward.


We wish you all a great June but remember to still keep distance and take care of yourself and others. To all our friends who are struggling with the pandemic and devastating conflicts around the world, stay strong, stay safe.
