---
title: 'DisNews #6 - domini personalizzati per email; miglior gestione dello SPAM; webmail Roundcube'
date: '20-09-2022'
media_order: stickers.jpg
taxonomy:
  category: news
  tag: [disroot, news, mail, roundcube, webmail]
body_classes: 'single single-post'

---

Ciao Disrooter,
è passato un po' di tempo dall'ultima volta che ci hai sentito. Questo post, che avevamo pianificato per luglio è stato procrastinato. Ciò è stato dovuto al fatto che dovevamo occuparci di alcune faccende personali e dovevamo risolvere alcuni problemi urgenti sul server che rendevano impossibile il rilascio delle funzionalità pianificate. Per fortuna tutto ciò è alle nostre spalle e siamo di nuovo in carreggiata. Di seguito vorremmo presentarti un riassunto degli ultimi tre mesi di lavoro.

Ecco una panoramica di quello che abbiamo fatto.

# Catchall per domini personalizzati per email

Alcuni di voi hanno deciso di collegare il proprio dominio personalizzato al nostro server di posta. Questa funzione ti permette di ricevere tutte le email utilizzando il tuo dominio personalizzato. Ora chiunque abbia il proprio dominio collegato al nostro servizio, può pure abilitare o disabilitare la funzione **catchall** aggiornando le impostazioni del proprio account su [https://user.disroot.org](https://user.disroot.org /pwm/privato/updateprofile). Questa ulteriore feature è stato anche uno dei motivi per cui non abbiamo elaborato nuove richieste di collegamento di domini negli ultimi tempi. Quindi se ne hai richiesto di collegare il tuo dominio personale ai nostri server, avrai prestissimo buone notizie da parte nostra.

# SearxNG

La nostra amata istanza Searx è inutilizzabile da un po' di tempo. Abbiamo cercato di trovare un modo per migliorare la situazione ma non siamo riusciti a trovare una soluzione che ci soddisfacesse. Alcuni Disrooter ci avevano indicato un recente fork di Searx chiamato SearxNG. Abbiamo deciso di provarlo e con nostra grande sorpresa il nostro SearxNG ha iniziato a fornire buoni risultati. Siamo molto contenti di questo cambiamento, ma non abbiamo ancora deciso se rimanere con SearxNG o tornare a Searx. Vogliamo indagare meglio la situazione per prendere la migliore decisione possibile. Searx è stato con noi sin dall'inizio, quindi è necessario riflettere bene su questa decisione.

# Migliore gestione dello SPAM

Nel nostro ultimo post, avevamo annunciato la nostra decisione di spostare automaticamente lo spam nelle cartelle di posta indesiderata. Di recente abbiamo allentato parte della prevenzione dello spam come i controlli dell'elenco RBL, il greylisting e il numero di altri controlli che rifiutavano direttamente le e-mail sospette. Volevamo dare più potere decisionale ai Disrooter per gestire le loro email. Questo porta naturalmente ad un aumento di potenziali e-mail di spam. Per aiutare a combattere la crescente quantità di SPAM, abbiamo ordinato al nostro bot anti-spam di apprendere automaticamente cos'è lo spam e cos'è ham. La scelta dell'opzione di apprendimento automatico ci consente di migliorare il rilevamento automatico dello spam. Per rispondere ai falsi positivi, email "legittime" contrassegnate automaticamente come spam, ti invitiamo a inoltrare tali email (o almeno le intestazioni) all'indirizzo email **ham.report@disroot.org**. Questo ci consentirà di modificare le impostazioni in modo che questi indirizzo non finiscano nella cartella posta indesiderata. Abbiamo inoltre creato un indirizzo email dove puoi segnalare email che dovrebbero essere contrassegnate come spam (**spam.report@disroot.org**). Con il tuo feedback, saremo in grado di trovare l'equilibrio nella guerra allo spam e migliorare la situazione per tutti.

# Roundcube: la nostra nuova webmail

Ricordi quando abbiamo annunciato la migrazione a Roundcube nel 2020? Beh, ci è voluto un po', ma finalmente ci siamo. La nostra nuova webmail è pronta per essere utilizzata. Puoi raggiungerla con il nuovo URL su [https://webmail.disroot.org](https://webmail.disroot.org). Provala e aiutaci fornendoci un feedback. Siamo consapevoli di alcune migliorie che potremmo attuale e siamo sicuri che ci sarà molto altro da scoprire una volta che più persone inizieranno a usarla. Per il momento forniamo sia SnappyMail che Roundcube, ma alla fine forniremo una sola soluzione. Ciò che è particolarmente interessante di Roundcube è la sincronizzazione automatica dei contatti con Nextcloud. Per sapere come migrare i tuoi contatti da snappymail (webmail corrente) a Roundcube o Nextcloud, controlla la [guida](https://howto.disroot.org/en/tutorials/email/webmail/migration) preparata da @fede.

# Aumento del limite dei file caricati su XMPP

Abbiamo deciso di aumentare il limite per i file caricati utilizzando il nostro servizio di chat xmpp da 10 MB a 512 MB! Nel farlo, abbiamo anche deciso che aveva senso rendere uguali la conservazione dei file e la cronologia delle chat che ora abbiamo impostato entrambi ad 1 mese.

# Stickers, stickers, stickers!!!!

Stavi sognando di mettere quel fantastico adesivo disroot sul tuo laptop per mostrare a tutti che stai utilizzando un servizio di cui nessuno ha sentito parlare?:) Da tempo volevamo fornire del merchandising per tutti i disrooter e mentre aspettiamo che @muppeth e @antilopa costruiscano la loro struttura per la stampa di magliette, abbiamo deciso di iniziare con i pacchetti di adesivi. E così, ti presentiamo il primo pacchetto di adesivi disroot. L'idea è semplice. Dona almeno 10 euro e lascia l'indirizzo a cui dovremmo inviarti gli adesivi in riferimento alla tua donazione. Abbiamo in programma di cambiare design ogni volta che ordiniamo un nuovo lotto di adesivi per rendere le cose più interessanti.

Questo è tutto per ora. Stiamo pianificando come sempre grandi cose, che forse prenderanno forma o forse no;). 

Speriamo in notizie più regolari d'ora in poi :P

Ciao e divertiti!
