---
title: 'DisNews 24.03 - Nuovi servizi e Lacre che raggiunge i server'
date: '11-03-2024'
media_order: tinker.png
taxonomy:
  category: news
  tag: [disroot, news, lacre, lemmy]
body_classes: 'single single-post'

---
Ho Ho Ho! Buon anno e ciao a tutti.

Accidenti, questa volta ci è voluta una vita. Il proposito del nuovo anno di postare regolarmente sembra aver preso una brutta piega fin dall'inizio. Non abbiamo idea di cosa sia successo, visto che questo post è stato sottoposto a diverse iterazioni e modifiche a causa del fatto che è stato rimandato più e più volte. 
Speriamo che i vostri propositi reggano meglio dei nostri e che abbiate iniziato alla grande l'anno nuovo.

Innanzitutto, siamo orgogliosi ed entusiasti del fatto che quest'anno pubblicheremo il nostro rapporto annuale ora e non a luglio. Una pacca sulla spalla al nostro team che è riuscito a terminarlo nei tempi previsti. Come sempre, nel rapporto è possibile leggere alcuni punti salienti dell'anno, una panoramica finanziaria e i piani per il 2024. Consultatelo [qui](https://disroot.org/annual_reports/AnnualReport2023.pdf)

Un altro aspetto degno di nota sono le informazioni aggiuntive contenute nella nostra [Informativa sulla privacy](https://disroot.org/privacy_policy) (versione 1.5). Le modifiche più importanti sono:
  - Informazioni sul nuovo servizio [D-Scribe](https://scribe.disroot.org)
  - Informazioni sulla nuova webchat [Movim](https://webchat.disroot.org)
  - Informazioni sul nuovo servizio [LibreTranslate](https://translate.disroot.org)
  - Nuove informazioni sui backup del server **(1.3)**

Per un po' di tempo i nostri post sono stati irregolari e non abbiamo parlato molto delle nuove funzionalità di ciascun servizio e, quando li abbiamo pubblicati, ci siamo concentrati sulle notizie generali di Disroot-come-piattaforma. Vogliamo cambiare questa situazione e informarvi sulle nuove funzionalità, sui cambiamenti e sui miglioramenti che avvengono su tutti i servizi su base mensile. Inoltre, come suggerito da alcuni, abbiamo cambiato la numerazione delle newsletter e d'ora in poi, come avrete notato, sarà Anno.Mese (24.03). Principalmente per mantenere la coerenza con [Disroot changelog](https://disroot.org/changelog), ma anche per rendere più facile trovare le cose quando necessario. Passiamo quindi alle ultime novità degli ultimi tre mesi:

# Nuovo servizio D-Scribe (powered by Lemmy)

Iniziamo con un nuovo servizio. Da qualche settimana stiamo testando un servizio sostitutivo del forum che abbiamo eliminato gradualmente l'anno scorso. Finora ne siamo soddisfatti e abbiamo deciso di invitarvi a provarlo. Lemmy è un servizio simile a reddit, ma con un pizzico di magia del fediverso. Il servizio consente di iscriversi e di creare comunità pubbliche in cui è possibile pubblicare link a cose interessanti o avviare discussioni. Lemmy è un servizio federato. Ciò significa che quando si utilizza la nostra istanza, non si è limitati ai contenuti e agli utenti dell'istanza, ma si può interagire con le comunità di molti server. Stiamo ancora testando il terreno, ma sembra che i disrooters spagnoli si siano già dati da fare e si stiano divertendo. Quindi non perdete altro tempo, create un account (richiede un account separato), condividete e interagite. @fede ha già [preparato dei tutorial](https://howto.disroot.org/en/tutorials/social/dscribe). È possibile esplorare le comunità al di fuori del nostro server [qui](https://lemmyverse.net/communities).

# Temi

Nelle ultime settimane @antilopa ha lavorato a una tavolozza di colori più unificata per i temi di Roundcube e SearXNG. Il risultato è già abbastanza buono. Siamo particolarmente soddisfatti dei temi scuri. Buon divertimento :D

# LibreTranslate

Oh! Un altro servizio. Mentre ci preparavamo a chiudere l'ultimo anno, @meaz ci ha proposto una configurazione pronta per LibreTranslate. Dal momento che questo servizio è stato richiesto da un buon numero di disrooters. Siamo quindi felici di annunciare che abbiamo il nostro servizio Disroot Translate all'indirizzo [https://translate.disroot.org](https://translate.disroot.org).

# Coltiviamo il nostro orticello!

Questa è una notizia sconvolgente. Guardando lo stato delle finanze e l'ammontare delle entrate nell'ultimo anno, abbiamo deciso che possiamo garantire fondi per diversi scopi (controllate il rapporto annuale per i dettagli). Tra questi, un compenso finanziario per @muppeth. Anche se non è pagato per il suo tempo pieno, è un passo importante. Grazie mille! Abbiamo deciso di assegnare a @muppeth un salario mensile di 1000 euro per quest'anno. Poiché non vogliamo avere problemi finanziari, questo si basa sui soldi che abbiamo raccolto in precedenza. Vogliamo essere in grado di diventare veramente indipendenti dal punto di vista finanziario in futuro e di retribuire l'intero team a tempo pieno. Il vostro sostegno finanziario al progetto è fondamentale. [Continuate a bere caffeina!](https://disroot.org/donate)

# Lacre

Ok. L'abbiamo annunciato da molto tempo. Oggi è finalmente arrivato il momento di colpire i server Disroot. Da una settimana stiamo eseguendo un cosiddetto test silenzioso sui server Disroot. Dal momento che tutte le e-mail devono passare attraverso Lacre, che deciderà se crittografarle o meno, possiamo testare le prestazioni di Lacre e scoprire alcuni dei bug che non siamo riusciti a trovare sul server di prova (semplicemente a causa del fattore di scala). Per questo motivo, nelle ultime settimane @pfm ha risolto alcuni casi limite che abbiamo riscontrato. Una volta risolti la maggior parte dei problemi, importeremo le prime chiavi di coloro che sono abbastanza coraggiosi da provare la crittografia. Poi annunceremo un vero e proprio beta test. Tenete quindi gli occhi e le orecchie aperte in aprile, perché Lacre arriverà sicuramente in primavera.
Il prossimo anno è già ricco di lavoro e abbiamo in programma di fare molto. Consultate il rapporto annuale per leggere tutte le novità. Siamo sicuri che quest'anno faremo tutto questo, ma tutto a piccoli passi. Potrete leggere tutti i progressi nelle prossime newsletter. 

# Un ringraziamento speciale!
Vorremmo dare un riconoscimento a tutti coloro che di recente sono stati coinvolti nella nostra [issue board](https://git.disroot.org/Disroot/Disroot-Project/issues) fornendo feedback, suggerimenti, aiutando a testare e a debuggare i problemi, oltre a dare una mano nelle traduzioni, aiutando con i testi del sito web e molto altro ancora. Questo piccolo angolo del progetto sta diventando sempre più vivo ed è bello vedere i disrooters prendere parte più attivamente alla creazione del progetto. Un ringraziamento speciale a @floss4good, @Shadowstreik, @l3o, @mester, @kitleopold! 

# Obiettivi per il 2024
A proposito di [issue board](https://git.disroot.org/Disroot/Disroot-Project/issues), nel [Rapporto annuale 2023](https://disroot.org/annual_reports/AnnualReport2023.pdf), potete leggere i nostri piani per quest'anno. Per rendere le cose un po' più chiare e facili da seguire, abbiamo creato una serie di [project board](https://git.disroot.org/Disroot/Disroot-Project/projects) in cui è possibile monitorare i progressi degli obiettivi.  


State in forma!
Il core team di Disroot
