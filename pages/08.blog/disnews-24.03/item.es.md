---
title: 'DisNews 24.03 - Nuevos servicios y Lacre alcanzando al servidor'
date: '11-03-2024'
media_order: tinker.png
taxonomy:
  category: news
  tag: [disroot, news, lacre, lemmy]
body_classes: 'single single-post'

---
¡Ho Ho Ho! ¡Feliz año nuevo! Hola.

Carajo, este demoró una eternidad. El objetivo de Año Nuevo de publicar de manera regular parece estar dando un mal giro de entrada. Ni idea qué sucedió, porque este post fue pasando por una cantidad de versiones y cambios como consecuencia de venir postergándose una y otra vez. Está bien. Esta vez en serio. Esperamos que sus objetivos se mantengan mejor que los nuestros y los hayan resuelto en el nuevo año.

Primero que nada, estamos orgullosos y entusiasmados de que este año estemos publicando nuestro reporte anual ahora y no en julio. Palmadas en la espalda para el equipo por entregar a tiempo. Como siempre, en el reporte pueden leer algo de lo más destacado del año, el resumen financiero y también la cumbre de los planes para 2024. Pueden leerlo [aquí](https://cloud.disroot.org/s/xFspo6FdktEi4yd).

Otro tema digno de mencionar es la información adicional agregada en nuestra [Política de Privacidad](https://disroot.org/es/privacy_policy) (version 1.5). Los cambios más notables son:
  - Información respecto al nuevo servicio [D•Scribe](https://scribe.disroot.org)
  - Información respecto al nuevo webchat [Movim](https://webchat.disroot.org)
  - Información respecto al nuevo servicio [LibreTranslate](https://translate.disroot.org)
  - Nueva información respecto a los respaldos del servidor **(1.3)**


Durante largo tiempo nuestras publicaciones fueron irregulares y no mencionábamos mucho acerca de nuevas características en cada servicio, y cuando lo hacíamos, nos enfocábamos más en noticias generales sobre Disroot como plataforma. Queremos cambiar eso e informarles sobre nuevas funcionalidades, cambios y mejoras que se van dando en todos los servicios de manera mensual. Comprobamos los registros de cambios así no tienen que hacerlo ustedes. Además, como algunas personas sugirieron, cambiamos la numeración de los boletines y a partir de ahora, como habrán notado, será Año.Mes (24.03). Principalmente para mantener la coherencia con el [registro de cambios de Disroot](https://disroot.org/changelog) pero también para que sea más fácil encontrar las cosas cuando es necesario. Pasemos entonces a lo más destacado de los últimos tres meses:


# Nuevo servicio D•Scribe (desarrollado por Lemmy)

Empecemos con un nuevo servicio. Durante algunas semanas hemos estado probando un remplazo para el servicio de foro que dimos de baja el año pasado. Estamos felices con él hasta ahora y por eso decidimos invitarles a probarlo. **Lemmy** es un servicio parecido a **Reddit**, pero con una pizca de magia del Fediverso. Permite suscribirse y crear comunidades públicas donde pueden compartir enlaces a cosas interesantes o comenzar hilos de discusión. Así es. Lemmy es un servicio federado, esto quiere decir que cuando usan nuestra instancia, no están limitados al contenido y usuaries de la instancia, sino que pueden interactuar con comunidades a través de los muchos servidores que hay por ahí. Todavía estamos probando el terreno, pero parece que las y los disrooters de habla hispana ya se pusieron con todo y lo están disfrutando. Así que no pierdan más tiempo, creen una cuenta (requiere una separada), y compartan e interactuen. @fede ya [preparó unos tutoriales](https://howto.disroot.org/es/tutorials/social/dscribe). Pueden explorar comunidades fuera de nuestro servidor [aquí](https://lemmyverse.net/communities)

# Temas

Las últimas semanas @antilopa ha trabajado en una paleta más unificada de colores para los temas de **Roundcube** y **SearXNG**. El resultado ya es bastante bueno. Estamos particularmente contentos con los temas oscuros porque esos son difíciles de crear, sobre todo con "los colores de Disroot". Disfruten :D

# LibreTranslate

¡Ah! Otro servicio. Mientras se preparaba para cerrar el año que pasó, @meaz salió de la nada con una configuración lista para implementar **LibreTranslate**. Como esto ha sido solicitado por una cantidad de disrooters, y tiene un factor muy bajo de desperdicio / abuso (si es que tiene alguno), dicidimos darle para adelante. Así que estamos felices de anunciar que tenemos nuestro propio servicio Disroot de traducción en [https://translate.disroot.org](https://translate.disroot.org).

# ¡Cultivando nuestro propio apio!

Esta es una noticia alucinante. Viendo el estado de las finanzas y la cantidad de ingresos del año pasado, decidimos que podemos asegurar fondos para varios propósitos (revisen el reporte anual para más detalles). Entre ellos, una compensación económica para @muppeth. Aunque no es un pago por su tiempo completo, es un paso impresionante. ¡Muchas gracias! Decidimos que podemos asignar remuneraciones mensuales a @muppeth de 1000 euros por este año. Como no queremos meternos en problemas financieros, esto se sostiene con dinero que hemos juntado previamente. Deseamos a futuro poder volvernos económicamente independientes de verdad y compensar al equipo entero a tiempo completo. Su apoyo económico al proyecto es crucial. [¡Sigan trayendo esa cafeína!](https://disroot.org/es/donate)

# Lacre

Muy bien. Le dimos manija por bastante tiempo ya. Es momento de que por fin llegue a los servidores de Disroot. Durante las últimas semanas estuvimos corriendo lo que se llama "una prueba silenciosa" en el servidor de Disroot. Como todos los correos necesitan pasar a través de **Lacre**, que entonces decidirá cuáles cifrar y cuáles no, podemos probar su performance y pescar algunos de los errores (o bugs) que no podríamos encontrar en un servidor de prueba (básicamente por un tema de escala). Y por eso durante las últimas semanas @pfm ha estado planchando cosas como resultado de los casos puntuales que hemos encontrado. Una vez que la mayoría de los problemas estén resueltos, importaremos las primeras claves de aquellas personas suficientemente valientes para probar el cifrado. Luego anunciaremos las pruebas beta formales. ¡Así que mantengan los ojos y oídos abiertos en abril porque Lacre está definitivamente llegando esta primavera (boreal) \o/ !
Este año que comienza ya está completo de trabajo y estamos planeando hacer un montón de cosas. Vean el reporte anual para leer sobre todo el revuelo. Estamos confiados que haremos todas esas cosas este año pero todo a pasitos de bebé. Podrán enterarse sobre el progreso en los boletines próximos.

# ¡Mención especial!
Nos gustaría agradecer a quienes recientemente se han involucrado más en nuestro [tablero de asuntos/problemas](https://git.disroot.org/Disroot/Disroot-Project/issues) proporcionando información, sugerencias, ayudando a probar y depurar problemas, también dando una mano con las traducciones, los textos del sitio y mucho, mucho más. Esa esquinita de Disroot está cada vez más y más viva y es maravilloso ver disrooters asumiendo un papel más activo en darle forma al proyecto. ¡Agradecimientos especiales a @floss4good, @Shadowstreik, @l3o, @mester, @kitleopold!

# Objetivos para 2024
Hablando del [tablero de asuntos/problemas](https://git.disroot.org/Disroot/Disroot-Project/issues). En el [Reporte Anual 2023](https://cloud.disroot.org/s/xFspo6FdktEi4yd), pueden leer sobre nuestros planes para este año. Para hacer las cosas un poco más claras y sencillas de seguir, hemos creado un puñado de [tableros de proyecto](https://git.disroot.org/Disroot/Disroot-Project/projects) donde pueden supervisar el progresos de los objetivos.  


¡Mantengan la calma!  
· El equipo de Disroot ·
