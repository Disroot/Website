---
title: 'DisNews 24.03 - New services and Lacre hitting the server'
date: '11-03-2024'
media_order: tinker.png
taxonomy:
  category: news
  tag: [disroot, news, lacre, lemmy]
body_classes: 'single single-post'

---
Ho Ho Ho! Merry new year! Hi there.

Damn this one took forever. New year's resolution of posting on regular basis seem to be taking a bad turn from the get go. No idea what happen as this blog post was going through number of iterations and changes due to having the post being postponed over and over again. Ok. This time for real. Hope your resolutions are holding up better than ours and you have settled in the new year.

First of, we are proud and excited that this year, we are publishing our yearly report now and not in July. Pat in the back for the team to deliver on time. As always in the report you can read some highlights of the year, financial overview as well as peak for plans for 2024. Check it [here](https://disroot.org/annual_reports/AnnualReport2023.pdf)

Another point worth mentioning is additional information in our [Privacy policy](https://disroot.org/privacy_policy) (version 1.5). Most notable changes are:
  - Information regarding new service [D•Scribe](https://scribe.disroot.org)
  - Information regarding new webchat [Movim](https://webchat.disroot.org)
  - Information regarding new service [LibreTranslate](https://translate.disroot.org)
  - New information regarding server backups **(1.3)**


For quite a while our postings were irregular and we did not mention much about new features on each service, and when we did post, we rather focused on general Disroot-as-a-platform news. We want to change that and inform you about new features, changes and improvements happening on all services on a monthly basis. We check upstream changelogs so you don't have to. Additionally as some suggested we have changed the numbering of the newsletters and from now on, as you might have noticed, it will be Year.Month (24.03). Mainly to keep consistent with [Disroot changelog](https://disroot.org/changelog) but also make it easier to find things when needed. So on to the latest highlights from last three months:


# New Service D•Scribe (powered by Lemmy)

Let's start with new service. For few weeks we have been testing out a replacement to forum service we have phased out last year. We are happy with it so far and so decided to invite you to give it a try. Lemmy is a similar service to reddit, but with a pinch of fediverse magic. Service allows to subscribe and create public communities where you can post links to interesting things, or start discussion threads. That's right. Lemmy is a federated service. That means when using our instance, you are not limited to the content and users on the instance, but you can interact with communities across many servers out there. We are still testing the grounds, but looks like Spanish disrooters already went all-in and are enjoying it. So waste no more time, create an account (it requires separate account), and share and interact. @fede has already [cooked up tutorials](https://howto.disroot.org/en/tutorials/social/dscribe). You can explore communities outside our server [here](https://lemmyverse.net/communities)

# Themes

Last weeks @antilopa has worked on more unified color palette for themes on Roundcube and SearXNG. The result is pretty good already. We are especially happy with dark themes as it's hard to create those specially with the "Disroot colors". Enjoy :D

# LibreTranslate

Oh! Another service. While preparing to close the last year, @meaz came out of the blue with ready to deploy setup for LibreTranslate. Since this has been requested by number of disrooters, and has very low (if any) littering / abuse factor, we decided to just go ahead with it. And so we are happy to announce we have our own Disroot Translate service at [https://translate.disroot.org](https://translate.disroot.org).

# Growing our own celery!

This is mind blowing news. Looking at the state of finances and amount of income in the past year, we decided we can secure funds for several purposes (check the yearly report for details). Among those, a financial compensation for @muppeth. Although not paid for his full time, it's an awesome step. Thank you so much! We have decided we can allocate monthly wages to @muppeth of 1000 euro for this year. As we do not want to get into financial issues, this is based on money we have gathered prior. We wish to be able to truly become financially independent in the future and compensate entire team full time. Your financial support of the project is crucial. [Keep that caffeine coming!](https://disroot.org/donate)

# Lacre

Ok. We hyped it for a long time now. It's time to hit the Disroot servers finally. For past week we are running a so called silent testing on Disroot server. Since all email does need to pass through Lacre, which will then decide whether to encrypt or not, we can test Lacre's performance and fish out some of the bugs we could not find when on test server (simply due to the scale factor). And so for past weeks @pfm has been ironing out things as result of edge cases we have find. Once most of the issues are solved, we will import first keys from those brave enough to try the encryption out. Then we will announce a proper beta tests. So keep your eyes and ears open in April cause Lacre is definitely coming your way this spring \o/
The coming year is packed with work already and we are planning to do a lot. Check the yearly report to read about all the hype. We are confident we will do all that stuff this year but everything in small baby steps. You will read about all the progress in the upcoming newsletters. 

# Special shoutout!
We would like to give a shoutout to everyone who recently got more involved on our [issue board](https://git.disroot.org/Disroot/Disroot-Project/issues) providing feedback, suggestions as well as helping test and debug issues as well as provide helping hand in translations, helping with website texts, and much much more. That little corner of the project is becoming more and more alive and it's nice to see disrooters taking more active part in shaping the project. Special thanks to @floss4good, @Shadowstreik, @l3o, @mester, @kitleopold! 

# Goals for 2024
Speaking of [issue board](https://git.disroot.org/Disroot/Disroot-Project/issues). In the [Annual report 2023](https://disroot.org/annual_reports/AnnualReport2023.pdf), you can read about our plans for this year. To make things a bit more clear and easy to follow, we have created bunch of [project boards](https://git.disroot.org/Disroot/Disroot-Project/projects) where you can monitor progress of goals.  


Stay cool!
-Disroot Core Team
