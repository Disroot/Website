---
title: 'El Fenomenal Desafío de Fin de Año del Alias Extra'
date: '23-12-2019 23:00'
media_order: front-page-image.jpg
taxonomy:
  category: news
  tag: [disroot, fundraising, domains, aliases]
body_classes: 'single single-post'

---

En los últimos días de este año, cuando el mundo entero se prepara para recibir la luz de una nueva década, tres dominios están peleando por el control del mercado de la remolacha. Unete a la batalla y elige a tu Jefe Supremo.
<br><br>
Desde el 24 de Diciembre hasta el 1ro de Enero, participa de nuestro evento de recaudación de fondos de fin de año. **Dona a Disroot** usando uno de los hashtags de dominios de abajo como tu referencia de donación. El dominio victorioso será otorgado como un alias extra para todxs lxs Disrooters ahora y en el futuro.
<br><br>
**¡Que el dominio más cool gane!**

 - **\#glorifiedhacker** - glorifiedhacker.net
 - **\#GetGoogleOffMe** - getgoogleoff.me
 - **\#Morecats** - morecats.net

Más información en: [https://disroot.org/aliaschallenge](https://disroot.org/aliaschallenge)
<br><br>

 <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.co.uk/videos/embed/5a3cbc03-1831-4dee-b2cd-92d80dd51f2b" frameborder="0" allowfullscreen></iframe>
