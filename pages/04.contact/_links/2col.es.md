---
title: Contacto
bgcolor: '#1F5C60'
fontcolor: '#FFF'
process:
    markdown: true
    twig: true
twig_first: false
---

[![](email.png?classes=contact,pull-left)](mailto:support@disroot.org) <div> <br/> support@disroot.org <br/> GPG: <a class="lighter" href="https://keys.openpgp.org/search?q=support%40disroot.org"><b>keys.openpgp.org</b></a><br/> Fingerprint: 70B5 C0F8 43CE 40F3 E1A4 ECB4 638F 13DE A52F 750B </div>

[![](mastodon.png?classes=contact)](https://nixnet.social/disroot) Mastodon:  <a class="lighter" href="https://nixnet.social/disroot">nixnet.social/disroot</a>

---

![](irc.png?classes=contact) IRC: #disroot en irc.libera.chat

[![](xmpp.png?classes=contact)](xmpp:disroot@chat.disroot.org?join) XMPP: <a class="lighter" href="xmpp:disroot@chat.disroot.org?join">disroot@chat.disroot.org/@disroot.es</a>

[![](webchat.png?classes=contact)](https://webchat.disroot.org/) XMPP <a class="lighter" href="https://webchat.disroot.org/">webchat</a>

[![](nextcloud.png?classes=contact)](https://cloud.disroot.org/call/di5y9zno
) Nextcloud: <a class="lighter" href="https://cloud.disroot.org/call/di5y9zno">cloud.disroot.org/call/di5y9zno</a>


---

Las salas listadas están todas conectadas. Esto siginifica que no importa cuál de las salas de lista se elija, podemos comunicarnos con la comunidad a través de un bot.
