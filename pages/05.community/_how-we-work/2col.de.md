---
title: 'Wie kann ich sehen'
published: true
bgcolor: '#FFF'
fontcolor: '#555'
---

![](puzzle-piece.png?resize=45,45)<br>
# Wie arbeiten wir?

Wir nutzen ein Projektboard, wo wir all die offenen Arbeitsschritte sammeln. Wir haben uns für eine "flexible Methodik" als Startpunkt entschieden und wir arbeiten in Sprints, welche kurze Zeitabschnitte beschreiben. In unserem Fall dauert ein Sprint zwei Wochen. Am Anfang des Sprints setzen wir uns zusammen und entscheiden, welche Aufgaben Priorität haben und zum aktuellen Aufgaben-Board hinzugefügt werden sollen. Dann, in einem Zeitraum von zwei Wochen, tun wir unser Bestes, um so viele Aufgaben wie möglich zu erledigen. Wenn die Zeit um ist, schließen wir den Sprint, schieben unfertige Aufgaben zurück in den Backlog-Stapel und senden eine Nachricht über die aktuellen Änderungen und Neuentwicklungen auf der **Disroot**-Plattform in alle Kanäle (Mastodon, Blog).

Zusaätzlich veröffentlichen wir einmal im Jahr unseren **Jahresbericht**, der eine Zusammenfassung dessen darstellt, was im vergangenen Jahr getan wurde, wie hoch die Kosten und Einnahmen des Jahres waren und was unser angestrebtes Budget und unsere Pläne für das nächste Jahr sind.

---

![](eye.png?resize=45,45)<br>
# Wie kann ich sehen, was passiert?

Die beste Möglichkeit zu sehen, mit was wir momentan beschäftigt sind, was wir bisher getan haben und was unsere Pläne für die Zukunft sind, ist es, unserem git repositories zu folgen.

[Disroot-Projekt](https://git.disroot.org/Disroot/Disroot-Project) | [Aktueller Fortschritt](https://git.disroot.org/Disroot/CHANGELOG) |
[Liste der Fehler/Fragen](https://git.disroot.org/Disroot/Disroot-Project/issues)