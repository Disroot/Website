---
title: 'Cómo puedo ayudar'
bgcolor: '#FFF'
fontcolor: '#555'
---

### Realizar envío y seguimiento de errores
No importa si hemos estado utilizando **Disroot** desde hace tiempo o si recién nos unimos, reportar errores es algo que siempre podemos hacer. Así que si descubrimos que algo no luce del todo bien o no funciona como debería, podemos informar de ello abriendo una incidencia en el [Tablero de proyecto de Disroot](https://board.disroot.org/project/disroot-disroot/issues).

### Enviar características/funciones
También podemos usar el [tablero](https://board.disroot.org/project/disroot-disroot/issues) para hacer saber qué nuevas características o funciones nos gustaría ver en el futuro.

![](theme://images/logo_project_board.png?resize=35,35) [Tablero de proyecto](https://board.disroot.org/project/disroot-disroot/?classes=button5)

---

### Ayudar a resolver los problemas de otras personas
Si estamos usando **Disroot** desde hace tiempo, tenemos familiaridad con la plataforma o resolvimos algunos problemas antes, entonces podemos ser de gran ayuda para otras personas que están en apuros intentado encontrarle la vuelta a todas las características. Es también una ayuda enorme para el equipo central si más Disrooters colaboran respondiendo preguntas y solucionando problemas menores de otres, lo que implica que la carga de trabajo se reparta mejor y las respuestas puedan aparecer más rápidamente. Podemos comenzar mirando en la **sala XMPP** para ver cómo ayudar si alguien más lo necesita.

![](theme://images/logo_chat.png?resize=35,35) [disroot@chat.disroot.org](xmpp:disroot@chat.disroot.org?classes=button5)

---

### Ayudar con las guías
Ya hemos escrito y traducido unas cuantas [guías y tutoriales](https://howto.disroot.org/es) para ayudar a las personas usuarias a aprender cómo utilizar los servicios que proveemos, pero todavía queda un largo camino para cubrir todos los aspectos, para todos los dispositivos y sistemas operativos posibles. Necesitamos más gente involucrada en la redacción y traducción de las guías si queremos crear una documentación universal que pueda ser utilizada por nuestra comunidad y las de otras plataformas que brindan servicios similares, y que estén también, en la medida de lo posible, en sus propios idiomas.

![](theme://images/logo_h2.png?resize=35,35) [Proyecto Howto de Disroot](https://howto.disroot.org?classes=button5)
![](theme://images/logo_chat.png?resize=35,35) [howto@chat.disroot.org](xmpp:howto@chat.disroot.org?classes=button5)
 

### Donar dinero
Y por último, pero no menos importante, el apoyo financiero siempre es necesario, así que no teman arrojar un maletín lleno de dólares en nuestro camino. Y si eso es mucho, entonces recuerden que cada centavo suma y que si deciden "comprarnos un café" con frecuencia, eso sería grandioso. Para más detalles, mirar [aquí](https://disroot.org/es/donate)

---
