---
title: 'Roadmap'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

# Community

---      

**Transparency** plays a big role here in **Disroot**.
We want to be open about our plans, ideas and current progress.

We also want to encourage contributions to the project from the **Disroot** community.
Therefore, we've setup a bunch of tools to make it easy for anyone to follow **Disroot**'s development, and also take an active part in discussing, reporting issues and submitting ideas and fixes.
