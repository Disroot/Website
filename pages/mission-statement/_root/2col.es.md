---
title: Declaración de Objetivos
bgcolor: '#FFF'
fontcolor: '#1F5C60'
wider_column: right
---

## <br><br>
<span style="float:right;">
![](disroot-root.png)
</span>

---

## Misión de la Fundación Disroot.org

<br>
Los objetivos establecidos por la Fundación Disroot.org apuntan a recuperar Internet como la herramienta diversa e independiente que estaba destinada a ser.

La internet que una vez fue descentralizada, democrática y libre, ha ido siendo dominada por un puñado de gigantes de la tecnología, promoviendo la concentración en monopolios, más control gubernamental y más regulaciones restrictivas. Todo aquello que, en nuestra opinión, se opone y destruye a la verdadera esencia de esta maravillosa herramienta.

Promoviendo el uso de software libre y de código abierto apoyamos los esfuerzos colectivos y comunitarios dirigidos a la colaboración. Disroot.org pretende ser un ejemplo de que hay alternativas posibles al mundo corporativo que se ofrece en general. Alternativas que no se construyan sobre la base de obtener beneficios económicos. Alternativas para las que transparencia, apertura, tolerancia y respeto sean elementos clave.

### La plataforma

Disroot.org ofrece una plataforma de servicios web construida sobre los principios de privacidad, apertura, transparencia y libertad. Su objetivo más importante desde el punto de vista técnico es brindar el mejor servicio posible con el mejor nivel de soporte. Elegimos un enfoque de trabajo en el que los usuarios y usuarias (referidos desde ahora como Disrooters) son la parte más valiosa y los principales beneficiados del proyecto: las decisiones se toman pensando en ellos y no en el beneficio económico, la realización personal, el control o el poder. Disroot está comprometido a defender estos principios claves de la mejor manera posible.

La privacidad en línea se encuentra constantemente comprometida por aquellos que buscan obtener ganancias y control financiero. Nuestros datos se han convertido en una mercancía muy valiosa y la mayoría de las empresas en línea se aprovechan de ellos de forma abierta o subrepticia. Disroot.org se compromete a nunca vender, ayudar a procesar o utilizar cualquier información proporcionada por los usuarios y usuarias, a terceros con fines económicos, políticos o de poder. Los datos de los y las Disrooters les pertenecen a ellos y el proyecto simplemente los almacena para que los usen. Nuestro lema es "Cuanto menos sepamos de nuestros usuarios, mejor". Implementamos el cifrado de los datos siempre que es posible para garantizar que la obtención de datos de los usuarios y usuarias por parte de terceros sea lo más difícil posible y mantenemos solo el mínimo de registros o datos de usuarios que son esenciales para el funcionamiento del servicio.
