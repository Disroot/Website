---
title: Leitbild
bgcolor: '#1F5C60'
fontcolor: '#FFF'
---

---

### TL:DR (Too Long, DID Read)

Das Ziel von Disroot.org ist die Rückeroberung der virtuellen Welt von der monolithischen Realität, die sie übernommen hat. Wir sind uns sicher, dass dieser Ansatz gigantisches Potential hat, die Gesellschaft vom derzeitigen Status Quo zu entwurzeln, der von Werbung, Konsum und der Macht des höchsten Bieters bestimmt wird. Mit der Dezentralisierung eines kooperativen Netzwerks von Diensten im 21sten Jahrhundert können wir die durch finanzielle Beschränkungen, energetische Fußabdrücke und komplexe Infrastrukturen aufgezwungenen Unzulänglichkeiten überwinden - dem Netzwerk die Macht geben, gemäß seiner eigenen Regeln unabhängig zu wachsen. Ein Netzwerk kleiner, kooperierender Stämme wird den monolithischen Giganten bezwingen. Wirst Du dabei sein und gemeinsam mit uns entwurzeln?
