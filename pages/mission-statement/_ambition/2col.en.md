---
title: Mission Statement
bgcolor: '#1F5C60'
fontcolor: '#FFF'
---

---

### The ambitions

The Disroot.org Foundation has ambitions that go beyond the mere operation of the platform. The intention is to not become a centralized platform but to promote and sustain an ecosystem that helps others establish their own independent nodes. One of the most important long-term goals is to provide space for other organizations, individuals and collectives who want to start their journey so they don't have to reinvent the wheel over and over again. We want to encourage the emergence of more independent, decentralized service providers who work in cooperation - not competition - with each other. Together we can build a truly stronger decentralized network.

We openly promote and encourage the birth of new federated nodes and hope to be able to help other groups set up their Disroot-like platform. In order to support new projects with similar objectives, we are working on a set of tools to facilitate the implementation, maintenance and administration of the services. Another of our ambitions is to create a space for sharing skills and exchanging knowledge, create a legal aid platform to help others deal with rules, laws, regulations and law enforcement, help create new nodes and contribute to sponsoring the development of Libre and Open Source software.
