---
title: Leitbild
bgcolor: '#1F5C60'
fontcolor: '#FFF'
---

---

### Die Ziele

Die Disroot.org-Stiftung hat Ambitionen, die über das reine Betreiben der Plattform hinausgehen. Die Intention ist es nicht, eine zentralisierte Plattform zu werden, sondern vielmehr ein Ökosystem, das Anderen bei der Einrichtung ihrer eigenen, unabhängigen Nodes hilft, sie zu fördern und zu unterstützen. Eines der wichtigsten Langzeit-Ziele ist es, einen Ort für andere Organisationen, Individuen und Gemeinschaften anzubieten, die Ihren eigenen Weg beginnen wollen, sodass sie das Rad nicht wieder und wieder neu erfinden müssen. Wir wollen die Entstehung weiterer unabhängiger, dezentralisierter Dienste-Anbieter ermutigen, die in Kooperation - nicht in Konkurrenz - miteinander zusammenarbeiten. Zusammen können wir ein wahrhaft stärkeres dezentralisiertes Netzwerk aufbauen.

Wir fördern und ermutigen offen die Entstehung neuer föderierter Nodes und hoffen in der Lage zu sein, anderen Gruppen beim Aufbau ihrer Disroot-ähnlichen Plattformen helfen zu können. Um neue Projekte mit den gleichen Zielen unterstützen zu können, arbeiten wir an einer Reihe von Tools, die die Implementierung, Wartung und Administration der Services erleichtern. Ein weiteres unserer Ziele ist es, einen Ort zum Teilen von Fertigkeiten und zum Austausch von Wissen zu schaffen, eine Plattform zur rechtlichen Unterstützung, um Anderen beim Umgang mit Regeln, Gesetzen, Regulierungen und Strafverfolgungsbehörden zu helfen und sie bei der Erstellung neuer Nodes und der Beteiligung am Sponsoring der Entwicklung von freier und quelloffener Software zu unterstützen.
