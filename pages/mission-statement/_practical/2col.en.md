---
title: Mission Statement
bgcolor: '#FFF'
fontcolor: '#1F5C60'
---

---

### The Practicalities

In order to make the Disroot.org project work according to the above principles, we operate according to the following practical ones:

##### 1) Funding

Disroot.org is a non-profit foundation and all revenues from the Disroot platform are invested back into the project, the maintenance of services and into free software development.

We recognize different ways to fund the project and its development.

A. Donations from users.
<br>
We think that putting a price on our services is, in some way, limiting the autonomy of users. Therefore, we entrust this responsibility to Disrooters. We understand that the value of money varies according to different factors, such as geographical location, wealth status or personal financial situation, so we decided to let the Disrooters themselves evaluate how much they can contribute to the project.
<br>
It is important to highlight the fact that nothing on the Internet is free and that online services always have a cost that is paid for with real money or valuable private data. We are sure that Internet freedom can only be achieved and sustained when people realize this and contribute voluntarily. Since Disroot.org does not sell user data, we rely heavily on users donations.

B. Grants or subsidies.
<br>
The receipt of grants and other forms of funding will only be accepted if they do not harm in any way the independence of the project. We will not jeopardize Disrooters’ privacy, or our freedom and the way we want to manage the service, to meet the requirements set by the “donors”. We are aware that grants are often allocated for a specific purpose or result but we are also convinced that some of those requirements can be pursued without harming the project or the independent federated network as a whole. We will consider every possible grant carefully before accepting it to ensure it doesn’t contradict the main principals of the project.

##### 2) Decision-making process
We follow a decision-making process based on consensus. Within the Disroot Core Team everyone has an equal voice and opinion. All important decisions are made during meetings at which proposals are presented and discussed extensively so everyone has time to elaborate their opinions or concerns before reaching mutual agreement.

##### 3) Community Inclusion
Disroot.org would not make any sense without the vibrant community that surrounds it, whose active participation in the project is very important and encouraged. We welcome any help, improvements, feedback and suggestions and we offer various ways of communication between members of the Disroot Community and the Core Team, such as: forums, various Taiga project boards or instant messaging rooms.
