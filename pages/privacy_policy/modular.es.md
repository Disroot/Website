---
title: Política de Privacidad
bgcolor: '#FFF'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _title
            - _pp
body_classes: modular
header_image: pp.jpg

translation_banner:
    set: true
    last_modified: Enero · 2024
    language: Español
---
