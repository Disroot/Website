---
title: Politique de confidentialité
bgcolor: '#FFF'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _title
            - _pp
body_classes: modular
header_image: pp.jpg

translation_banner:
    set: true
    last_modified: Février 2024
    language: French
---
