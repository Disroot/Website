---
title: Termini di utilizzo
bgcolor: '#1F5C60'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _title
            - _tos
body_classes: modular
header_image: havefun.jpg

translation_banner:
    set: true
    last_modified: Maggio 2024
    language: Italian
---
