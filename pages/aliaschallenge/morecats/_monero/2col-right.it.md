---
title: Monero
bgcolor: '#1F5C60'
fontcolor: '#FFF'
---

![](morecats-monero.png)

---

# Monero
Invia i tuoi monero al nostro portamonete:
8BXP6mKZioYjRJtwzpB1vQ6iMCXbyasMMa8VUbu6LbBa25KvAUn7fSteNyv5KsE1dhjPxBSoDo7rzVGMmfXYwntXBDQhNQr
