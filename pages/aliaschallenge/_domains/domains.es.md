---
title: Dominios
section_id: services
bgcolor: '#f2f2f2ff'
fontcolor: '#555'
text_align: left
services:
    -
        title: "#GlorifiedHacker"
        text: "glorifiedhacker.net"
        button1: 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=J36HJARW8FGGS'
        button2: 'aliaschallenge/glorifiedhacker'
    -
        title: "#GetGoogleOffMe"
        text: "getgoogleoff.me"
        button1: 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=ZE2GP4QZNZ7L6'
        button2: 'aliaschallenge/getgoogleoffme'
    -
        title: "#Morecats"
        text: "morecats.net"
        button1: 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=B5RR8H4Q7MKN6'
        button2: 'aliaschallenge/morecats'

---

<div style="width: 50%; margin-left: auto; margin-right: auto;">

<h4 style="color:#327E82;">Transferencia bancaria:</h4>
<p style="color:#327E82; font-size:1.8em; line-height:1.4"> Stichting Disroot.org <br>
IBAN NL19 TRIO 0338 7622 05 <br>
BIC TRIONL2U
</p>
<h4 style="color:#327E82;">¡No olvides mencionar tu elección en la referencia!</h4>
</div>
<br>
