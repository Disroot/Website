---
title: 'Introduction'
bgcolor: '#7b384fff'
fontcolor: '#FFF'
text_align: left
---

<br>

<span style="padding-right:10%;" id="fund1">
Nos últimos dias deste ano, quando o mundo inteiro se prepara para dizer olá a uma nova década, três domínios estão a lutar pelo controlo do mercado da beterraba. **junta-te a esta batalha e escolhe o teu senhor**.
</span>

<span style="padding-right:30%;" id="fund2">
De 23 de Dezembro até ao dia 1 de Janeiro, participa na nossa angariação de fundos. **Faz um donativo para o  Disroot** escolhendo um dos domínios em baixo como a referência do teu donativo. O domínio vencedor será **dado como um alias extra para todos os utilizadores do Disroot**.
</span>

## Que ganhe o Domínio mais fixe!
