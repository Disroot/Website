---
title: 'Annual reports'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---
<br>

#Ежегодные отчеты:

**[Annual Report 2023](/annual_reports/AnnualReport2023.pdf?target=_blank)**

**[Annual Report 2022](/annual_reports/AnnualReport2022.pdf?target=_blank)**

**[Annual Report 2020](/annual_reports/AnnualReport2020.pdf?target=_blank)**

**[Annual Report 2019](/annual_reports/AnnualReport2019.pdf?target=_blank)**

**[Annual Report 2018](/annual_reports/AnnualReport2018.pdf?target=_blank)**

**[Report Aug 2016 - Aug 2017](/annual_reports/2017.pdf?target=_blank)**

**2015 - Aug 2016**
Costs: €1569.76 / Donation: €264.52

---



---
<br>

# Пожертвования компьютерным железом
Если у вас есть какое-либо оборудование, которое может быть использовано для проекта (планки оперативной памяти, серверы, процессоры, жесткие диски, рейдовые/сетевые карты, сетевое оборудование и т.д.), пожалуйста, свяжитесь с нами. Приветствуется любое оборудование. То, что мы не можем использовать, мы можем продать, чтобы обналичить. Мы также с нетерпением ждем предложений по размещению в вашей серверной стойке.
