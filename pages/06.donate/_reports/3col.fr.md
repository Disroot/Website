---
title: 'Rapports annuels'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---
<br>

# Rapports annuels:

**[Rapport annuel 2023](/annual_reports/AnnualReport2023.pdf?target=_blank)**

**[Rapport annuel 2022](/annual_reports/AnnualReport2022.pdf?target=_blank)**

**[Rapport annuel 2020](/annual_reports/AnnualReport2020.pdf?target=_blank)**

**[Rapport annuel 2019](/annual_reports/AnnualReport2019.pdf?target=_blank)**

**[Rapport annuel 2018](/annual_reports/AnnualReport2018.pdf?target=_blank)**

**[Rapport Août 2016 - Août 2017](/annual_reports/2017.pdf?target=_blank)**

**2015 - Août 2016**
Coûts: €1569.76 / Dons: €264.52

---



---
<br>

# Dons de matériel informatique
Si vous avez du matériel qui pourrait être utilisé pour notre projet (Mémoire, serveurs, processeurs, disques durs, cartes réseaux/Raid, équipement réseau, etc.) contactez-nous. Tout matériel informatique est le bienvenu. Les choses qu'on ne peut pas utiliser peuvent être vendues pour gagner de l'argent.
Nous recherchons également des propositions d'espace rack.
