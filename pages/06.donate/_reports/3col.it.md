---
title: 'Annual reports'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---
<br>

#Report annuale:

**[Report annuale 2023](/annual_reports/AnnualReport2023.pdf?tariget=_blank)**

**[Report annuale 2022](/annual_reports/AnnualReport2022.pdf?target=_blank)**

**[Report annuale 2020](/annual_reports/AnnualReport2020.pdf?target=_blank)**

**[Report annulae 2019](/annual_reports/AnnualReport2019.pdf?target=_blank)**

**[Report annuale 2018](/annual_reports/AnnualReport2018.pdf?target=_blank)**

**[Report Agosto 2016 - Agosot 2017](/annual_reports/2017.pdf?target=_blank)**

**2015 - Agosto 2016**
Costi: €1569.76 / Donazioni: €264.52

---



---
<br>
# Donazioni hardware
Nel caso dovessi avere a disposzione delle componenti hardware utili al progetto (memorie, server, CPU, harddrive, raid/network card ecc.), contattaci. Ogni componente è la benvenuta. Le componenti che non possiamo utilizzare possiamo venderle per incassare denaro da destinare al progetto.
