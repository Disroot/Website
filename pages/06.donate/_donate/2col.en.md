---
title: Online-donation
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---


# "If you are not paying for the product, you are the product."

---

#### Online donation:

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Donate using Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Become a Patron" src="donate/_donate/p_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="PayPal"  src="donate/_donate/pp_button.png" /></a>

<a href="https://donate.stripe.com/4gweYZfyR6FS3w49AD" target=_blank><img alt="stripe" src="donate/_donate/s_button.png" /></a>

<a href="/cryptocurrency"><img alt="Cryptocurrency" src="donate/_donate/c_button.png" /></a>

</div>

#### Bank transfer:
<span style="color:#8EB726; font-size:1.8em;"> Stichting Disroot.org <br>
IBAN: NL19 TRIO 0338 7622 05<br>
BIC: TRIONL2U
</span>


#### Hardware donation: <span style="color:#8EB726;"> See below </span>
