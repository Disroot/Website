---
title: 'Blue bar'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## ¡Estamos agradecidos por sus donaciones y apoyo!

Con su ayuda, estamos acercándonos a nuestra meta para alcanzar la sustentabilidad financiera, y probamos que un modelo social de economía es posible.

Para agradecer a quienes donan al menos 10€, enviaremos un **paquete de stickers de Disroot**. No olviden dejar, en la referencia de sus donaciones, la dirección postal a la que debemos mandarlo.

![](stickers.jpg?resize=50%)