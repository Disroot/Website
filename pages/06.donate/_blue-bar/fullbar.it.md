---
title: 'Blue bar'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## Siamo riconoscenti per il supporto!

Con il tuo aiuto ci stiamo avvicinando al raggiungimento del nostro obiettivo di sostenibilità finanziaria. Stiamo dimostrando che un modello economico e sociale altro è possibile. 

Per ringraziarvi del supporto, a chi donerà almeno 10€, invieremo un **pacchetto di adesivi Disroot**. Non dimenticate di lasciare, nel riferimento della donazione, l'indirizzo postale per l'invio.

![](stickers.jpg?resize=50%)
