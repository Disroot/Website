---
title: Objetivos
bgcolor: '#FFF'
fontcolor: '#1F5C60'
wider_column: right
---

##  <span style="color:#8EB726;"> ¡No existe tal cosa como un servicio gratuito en la Internet! </span>

---

Ya sea la gente que da su tiempo para mantener el software o que paga por el hardware que lo ejecuta, la electricidad utilizada para alimentar las máquinas o el espacio de alojamiento en el centro de datos. Con las grandes compañías populares que dan servicio "gratuito", lo más probable es que el precio lo estés pagando con tus datos personales. Imagina el alcance de sus operaciones, la cantidad de personal y hardware necesarios para servir a miles de millones de usuarios y usuarias en todo el mundo. No son una organización benéfica, y sin embargo proporcionan un servicio "gratuito"...

Disroot.org es sostenido por personas voluntarias y ofrece todos los servicios sin cargo. No recopilamos información para venderla a empresas de publicidad o agencias gubernamentales. Por lo tanto, debemos depender de las donaciones y el apoyo de quienes usan los servicios y la comunidad. Si desean mantener el proyecto en marcha y crear espacio para más personas, pueden utilizar cualquiera de los métodos que se indican a continuación para hacer una contribución económica.

**¡Recuerden, cualquier cantidad de dinero cuenta! Si todos y todas las Disrooters nos compraran una taza de café al mes, podríamos hacer funcionar el servicio sin problemas financieros.**
