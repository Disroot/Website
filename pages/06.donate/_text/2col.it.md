---
title: Goals
bgcolor: '#FFF'
fontcolor: '#1F5C60'
wider_column: right
---

##  <span style="color:#8EB726;">Non esiste un servizio gratuito su internet</span>

---

Che si tratti delle persone che dedicano il loro tempo alla manutenzione e aggiornamento del software o del pagamento dell'hardware che lo esegue, dell'elettricità utilizzata per alimentare le macchine o dello spazio di hosting nel data center. Con le grandi aziende che offrono un servizio "gratuito", è molto probabile che siano i dati personali dell'utente a pagarne il prezzo. Immagina l'ambito delle loro operazioni, la quantità di personale e l'hardware necessari per servire milioni di utenti in tutto il mondo. Non sono enti di beneficenza e tuttavia forniscono un servizio "gratuito"...

Disroot.org è gestito da volontari e offre tutti i servizi liberamente. Non raccogliamo informazioni da vendere a società pubblicitarie o agenzie governative. Quindi dobbiamo fare affidamento sulle donazioni e sul sostegno degli utenti dei servizi e della comunità. Se desideri continuare il progetto e creare spazio per potenziali nuovi utenti, puoi utilizzare uno dei metodi seguenti per fornire un contributo finanziario.

**Ricorda, qualsiasi somma di denaro conta! Se tutti i Disrooters ci comprassero una tazza di caffè ogni mese, potremmo eseguire questo servizio senza difficoltà finanziarie.** 
