---
title: Overview
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---

## Monthly Overview

![](graph.png?lightbox=1024)


Supported by <button class="button button4">270</button> Disrooters
_(Click the number for an overview)_

![](graph_support.png?lightbox=1024&resize=40,40&class=transparent)

## FOSS Donations
![](donated.png?lightbox=1024&resize=70%)
