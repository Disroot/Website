---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Condividiamo la nostra fortuna'
        text: "Se riceviamo almeno 400 EUR in donazioni, condividiamo il 15% del nostro surplus con gli sviluppatori del software che stiamo utilizzando. Disroot non esisterebbe senza quegli sviluppatori."
        unlock: yes
    -
        title: 'Paga quattro quote ai volontari'
        text: "Se alla fine del mese ne rimangono più di 600, paghiamo a quattro membri di Disroot Core una quota di volontariato di 150 €."
        unlock: yes
    -
		title: 'Continua a pagare il nostro amministratore di sistema principale'
		text: "Per le innumerevoli ore di duro lavoro che mantengono in vita Disroot.<br/><br/> Siamo orgogliosi di poter dire che ora siamo in grado di pagare uno dei nostri amministratori di sistema, grazie ai fondi risparmiati negli anni precedenti. Per poter continuare a pagare gli stipendi l'anno prossimo, dovremo risparmiare di più quest'anno."

        unlock: no
---

<div class=goals markdown=1>

</div>

![](goals.png)

