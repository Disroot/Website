---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Compartiendo nuestra buena fortuna'
        text: "Si recibimos al menos 400 EUR en donaciones, compartimos el 15% de nuestro excedente con las y los desarrolladores del software que utilizamos. Disroot no existiría sin su trabajo."
        unlock: yes
    -
        title: 'Pagar cuatro Cuotas por Voluntariado'
        text: "Si nos quedan más de 600€ al final del mes, pagamos a cuatro miembros del Equipo Central de Disroot una cuota por voluntariado de 150€."
        unlock: yes
    -
        title: 'Continuar pagando a nuestro principal administrador de sistema' 
        text: "Por las incontables horas de trabajo duro manteniendo Disroot con vida.<br/><br/> Estamos orgullosos de poder pagar el sueldo a uno de nuestros administradores de sistemas con los fondos ahorrados en años anteriores. Para poder seguir pagando salarios el año que viene, tendremos que ahorrar más este año."
        unlock: no
---

<div class=goals markdown=1>

</div>

![](goals.png)

