---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Partager notre chance'
        text: "Si nous recevons au moins 400€ de dons, nous partageons 15% de notre excédent avec les développeurs des logiciels que nous utilisons. Disroot n'existerait pas sans ces développeurs."
        unlock: yes
    -
        title: 'Payer quatre volontaires'
        text: "S'il nous reste plus de 600€ à la fin du mois, nous versons à un membre de l'équipe Disroot Core une rémunération bénévole de 150€."
        unlock: yes
    -
        title: 'Continuer de payer notre administrateur système principal' 
        text: "Pour les innombrables heures de travail acharné qui permettent à Disroot de survivre.<br/><br/> Nous sommes fiers de dire que nous sommes maintenant en mesure de payer le salaire de l'un de nos administrateurs système, grâce aux fonds économisés au cours des années précédentes. Pour pouvoir continuer à payer les salaires l'année prochaine, nous devrons économiser davantage cette année."
        unlock: no
---

<div class=goals markdown=1>

</div>

![](goals.png)

