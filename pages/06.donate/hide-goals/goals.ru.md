---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Поделитесь нашей удачей'
        text: "Если мы получаем пожертвование на сумму не менее 400 евро, мы делим 15% излишка с разработчиками программного обеспечения, которое мы используем. Без этих разработчиков Disroot не существовал бы."
        unlock: yes
    -
        title: 'Сделайте четыре добровольных взноса'
        text: "Если в конце месяца у нас остается более 600 евро, мы платим четырем участникам Disroot Core гонорар волонтеров в размере 150 евро."
        unlock: yes
    -
        title: 'Continue Pay our main system administrator' 
        text: "For the countless hours of hard work keeping Disroot alive.<br/><br/> We are proud to say we are now able to pay wages to one of our system administrators, from funds saved in previous years. To be able to continue to pay wages next year, we will need to save up more this year."
        unlock: no
---

<div class=goals markdown=1>

</div>

![](goals.png)

