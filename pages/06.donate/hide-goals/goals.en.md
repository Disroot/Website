---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
hide-goals:
    -
        title: 'Share our Good fortune'
        text: "If we receive at least 400 EUR in donations, we share 15% of our surplus with the developers of the software we are using. Disroot would not exist without those developers."
        unlock: yes
    -
        title: 'Pay four Volunteers'
        text: "If we have more than 632€ left at the end of the month, we pay to four Disroot Core members a volunteer fee of 158€ a month."
        unlock: yes
    -
        title: 'Continue Pay our main system administrator' 
        text: "For the countless hours of hard work keeping Disroot alive.<br/><br/> We are proud to say we are now able to pay wages to one of our system administrators, from funds saved in previous years. To be able to continue to pay wages next year, we will need to save up more this year."
        unlock: no
---

<div class=goals markdown=1>

</div>

![](goals.png)
