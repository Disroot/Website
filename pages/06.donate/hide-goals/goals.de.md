---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Wir teilen die Spenden'
        text: "Wenn wir mindestens 400€ an Spenden erhalten, teilen wir 15 % unseres Überschusses mit den Entwicklern der Software, die wir nutzen. Disroot würde ohne diese Entwickler nicht existieren."
        unlock: yes
    -
        title: 'Eine Aufwands&shy;entschädigung für vier Helfer'
        text: "Wenn wir am Ende des Monats mehr als 600€ übrig haben, zahlen wir vier Mitgliedern des Disroot-Teams eine Aufwandsentschädigung von 150€."
        unlock: yes
    -
        title: 'Fortführung der Vergütungen für unseren Main-Admin' 
        text: "Für die unzähligen Stunden harter Arbeit die nötig sind, um Disroot am Leben zu erhalten.<br/><br/> Wir sind stolz, dass wir nun in der Lage sind, einem unserer System&shy;administratoren ein Entgelt zu zahlen, und zwar aus Mitteln, die wir in den vergangenen Jahren gespart haben. Um die Vergütungen auch im nächsten Jahr fortführen zu können, müssen wir in diesem Jahr noch mehr Geld sammeln."
        unlock: no
---

<div class=goals markdown=1>

</div>

![](goals.png)

