---
title: Tor
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://www.torproject.org/download/" target="_blank">Descarga el navegador Tor</a>

---

![](Tor_Logo.png)


## Acceso al sitio de Disroot a través de Tor

Pueden acceder al **sitio de Disroot** a través de Tor utilizando su navegador. Tor previene que cualquiera monitoreando la conexión sepa que están visitando el sitio de Disroot. 

Simplemente copien y peguen esta dirección .onion en el navegador Tor: **j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion**; si ya lo están utilizando, hagan clic [aquí](http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion)

Sitio del proyecto: [https://www.torproject.org/](https://www.torproject.org/)
