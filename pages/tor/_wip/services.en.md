---
title: WIP
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: "Smtp/Imap/Pop3"
        icon: email.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/email/'
        text: "Send, manage and downloads emails."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/566'
        buttontext: "Git issue"
    -
        title: Cloud
        icon: cloud.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/nextcloud/'
        text: "Your data under your control! Collaborate, sync & share files, calendars, contacts and more."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/569'
        buttontext: "Git issue"
    -
        title: XMPP Chat
        icon: xmpp.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/xmpp'
        text: "Decentralized instant messaging."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/567'
        buttontext: "Git issue"
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/cryptpad'
        text: "A private-by-design alternative to popular office tools."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/570'
        buttontext: "Git issue"
    -
        title: 'Akkoma'
        icon: akkoma.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/akkoma'
        text: "A microblogging tool that can federate with other servers that support ActivityPub."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/571'
        buttontext: "Git issue"
    -
        title: 'D-Scribe'
        icon: scribe.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/scribe'
        text: "A link aggregation and discussion platform that can federate with other servers that support ActivityPub."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/743'
        buttontext: "Git issue"

---
