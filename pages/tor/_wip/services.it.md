---
title: Services
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: "Smtp/Imap/Pop3"
        icon: email.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/email'
        text: "Account di posta libero e sicuro accessibile attraverso client o interfaccia web."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/566'
        buttontext: "Git issue"
    -
        title: Cloud
        icon: cloud.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/nextcloud/'
        text: "I tuoi dati sotto il tuo controllo. Collabora, sincronizza e condividi file, calendari e contatti."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/569'
        buttontext: "Git issue"
    -
        title: XMPP Chat
        icon: xmpp.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/xmpp'
        text: "Sistema di messaggistica decentralizzato."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/567'
        buttontext: "Git issue"
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/cryptpad'
        text: "Un'alternativa personalizzata ai popolari strumenti di ufficio."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/570'
        buttontext: "Git issue"
    -
        title: 'Akkoma'
        icon: akkoma.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/akkoma'
        text: "A microblogging tool that can federate with other servers that support ActivityPub."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/571'
        buttontext: "Git issue"
	-
        title: ''D-Scribe'
        icon: scribe.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/scribe'
        text: "Una piattaforma di aggregazione e discussione di link che può essere federata con altri server che supportano ActivityPub."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/743'
        buttontext: "Git issue"


---
