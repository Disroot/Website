---
title: Servicios
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: "Smtp/Imap/Pop3"
        icon: email.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/email'
        text: "Cuentas de correo seguras y gratuitas para tu cliente IMAP de escritorio o via web."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/566'
        buttontext: "Incidencia en Git"
    -
        title: Nube
        icon: cloud.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/nextcloud/'
        text: "¡Tus datos bajo tu control! Colabora, sincroniza y comparte archivos, calendarios, contactos y más."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/569'
        buttontext: "Incidencia en Git"
    -
        title: Chat XMPP
        icon: xmpp.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/xmpp'
        text: "Mensajería instantánea descentralizada."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/567'
        buttontext: "Incidencia en Git"
    -
        title: 'CryptPad'
        icon: cryptpad.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/cryptpad'
        text: "Herramientas ofimáticas privadas-por-defecto."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/570'
        buttontext: "Incidencia en Git"
    -
        title: 'Akkoma'
        icon: akkoma.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/akkoma'
        text: "Herramienta de microblogging que puede federar con otros servidores que soporten ActivityPub."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/571'
        buttontext: "Incidencia en Git"

---
