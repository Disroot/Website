---
title: Tor
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: Webmail
        icon: email.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/email'
        text: "Free and secure email accounts for your desktop IMAP client or via a Web-interface."
        button: 'http://mdlwkwn2nhzzuymmyr5mjyoqppoyp46k4remaliu2zrmkayl5yfeh7ad.onion'
        buttontext: "Log in"
    -
        title: Pad
        icon: pads.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/pads'
        text: "Create and edit documents collaboratively in real-time directly in the web browser."
        button: 'http://b6mttjczryfoyz2go65hyjl5k6xfqqacpvz3ameurvraijxg5sv2z2id.onion'
        buttontext: "Start a pad"
    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/privatebin'
        text: "Encrypted online paste-bin/discussion board."
        button: 'http://n63ite5off46lfh7qei4uhkvttrgvpve7ag3kwftlqkxo4o5mu7l4cqd.onion'
        buttontext: "Share a pastebin"
    -
        title: Upload
        icon: upload.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/upload'
        text: "Encrypted, temporary file hosting and sharing software."
        button: 'http://e2olmnzdp5d72z3xs2ugftvwgxywgbgipofa443zizolbgxoj5m46vyd.onion'
        buttontext: "Share a file"
    -
        title: Search
        icon: search.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/search'
        text: "An anonymous multi search engine platform."
        button: 'http://bzg6fq2cbzrp52z5xkmggsiqhfc4zb4ouq3g7y6b2yfdnuud6yajpyqd.onion'
        buttontext: "Search"
    -
        title: 'Git'
        icon: git.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/git'
        text: "A code hosting and project collaboration."
        button: 'http://kgtz2pmmov5jfvn3z4mqryffjnnw6krzrgxxoyaqhqckjrr4pckyhsqd.onion'
        buttontext: "Log in"
    -
        title: 'Libre Translate'
        icon: translate.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/libretranslate'
        text: "An online translation service."
        button: 'http://3veapfc2xpg4ouflejs2r2y3z6nkrylqkx25ruxtqpznxeyfycvhhrqd.onion'
        buttontext: "Translate"
---
