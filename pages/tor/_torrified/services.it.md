---
title: Tor
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: Webmail
        icon: email.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/email'
        text: "Account di posta libero e sicuro accessibile attraverso client o interfaccia web."
        button: 'http://mdlwkwn2nhzzuymmyr5mjyoqppoyp46k4remaliu2zrmkayl5yfeh7ad.onion'
        buttontext: "Accedi"
    -
        title: Pads
        icon: pads.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/pads'
        text: "Crea e modifica documenti direttamente nel browser web."
        button: 'http://b6mttjczryfoyz2go65hyjl5k6xfqqacpvz3ameurvraijxg5sv2z2id.onion'
        buttontext: "Iniziare"
    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/privatebin'
        text: "Paste Bin crittografata."
        button: 'http://n63ite5off46lfh7qei4uhkvttrgvpve7ag3kwftlqkxo4o5mu7l4cqd.onion'
        buttontext: "Iniziare"
    -
        title: Upload
        icon: upload.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/upload'
        text: "Software di condivisione per file temporanei crittografato ."
        button: 'http://e2olmnzdp5d72z3xs2ugftvwgxywgbgipofa443zizolbgxoj5m46vyd.onion'
        buttontext: "Condividere"
    -
        title: Search
        icon: search.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/search'
        text: "Un metamotore di ricerca anonimo."
        button: 'http://bzg6fq2cbzrp52z5xkmggsiqhfc4zb4ouq3g7y6b2yfdnuud6yajpyqd.onion'
        buttontext: "Ricerca"
    -
        title: 'Git'
        icon: git.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/git'
        text: "Un hosting per codice e gestione di progetti."
        button: 'http://kgtz2pmmov5jfvn3z4mqryffjnnw6krzrgxxoyaqhqckjrr4pckyhsqd.onion'
        buttontext: "Entra"
    -
        title: 'Libre Translate'
        icon: translate.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/libretranslate'
        text: "Una applicazione di traduzione."
        button: 'http://3veapfc2xpg4ouflejs2r2y3z6nkrylqkx25ruxtqpznxeyfycvhhrqd.onion'
        buttontext: "Traduire"
---
