---
title: XMPP
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: left
---

# XMPP
### Decentralized and Federated Instant Messaging.

Communicate using an open and federated chat protocol, with the ability to encrypt your communication. With XMPP you are not bound to one service provider (e.g. Disroot server) but can freely communicate with contacts from other Xmpp/Jabber [servers](https://compliance.conversations.im/), just like you would communicate between different email servers.

<a class="button button2" href="https://webchat.disroot.org">Chat</a>

---

<br>
![xmpp_conversations_client](conversations.png?lightbox=1024)

