---
title: Disroot Chats
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _chat-intro
            - _xmpp
            - _nextcloud-talk
            - _calls
            - _mumble
            - _delta-chat
body_classes: modular
header_image: 'none'
---
