---
title: 'Talk'
bgcolor: '#FFF'
fontcolor: '#1F5C60'
text_align: left
wider_column: right
---

<br><br>
![](en/call-in-action.png?lightbox=1024)

---

# Nextcloud Talk
### Audio/video conference calls. 

Securely communicate with your friends and family using rich audio and video from your browser. All calls are peer 2 peer.

You can create a public link that you can share with people that don't have a **Nextcloud** account.

<a class="button button2" href="https://cloud.disroot.org/apps/spreed/">Talk</a>
