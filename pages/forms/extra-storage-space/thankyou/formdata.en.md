---
title: 'Extra storage requested'
cache_enable: false
process:
    twig: true
---

<br><br> **We have received your request. <br><br>You will get a confirmation per E-mail with you billing reference. <br> Once we receive your payment we will assign you the extra storage. <br><br> Thank you for supporting Disroot!**
<br>
<hr>
<br>
**Here is a summary of the received request:**
