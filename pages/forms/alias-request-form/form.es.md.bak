---
title: 'Formulario de Solicitud de Alias'
header_image: introverts.jpg
form:
    name: Alias-request-form
    fields:
        -
            name: username
            label: 'Nombre de usuario'
            placeholder: 'Ingresa tu nombre de usuario de Disroot'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 1'
            label: Alias
            placeholder: tu alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 2'
            label: 'Segundo Alias (opcional)'
            placeholder: tu alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 3'
            label: 'Tercer Alias (opcional)'
            placeholder: tu alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 4'
            label: 'Cuarto Alias (opcional)'
            placeholder: tu alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 5'
            label: 'Quinto Alias (opcional)'
            placeholder: tu alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: contribution
            label: 'Contribuir a través de'
            placeholder: select
            type: select
            options:
                patreon: Patreon
                paypal: Paypal
                stripe: 'Stripe (creditcard)'
                bank: 'Transferencia bancaria'
                faircoin: Faircoin
                bitcoin: Bitcoin

            validate:
                required: true
        -
            name: amount
            label: Cantidad
            placeholder: 'EUR/USD/BTC/etc.'
            type: text
            validate:
                pattern: '[A-Za-z0-9., ]*'
                required: true
        -
            name: frequency
            label: Frecuencia
            type: radio
            default: monthly
            options:
                monthly: Mensual
                yearly: Anual
            validate:
                required: true
        -
            name: reference
            label: Referencia
            type: text
            placeholder: 'Nombre del titular del pago u otras referencias de la transferencia'
            validate:
                required: true
        -
            name: comments
            type: textarea
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Enviar
        -
            type: reset
            value: Reiniciar
    process:
        -
            email:
                from: alias-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] Tu solicitud de Alias'
                body: '<br><br>Hola {{ form.value.username|e }}, <br><br><strong>¡Gracias por tu contribución a Disroot.org!</strong><br>Apreciamos sinceramente tu generosidad.<br><br>Revisaremos tu solicitud de alias y te responderemos tan pronto como podamos.</strong><br><br><hr><br><strong>Aquí está un resumen de la solicitud recibida:</strong><br><br>{% include ''forms/data.html.twig'' %}'
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Alias request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: '¡Tu solicitud de Alias ha sido enviada!'
        -
            display: Gracias
---

<h1 class="form-title"> Solicitud de Alias de Correo </h1>
<p class="form-text"><strong>Los Alias están disponibles para nuestros aportantes habituales.</strong> Con aportantes habituales nos referimos a aquellos que contribuyen con el equivalente a, por lo menos, una tasa de café al mes.
 <br><br>
No estamos promoviendo el café, al contrario - el café es un símbolo de <a href="http://thesourcefilm.com/">explotación</a> e <a href="http://www.foodispower.org/coffee/">inequidad</a>, pero pensamos que es una buena manera de dejar a la gente mensurar por sí mismos cuánto pueden dar.
Encontramos esta <a href="https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee">lista</a>  de precios de tazas de café alrededor del mundo, podría no ser muy precisa, pero da un buen indicio de los diferentes costos.
<br><br>
<strong>Por favor, tómate el tiempo para considerar tu contribución.</strong> Si puedes 'comprarnos' una taza de café al mes en Río De Janeiro eso está bien, pero si puedes permitirte pagar un Café Doble Descafeinado Con Crema extra al mes, entonces puedes ayurdarnos en serio a mantener la plataforma Disroot corriendo y asegurar que esté disponible de manera gratuita para otras personas con menos recursos.</p>
