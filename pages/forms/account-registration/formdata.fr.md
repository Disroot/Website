---
title: 'Account registration'
cache_enable: false
process:
    twig: true
---

<h2>Demande de compte</h2>

<br> **Merci d'avoir créé un compte sur Disroot. Nous examinerons votre demande et nous vous répondrons. Le temps d'attente ne dépasse généralement pas 48 heures, mais parfois, en raison d'obligations de vie privée ou d'autres priorités, cela peut prendre plus de temps. Si vous ne recevez pas de courriel de notre part dans quelques jours, veuillez nous contacter à support@disroot.org**
<br>
<br>
**Bien cordialement, <br>l'équipe Disroot.**
