---
title: 'Registrazione account'
cache_enable: false
process:
    twig: true
---

<h2> Richiesta account </h2>

<br> **Grazie per aver registrato un account su Disroot. Esamineremo la tua richiesta e ti risponderemo. Il tempo di attesa di solito non supera le 48 ore.**
<br>
<br>
**Cordiali saluti, <br>il team Disroot.**
