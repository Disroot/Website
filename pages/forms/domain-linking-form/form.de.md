---
title: 'Domain Linking Formular'
header_image: netneutrality.jpg
form:
    name: Domain-linking-form
    fields:
        -
           type: spacer
           title: 'Benutzername und zu verlinkende Domain:'
           text: "Gib Deinen Disroot Benutzernamen und die Domain an, die du mit unseren Servern verknüpfen möchtest."
           markdown: true

        -    
          name: username
          label: 'Benutzername'
          placeholder: 'Gib Deinen Disroot-Benutzernamen an'
          autofocus: 'on'
          autocomplete: 'on'
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: true
        -
          name: Domain
          label: 'Deine Domain'
          placeholder: example.com
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: true
        -
          name: catchall
          markdown: true
          type: checkbox
          label: '**Catchall mail** - ist eine Funktion für deine eigene Domain. Sie ermöglicht es dir, alle E-Mails zu empfangen, die an deine Domain adressiert sind, auch wenn sie nicht an deine E-Mail-Adresse gerichtet sind (anything@yourdomain.ltd).'
          validate:
            required: false
        -
          type: spacer
          title: 'Alias Informationen'
          markdown: true
          text: "Mit Hilfe von Aliasen kannst du mehrere Identitäten für deine E-Mail erstellen, die mit deiner eigenen Domain *(z. B. john@example.com oder nickname@example.com)* benutzt werden können. Denke daran, dass diese immer noch mit deinem Disroot-Benutzerkonto verbunden sind und du maximal 5 pro Account haben kannst. Wenn du weitere Aliase hinzufügen möchtest, um deine Domain auszunutzen, musst du uns per E-Mail kontaktieren oder die zusätzlichen Aliase, die du hinzufügen möchtest, im Kommentarfeld unten angeben. Alle zusätzlichen Aliase oder Änderungen müssen durch Kontakt mit dem Support angefragt werden, bis wir eine automatische Lösung anbieten können."

        -
          name: 'Alias 1'
          label: Alias
          placeholder: Dein Alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: true
        -
          name: 'Alias 2'
          label: 'Zweiter Alias (optional)'
          placeholder: Dein Alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          name: 'Alias 3'
          label: 'Dritter Alias (optional)'
          placeholder: Dein Alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          name: 'Alias 4'
          label: 'Vierter Alias (optional)'
          placeholder: Dein Alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          name: 'Alias 5'
          label: 'Fünfter Alias (optional)'
          placeholder: Dein Alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          type: spacer
          title: "Spenden Details"
          markdown: true
          text: "Bitte nenne uns nachfolgend die Informationen über die von dir getätigte Spende. Bitte stelle sicher, dass du bereits gespendet hast, bevor du die Domainverknüpfung beantragst. **Die Informationen werden gelöscht, sobald die Anfrage bearbeitet wurde.**"

        -
          name: contribution
          label: 'Spende über'
          placeholder: wähle
          type: select
          options:
            patreon: Patreon
            stripe: 'Stripe (Kreditkarte)'
            paypal: Paypal
            bank: 'Banküberweisung'
            bitcoin: Bitcoin
            monero: Monero
          validate:
            required: true

        -
          name: amount
          label: Betrag
          placeholder: 'EUR/USD/BTC/etc.'
          type: text
          validate:
            pattern: '[A-Za-z0-9., ]*'
            required: true

        -
          name: Kommentare
          type: textarea

        -
          name: honeypot
          type: honeypot
    buttons:
        -
            type: submit
            value: absenden
        -
            type: reset
            value: zurücksetzen
    process:
        -
            email:
                from: domain-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] Deine Domain Linking Anfrage'
                body: '<br><br>Hi {{ form.value.username|e }}, <br><br><strong>Vielen Dank für deine Unterstützung von Disroot.org!</strong><br>Wir wissen das wirklich sehr zu schätzen.<br><br>Wir werden deine Anfrage prüfen und uns so schnell wie möglich bei dir melden.</strong><br><br><hr><br><strong>Hier ist eine Zusammenfassung der erhaltenen Anfrage:</strong><br><br>{% include ''forms/data.html.twig'' %}'
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Domain linking request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Deine Domain Linking Anfrage wurde gesendet!'
        -
            display: Danke
---

<h1 class="form-title"> Domain Linking Anfrage </h1>
<p class="form-text"><strong> Fülle dieses Formular aus, wenn du deine eigene Domain für E-Mail-Aliase und/oder XMPP-Chat verwenden möchtest</strong>
<br><br>
Stelle sicher, dass du alle notwendigen Schritte befolgt hast, bevor du deine Domain beantragst, um weder deine noch unsere Zeit zu verschwenden.
Wir prüfen alle eingehenden Anfragen einmal pro Woche, habe also bitte etwas Geduld. Wir werden uns mit dir in Verbindung setzen, sobald wir deine Anfrage bearbeitet haben.  
</p>
<br><br><br><br>
