---
title: Online-donation
bgcolor: '#fff'
wider_column: left
fontcolor: '#1e5a5e'
---

![](premium.png?class=reward) **Disroot** полагается на пожертвования и поддержку сообщества. Если вы хотите, чтобы проект продолжал существование и развитие, помогая новым пользователям находить уголок цифровой свободы, пожалуйста, используйте любой из доступных методов доната для внесения материального вклада в наше дело.
Для людей, которые регулярно поддерживают нас, возможна [привязка доменов](/services/email#alias).

Вы также можете поддержать нас, [расширив почтовое хранилище](/services/email#storage) и/или [увеличив дисковое пространство](/services/nextcloud#storage) вашего облачного хранилища.

---

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Donate using Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Become a Patron" src="donate/_donate/p_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="https://disroot.org/cryptocurrency"><img alt="Cryptocurrency" src="donate/_donate/c_button.png" /></a>

</div>

<span style="color:#4e1e2d; font-size:1.1em; font-weight:bold;">Банковский перевод:</span>
<span style="color:#4e1e2d; font-size:1.1em;">
Stichting Disroot.org
IBAN NL19 TRIO 0338 7622 05
BIC TRIONL2U
</span>
