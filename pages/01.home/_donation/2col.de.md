---
title: Online-Spende
bgcolor: '#fff'
wider_column: left
fontcolor: '#1e5a5e'
---

![](premium.png?class=reward) **Disroot** ist angewiesen auf Spenden und Unterstützung seiner Community und der Nutzer der angebotenen Dienste. Wenn Du gerne das Projekt am Leben erhalten und dabei helfen möchtest, Platz für potentielle neue Disrooter zu schaffen, nutze bitte eine der verfügbaren Möglichkeiten, um einen finanziellen Beitrag zu leisten. Die [Domain-Verknüpfung](/services/email#alias) ist verfügbar für regelmäßige Unterstützer.

Du kannst uns auch unterstützen, indem Du [zusätzlichen E&#8209;Mailspeicher](/services/email#storage) und/oder [zusätzlichen Cloudspeicher](/services/nextcloud#storage) kaufst.

---

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Spenden via Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Ein Patron werden" src="donate/_donate/p_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=de&hosted_button_id=AW6EU7E9NN3VQ&source=url" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="https://disroot.org/cryptocurrency"><img alt="Kryptowährung" src="donate/_donate/c_button.png" /></a>

</div>

<span style="color:#4e1e2d; font-size:1.1em; font-weight:bold;">Banküberweisung:</span>
<span style="color:#4e1e2d; font-size:1.1em;">
Stichting Disroot.org
IBAN NL19 TRIO 0338 7622 05
BIC TRIONL2U
</span>
