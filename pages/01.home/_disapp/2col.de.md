---
title: 'Disroot App'
bgcolor: '#1F5C60'
fontcolor: '#fff'
text_align: left
wider_column: left
---

<br>

## Get it on  [![F-droid](F-Droid.svg.png?resize=80,80&class=imgcenter)](https://f-droid.org/en/packages/org.disroot.disrootapp/) [F-droid](https://f-droid.org/en/packages/org.disroot.disrootapp/?class=lighter)

---

## Disroot App
Diese App ist wie ein Schweizer Taschenmesser für die **Disroot**-Plattform, gemacht von der Community für die Community. Auch wenn Du kein **Disroot**-Konto hast, kannst Du mit dieser App trotzdem auf alle **Disroot**-Dienste zugreifen, für die kein Konto erforderlich ist. Das sind beispielsweise Etherpad, Upload und andere.
