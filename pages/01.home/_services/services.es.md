---
title: Servicios
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: Correo
        icon: email.png
        link: 'https://disroot.org/services/email'
        text: "Cuentas de correo seguras y gratuitas para tu cliente IMAP de escritorio o via web."
        button: 'https://webmail.disroot.org'
        buttontext: "Acceder"
    -
        title: Nube
        icon: cloud.png
        link: 'https://disroot.org/services/nextcloud/'
        text: "¡Tus datos bajo tu control! Colabora, sincroniza y comparte archivos, calendarios, contactos y más."
        button: 'https://cloud.disroot.org'
        buttontext: "Acceder"
    -
        title: Chat XMPP
        icon: xmpp.png
        link: 'https://disroot.org/services/xmpp'
        text: "Mensajería instantánea descentralizada."
        button: 'https://webchat.disroot.org'
        buttontext: "Acceder"
    -
        title: Blocs
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Crea y edita documentos colaborativamente y en tiempo real directamente en tu navegador."
        button: 'https://pad.disroot.org'
        buttontext: "Abrir un bloc"

    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Paste-bin/tablero de discusión cifrado en línea."
        button: 'https://bin.disroot.org'
        buttontext: "Compartir un pastebin"
        badge: E2EE

    -
        title: Subida
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Alojamiento temporario cifrado."
        button: 'https://upload.disroot.org'
        buttontext: "Compartir un archivo"
        badge: E2EE

    -
        title: Búsqueda
        icon: search.png
        link: 'https://disroot.org/services/search'
        text: "Plataforma multimotor de búsqueda anónima."
        button: 'https://search.disroot.org'
        buttontext: "Buscar"

    -
        title: 'Llamadas'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Herramienta de videoconferencia."
        button: 'https://calls.disroot.org'
        buttontext: "Llamar"
    -
        title: 'Git'
        icon: git.png
        link: 'https://disroot.org/services/git'
        text: "Hospedaje de código y proyectos colaborativos."
        button: 'https://git.disroot.org'
        buttontext: "Acceder"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "Herramienta de audio chat."
        button: 'https://disroot.org/services/audio'
        buttontext: "Hablar"
    -
        title: 'CryptPad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "Herramientas ofimáticas privadas-por-defecto."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Acceder"
        badge: E2EE
    -
        title: 'Akkoma'
        icon: akkoma.png
        link: 'https://disroot.org/services/akkoma'
        text: "Herramienta de microblogging que puede federar con otros servidores que soporten ActivityPub."
        button: 'https://fe.disroot.org'
        buttontext: "Iniciar sesión"

    -
        title: 'Traductor'
        icon: translate.png
        link: 'https://disroot.org/services/libretranslate'
        text: "Aplicación en línea para traducir."
        button: 'https://translate.disroot.org'
        buttontext: "Traducir"
    -
        title: 'D·Scribe'
        icon: scribe.png
        link: 'https://disroot.org/services/scribe'
        text: "Agregador de links y plataforma de discusión que puede federar con otros servidores que soporten ActivityPub."
        button: 'https://scribe.disroot.org'
        buttontext: "Acceder"
---
