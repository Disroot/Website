---
title: Services
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: Email
        icon: email.png
        link: 'https://disroot.org/services/email'
        text: "Comptes de messagerie gratuits et sécurisés pour votre client IMAP de bureau ou via une interface Web."
        button: 'https://webmail.disroot.org'
        buttontext: "Se connecter"
    -
        title: Cloud
        icon: cloud.png
        link: 'https://disroot.org/services/nextcloud/'
        text: "Vos données sous votre contrôle ! Collaborez, synchronisez et partagez des fichiers, des calendriers, etc."
        button: 'https://cloud.disroot.org'
        buttontext: "Se connecter"

    -
        title: Chat XMPP
        icon: xmpp.png
        link: 'https://disroot.org/services/xmpp'
        text: "Messagerie instantanée décentralisée."
        button: 'https://webchat.disroot.org'
        buttontext: "Se connecter"
    -
        title: Pads
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Créez et modifiez des documents en collaboration et en temps réel."
        button: 'https://pad.disroot.org'
        buttontext: "Commencer"

    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Paste-bin en ligne chiffré / forum de discussion."
        button: 'https://bin.disroot.org'
        buttontext: "Partager"
        badge: E2EE
    -
        title: Upload
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Logiciel d'hébergement et de partage de fichiers temporaires et chiffrés."
        button: 'https://upload.disroot.org'
        buttontext: "Partager"
        badge: E2EE
    -
        title: Search
        icon: search.png
        link: 'https://disroot.org/services/search'
        text: "Une plate-forme anonyme multi moteurs de recherche."
        button: 'https://search.disroot.org'
        buttontext: "Rechercher"

    -
        title: 'Calls'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Un outil de vidéo conférence."
        button: 'https://calls.disroot.org'
        buttontext: "Appeler"
    -
        title: 'Git'
        icon: git.png
        link: 'https://disroot.org/services/git'
        text: "Un hébergement de code et une collaboration de projet."
        button: 'https://git.disroot.org'
        buttontext: "Se connecter"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "Une application de chat vocal à faible latence et de haute qualité."
        button: 'https://disroot.org/services/audio'
        buttontext: "Info"
    -
        title: 'CryptPad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "Une alternative privée aux outils de bureautique populaires."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Accès"
        badge: E2EE
    -
        title: 'Akkoma'
        icon: akkoma.png
        link: 'https://disroot.org/services/akkoma'
        text: "Un outil de microblogging qui peut fédérer avec d'autres serveurs qui supportent ActivityPub."
        button: 'https://fe.disroot.org'
        buttontext: "Se connecter"

    -
        title: 'Translate'
        icon: translate.png
        link: 'https://disroot.org/services/libretranslate'
        text: "Une application de traduction."
        button: 'https://translate.disroot.org'
        buttontext: "Accès"

    -
        title: 'D·Scribe'
        icon: scribe.png
        link: 'https://disroot.org/services/scribe'
        text: "Une plateforme d'agrégation de liens et de discussion qui peut se fédérer avec d'autres serveurs qui supportent ActivityPub."
        button: 'https://scribe.disroot.org'
        buttontext: "Accès"
---
