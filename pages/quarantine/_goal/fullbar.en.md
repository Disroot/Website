---
title: 'Goal'
bgcolor: '#09846aff'
fontcolor: '#FFFFFF'
text_align: left
---

<br>


# Quick guide:
1. To get you started, install Teeworlds either using your package manager if you are using a proper operating system like linux/bsd or go to teeworlds [website](https://teeworlds.com/?page=downloads) and download it for your platform.
2. Once you're done with that, start the game.
3. Fisrt select "Setup" to add your players name, choose and customize your Tee is you like.
4. Hit play and find **"Quarantine with Disroot"** and join us.
![](splashtee3.png)
