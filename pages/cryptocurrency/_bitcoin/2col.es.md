---
title: Bitcoin
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

![](qr1224.png)

---

# Bitcoin
Envía [bitcoins](https://es.wikipedia.org/wiki/Bitcoin) a nuestra cartera a:  
bc1q87hgd20lqrj38mvq03zugwzzfmymct7dtsm65l
