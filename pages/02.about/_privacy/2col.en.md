---
title: privacy
fontcolor: '#555'
wider_column: left
bgcolor: '#fff'

---

# Privacy

Software can be created and shaped into anything. Every button, every color and every link that we see and use on the web was put there by someone. When we use the applications provided for us, we mostly don't see - and sometimes don't care about - much of what happens behind the interface we use. We connect with other people, we store our files, organize meetings and festivals, send emails or chat for hours and it all happens magically.
In the last few decades information has become very valuable and more and more easy to collect and process. We are accustomed to being analyzed, blindly accepting terms and conditions for "our own good", trusting authorities and multi-billion dollar companies to protect our interest, while all along we are the product in their 'people farms'.

**Own your own data:**
Many networks use your data to make money by analyzing your interactions and using this information to advertise things to you. **Disroot** doesn't use your data for any purpose other than allowing you to connect and use the service.
Your files on the cloud are encrypted with your user password, every bin-paste and file uploaded on **Lufi** service is client side encrypted too, meaning that even server administrators have no access to your data. Whenever there is a possibility for encryption, we enable it and if it's not possible, we advice to use external encryption software. The less we, as admins, know about your data, the better :D.(_**Tip of the day**: Since your files on **Nextcloud** are encrypted with your user's password, if you forget it you will not be able to access your files again, so **make sure to NEVER lose your password!** (We suggest using a password manager)_) [Read this tutorial](https://howto.disroot.org/en/tutorials/user/account/administration/ussc)

---

<br>
![](priv1.jpg?lightbox=1024)
![](priv2.jpg?lightbox=1024)
