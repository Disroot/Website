---
title: 'Green bar'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: center
---


## Disroot Team

**Antilopa**, **Muppeth**, **Fede**, **Meaz**, **Avg_Joe**.

![antilopa](antilopa.png "Antilopa") ![muppeth](muppeth.png "Muppeth") ![fede](fede.png "Fede") ![meaz](meaz.png "Meaz") ![avg_joe](avg_joe.png "Avg_Joe")

## Übersetzungs-Team
Die italienische Übersetzung stammt von **@l3o**, Französisch, Spanisch und Deutsch werden vom **Kernteam** betreut.
*Wenn du daran interessiert bist, uns bei dieser Arbeit zu helfen, kontaktiere uns [hier](https://git.disroot.org/Disroot/Disroot-Project/issues/148)*
