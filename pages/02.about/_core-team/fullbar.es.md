---
title: 'Green bar'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: center
---


## Equipo de Disroot

**Antilopa**, **Muppeth**, **Fede**, **Meaz**, **Avg_Joe**.

![antilopa](antilopa.png "Antilopa") ![muppeth](muppeth.png "Muppeth") ![fede](fede.png "Fede") ![meaz](meaz.png "Meaz") ![avg_joe](avg_joe.png "Avg_Joe")

## Equipo de traducciones
La traducción italiana es obra de **@l3o**, las traducciones al francés, español y alemán están a cargo del **Equipo Principal**.
*Si tienen interés por ayudarnos con esta tarea, contáctennos [aquí](https://git.disroot.org/Disroot/Disroot-Project/issues/148)*
