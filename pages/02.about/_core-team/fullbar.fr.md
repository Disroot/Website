---
title: 'Green bar'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: center
---


## Equipe Disroot

**Antilopa**, **Muppeth**, **Fede**, **Meaz**, **Avg_Joe**.

![antilopa](antilopa.png "Antilopa") ![muppeth](muppeth.png "Muppeth") ![fede](fede.png "Fede") ![meaz](meaz.png "Meaz") ![avg_joe](avg_joe.png "Avg_Joe")


## Equipe de traduction
La traduction italienne est le travail de **@l3o**, le français, l'espagnol et l'allemand sont pris en charge par l'équipe **Core**.
*Si vous souhaitez nous aider dans ce travail, contactez-nous [ici](https://git.disroot.org/Disroot/Disroot-Project/issues/148)*.
