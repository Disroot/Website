---
title: 'Green bar'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: center
---


## Команда Disroot 

**Antilopa**, **Muppeth**, **Fede**, **Meaz**, **Avg_Joe**.

![antilopa](antilopa.png "Antilopa") ![muppeth](muppeth.png "Muppeth") ![fede](fede.png "Fede") ![meaz](meaz.png "Meaz") ![avg_joe](avg_joe.png "Avg_Joe")


## Команда переводчиков
Перевод на итальянский язык выполняет **@l3o**. Французским, испанским и немецким языками занимается **Основная команда**.
*Если вы заинтересованны в помощи нам в этой работе, свяжитесь с нами [здесь](https://git.disroot.org/Disroot/Disroot-Project/issues/148)*
