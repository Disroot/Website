---
title: 'Nosotros'
bgcolor: '#1F5C60'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _about-who-and-why
            - _core-team
            - _federation
            - _privacy
            - _transparency
            - _greenenergy
body_classes: modular
header_image: about-banner.jpeg

translation_banner:
    set: true
    last_modified: Enero · 2024
    language: Español
---
