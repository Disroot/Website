---
title: Online-donation
bgcolor: '#1F5C60'
wider_column: left
fontcolor: '#ffffff'
---

I server di Disroot sono hostati in un data-center che usa energia rinnovabile.

---

<a href="https://api.thegreenwebfoundation.org/greencheckimage/disroot.org?nocache=true" target=_blank><img src="https://api.thegreenwebfoundation.org/greencheckimage/disroot.org?nocache=true" alt="Questo sito usa energia rinnovabile - verificato da thegreenwebfoundation.org" style="height:100px"></a>
