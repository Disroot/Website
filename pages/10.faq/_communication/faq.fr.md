---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "COMMUNICATION"
section_number: "100"
faq:
    -
        question: "Comment utiliser l'IRC de Disroot dans les salles XMPP ?"
        answer: "<p><b>Disroot</b> fournit ce qu'on appelle une passerelle IRC. Cela signifie que vous pouvez rejoindre n'importe quelle salle IRC hébergée sur n'importe quel serveur IRC. Il s'agit du schéma d'adresse de base :</p>
        <ol>
        <li>Pour rejoindre une salle IRC: <b>#salle%irc.domain.tld@irc.disroot.org</b></li>
        <li>Pour ajouter un contact IRC: <b>nomducontact%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Où <b>#salle</b> est la salle IRC que vous voulez rejoindre et <b>irc.domain.tld</b> est l'adresse du serveur irc que vous voulez rejoindre. Assurez-vous de laisser <b>@irc.disroot.org</b> tel quel car c'est l'adresse des passerelles IRC.</p>"

---
