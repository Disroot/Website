---
title: 'Stockage supplémentaire'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Plus de stockage pour les Emails, le Cloud et/ou Cryptpad

![email](logo_email.png?resize=100) ![cloud](logo_cloud.png?resize=100) ![cryptpad](logo_cryptpad.png?resize=100)

---      

<br>
Avec votre compte **Disroot**, vous bénéficiez d'un stockage GRATUIT : 1GB pour vos emails, 2GB pour le cloud, 500Mo pour Cryptpad. Cependant, nous vous offrons la possibilité de l'étendre.

Voici les prix **par an, frais de paiement inclus** :

||||
|---:|---|---:|
| 5Go |......| 11€ |
| 10Go |......| 20€ |
| 15Go |......| 29€ |
| 30Go |......| 56€ |
| 45Go |......| 83€ |
| 60Go |......| 110€ |


<br>
Les transactions au sein de l'UE sont soumises à une TVA (taxe sur la valeur ajoutée) supplémentaire de 21 %.

Vous pouvez décider de répartir ce stockage supplémentaire comme vous le souhaitez entre le courrier et le cloud. Par exemple, si vous bénéficiez d'un espace de stockage de 10 Go, vous pouvez décider de consacrer 8 Go au cloud et 2 Go pour les emails.

<a class="button button1" href="/forms/extra-storage-space">Demande de stockage supplémentaire</a>
