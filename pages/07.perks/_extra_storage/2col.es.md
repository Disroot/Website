---
title: 'Almacenamiento Extra'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Más espacio para el correo, la nube y/o Cryptpad

![email](logo_email.png?resize=100) ![cloud](logo_cloud.png?resize=100) ![cryptpad](logo_cryptpad.png?resize=100)

---      

<br>
Con la cuenta de **Disroot**, se obtiene almacenamiento GRATUITO: 1GB para los correos, 2GB para la nube, 500MB para Cryptpad. Sin embargo, ofrecemos la posibilidad de ampliarlo.

Estos son los precios **por año, tasas de pagos incluidas**:

||||
|---:|---|---:|
| 5GB |......| 11€ |
| 10GB |......| 20€ |
| 15GB |......| 29€ |
| 30GB |......| 56€ |
| 45GB |......| 83€ |
| 60GB |......| 110€ |


<br>
Las transacciones dentro de la UE están sujetas a un I.V.A (Impuesto al Valor Agregado) adicional de 21%.

Es posible distribuir este almacenamiento adicional como se desee entre el correo y la nube. Por ejemplo, alguien obtiene 10GB de almacenamiento y puede repartir 8GB para la nube y 2GB para el correo.

<a class="button button1" href="/forms/extra-storage-space">Solicitar almacenamiento extra</a>
