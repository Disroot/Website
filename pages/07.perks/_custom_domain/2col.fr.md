---
title: 'Domaine personnalisé'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Domaine personnalisé

---      

<br>
**Disroot** offre une option de domaine personnalisé pour le courrier électronique. Cela signifie que vous pouvez utiliser les services de **Disroot** avec votre propre nom de domaine privé pour envoyer et recevoir des e-mails sous un domaine comme `nom@votredomaine.net`, par exemple.

Cette fonctionnalité est disponible pour toute personne qui décide de donner l'équivalent, au moins, de 12 cafés (ou autre boisson de votre choix) une fois ou annuellement (à vous de décider).

**✨ Le lien de domaine personnalisé est à vie ! ✨**
Aussi longtemps que **Disroot** sera en vie...

<p>
