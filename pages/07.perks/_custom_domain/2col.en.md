---
title: 'Custom domain'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Custom domain

---      

<br>
**Disroot** offers a custom domain feature for email. This means you can use **Disroot** services with your own private domain name to send and receive emails under a domain like `name@yourdomain.net`, for example.

This feature is available to anyone who decides to donate the equivalent of, at least, 12 coffees (or other beverage of your choice) one time or annually (you decide).

**✨ Custom domain linking is for life! ✨**
As long as **Disroot** is alive...
<p>
