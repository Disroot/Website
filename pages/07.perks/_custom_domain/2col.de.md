---
title: 'Benutzerdefinierte Domain'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Benutzerdefinierte Domain

---      

<br>
**Disroot** bietet Dir die Nutzung einer benutzerdefinierten Domain für E&#8209;Mails. Das bedeutet, dass Du diese Disroot-Anwendungen mit Deinem eigenen, privaten Domainnamen nutzen kannst, um mit einer Domain wie zum Beispiel `name@deinedomain.net` E&#8209;Mails zu senden und zu erhalten.

Diese Funktion steht jedem zur Verfügung, der sich entscheidet, den Gegenwert von zumindest 12 Kaffee (oder eines anderen Getränks Deiner Wahl) zu spenden, einmalig oder jährlich (Du entscheidest).

**✨ Die Verknüpfung der eigenen Domain ist lebenslang! ✨**
Solange **Disroot** am Leben ist...
<p>
