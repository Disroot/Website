---
title: 'Paso 1'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Paso 1:
## Consigue un dominio



---      

<br>
Si todavía no lo tienes, necesitas uno primero. Por el momento, **Disroot** no ofrece una opción para comprar dominios. Sin embargo, hay muchos proveedores en la vuelta, así que puedes buscar un poco e ir de compras. Te sugerimos que trates de elegir un registrador de dominios local, más ético y pequeño antes que ir a las grandes compañías.
Apoyemos nuestras economías locales.
