---
title: 'Etape 1'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Etape 1:
## Obtenir un domaine



---      

<br>
Si vous n'en avez pas encore, vous devez d'abord l'obtenir. Pour le moment, **Disroot** ne propose pas de fonction d'achat de domaine. Cependant, il existe de nombreux fournisseurs de domaines, vous pouvez donc effectuer des recherches et faire des achats. Nous vous suggérons d'essayer de choisir un petit fournisseur de noms de domaine local, éthique, au lieu d'aller chez les grandes entreprises.
Soutenez votre économie locale.
