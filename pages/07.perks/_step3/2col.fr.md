---
title: 'Etape 3'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Etape 3:
##  Faites un don à Disroot

![](../../01.home/_donation/premium.png?class=reward) **Disroot dépend de votre contribution financière**. C'est ce qui nous permet d'être totalement indépendants et ce qui paie les factures de l'hébergement de **Disroot**, tout en contribuant à l'objectif ultime de mettre de la nourriture sur notre table.


---

<br>
**La liaison de domaine personnalisée est une fonction à vie**. Il n'y a pas d'abonnement ni de prix attaché à cette fonction. **C'est vous qui décidez de l'importance que vous accordez à notre travail et à notre service** et du montant que vous êtes prêt à donner pour cette fonctionnalité.

**À titre indicatif, nous proposons l'équivalent de 12 cafés par an** (inviter vos admins pour une tasse de votre café préféré dans votre cafétéria préférée est quelque chose qui devrait être un droit humain 😋 ). Quel café et quel prix, **c'est votre choix**. Gardez simplement à l'esprit que le bien-être de la plateforme signifie le bien-être de votre communication. Alors gardons-la saine.

Pour faire un don à **Disroot**, choisissez l'une des options suivantes.
**Assurez-vous de créer un nom de référence pour votre don**, que vous pourrez utiliser dans le formulaire de demande ci-dessous afin que nous puissions vérifier que vous avez bien fait un don. Une fois la demande traitée, les informations de référence liées à votre compte sont supprimées de notre administration (toutefois, pour des raisons évidentes, elles ne le seront pas de la banque ou de Paypal car nous n'avons aucun contrôle sur elles, alors gardez cela à l'esprit).

#### Don en ligne :

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Faire un don en utilisant Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Devenir un Patron" src="donate/_donate/p_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="https://donate.stripe.com/4gweYZfyR6FS3w49AD" target=_blank><img alt="stripe" src="donate/_donate/s_button.png" /></a>

<a href="/cryptocurrency"><img alt="Crypto-monnaie" src="donate/_donate/c_button.png" /></a>

</div>

#### Virement bancaire:
<span style="color:#8EB726; font-size:1.6em;"> Stichting Disroot.org <br>
IBAN: NL19 TRIO 0338 7622 05<br>
BIC: TRIONL2U
</span>

Cartes de crédit:<br><span style="color:#8EB726;"> Vous pouvez utiliser le bouton bleu Paypal pour les dons par carte de crédit (un compte Paypal n'est pas nécessaire). </span>
<br><br>
