---
title: 'Step 4'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
#bgcolor: '#C6FF9D'
#fontcolor: '#555'
wider_column: left
---

# Step 4:
## Request custom domain linking


---
<br>
<br>

Almost there...
If you got your domain, have set up DNS and injected your admins with fresh caffeine, now you can fill in the form and submit your request for custom domain linking.

We process the requests every Friday afternoon. Once processed, you will get an email with all the needed information to get you onboard sending messages from your own domain.
<br>
<a class="button button1" href="forms/domain-linking-form">Request Domain Linking</a>
