---
title: 'Passo 4'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
#bgcolor: '#C6FF9D'
#fontcolor: '#555'
wider_column: left
---

# Passo 4:
## Richiedi il collegamento al tuo dominio personale


---
<br>
<br>

Tutto qua...
Se hai il tuo dominio, hai impostato il DNS e hai offerto della caffeina agli amministratori di Disroot, ora puoi compilare il modulo e inviare la tua richiesta per il collegamento al tuo dominio personalizzato.

Elaboriamo le richieste ogni venerdì pomeriggio. Una volta elaborato, riceverai un'e-mail con tutte le informazioni necessarie per iniziare a inviare messaggi dal tuo dominio. 
<br>
<a class="button button1" href="forms/domain-linking-form">Richiedi il collegamento al tuo dominio personale</a>
