---
title: 'Paso 4'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
#bgcolor: '#C6FF9D'
#fontcolor: '#555'
wider_column: left
---

# Paso 4:
## Solicitar la vinculación de dominio personalizado


---
<br>
<br>

Casi ahí...
Si tienes tu dominio, has configurado los DNS e inyectado a tus admins con cafeína fresca, ahora puedes completar el formulario y enviar tu solicitud para vincular tu dominio personal.

Procesamos las solicitudes cada viernes por la tarde. Una vez hecho, recibirás un correo con toda la información necesaria para que empieces a mandar mensajes desde tu propio dominio.
<br>
<a class="button button1" href="forms/domain-linking-form">Solicitar vinculación de Dominio</a>
