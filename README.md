# Disroot.org
https://disroot.org - site

For instructions on cloning & running locally, see [vagrant/README.md](vagrant/README.md)

## License
The content on **Disroot Website** is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/)
